
#
# a library to draw a widget for tap-input of text
#
# Have a 4x10 array of buttons.  Some buttons enter a symbol,
# others switch to a different array of buttons.
# The 4 rows aren't that same, but vary like a qwerty keyboard.
# Row1:  10 buttons
# Row2:   9 buttons offset by half a button
# Row3:  10 buttons just like Row1
# Row4:   5 buttons each double-width
#
# press/drag is passed to caller as movement.
# press/hold is passed to caller as 'unmap'.
#
# Different configs are:
# lower-case alpha, with minimal punctuation
# upper-case alpha, with more punctuation
# numeric with phone and calculator punctuation
# all punctuations (there are 32 plus escape, tab,...
#
# Bottom row is normally:
#   Shift NUM  SPC ENTER BackSpace
# When 'shift' is pressed, the keyboard flips between
#   upper/lower or numeric/punc
# and bottom row becomes:
#   lock control alt ... something.

import gtk, pango, gobject

keymap = {}

keymap['lower'] = [
    ['q','w','e','r','t','y','u','i','o','p'],
    [  'a','s','d','f','g','h','j','k','l'],
    ['-','z','x','c','v','b','n','m',',','.']
]
keymap['UPPER'] = [
    ['Q','W','E','R','T','Y','U','I','O','P'],
    [  'A','S','D','F','G','H','J','K','L'],
    ['+','Z','X','C','V','B','N','M',';',':']
]
keymap['number'] = [
    ['1','2','3','4','5','6','7','8','9','0'],
    [  '+','*','-','/','#','(',')','[',']'],
    ['{','}','<','>','?',',','.','=',':',';']
]

keymap['punctuation'] = [
    ['!','@','#','$','%','^','&','*','(',')'],
    [  '~','`','_',',','.','<','>','\'','"'],
    ['\\','|','+','=','_','-','TAB','ESC','DEL','HOME']
]

class TapBoard(gtk.VBox):
    __gsignals__ = {
        'key' : (gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE,
                 (gobject.TYPE_STRING,)),
        'move': (gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE,
                 (gobject.TYPE_INT, gobject.TYPE_INT)),
        'hideme' : (gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE,
                  ())
        }
    def __init__(self):
        gtk.VBox.__init__(self)
        self.keysize = 40
        self.aspect = 1
        self.width = int(10*self.keysize)
        self.height = int(4*self.aspect*self.keysize)

        self.dragx = None
        self.dragy = None
        self.moved = False

        self.isize = gtk.icon_size_register("mine", 40, 40)

        self.button_timeout = None

        self.buttons = []

        self.set_homogeneous(True)

        for row in range(3):
            h = gtk.HBox()
            h.show()
            self.add(h)
            bl = []
            if row == 1:
                l = gtk.Label(''); l.show()
                h.pack_start(l, padding=self.keysize/4)
                l = gtk.Label(''); l.show()
                h.pack_end(l, padding=self.keysize/4)
            else:
                h.set_homogeneous(True)
            for col in range(9 + abs(row-1)):
                b = self.add_button(None, self.tap, (row,col), h)
                bl.append(b)
            self.buttons.append(bl)

        h = gtk.HBox()
        h.show()
        h.set_homogeneous(True)
        self.add(h)

        fd = pango.FontDescription('sans 10')
        fd.set_absolute_size(50 * pango.SCALE * self.keysize / 80)
        b = self.add_button('Shft', self.nextmode, False, h, fd)
        self.shftbutton = b

        b = self.add_button('Num', self.nextmode, True, h, fd)
        b = self.add_button('SPC', self.tap, (-1,' '), h, fd)
        b = self.add_button('Entr', self.tap, (-1,'\n'), h, fd)
        b = self.add_button(gtk.STOCK_UNDO, self.tap, (-1,'\b'), h)

        self.mode = 'lower'
        self.single = False
        self.size = 0
        self.connect("size-allocate", self.update_buttons)
        self.connect("realize", self.update_buttons)


    def add_button(self, label, click, arg, box, font = None):
        if not label:
            b = gtk.Button()
        elif label[0:4] == 'gtk-':
            img = gtk.image_new_from_stock(label, self.isize)
            img.show()
            b = gtk.Button()
            b.add(img)
        else:
            b = gtk.Button(label)
        b.show()
        b.set_property('can-focus', False)
        if font:
            b.child.modify_font(font)
        b.connect('button_press_event', self.press, arg)
        b.connect('button_release_event', self.release, click, arg)
        b.connect('motion_notify_event', self.motion)
        b.add_events(gtk.gdk.POINTER_MOTION_MASK|
                     gtk.gdk.POINTER_MOTION_HINT_MASK)

        box.add(b)
        return b

    def update_buttons(self, *a):
        if self.window == None:
            return

        alloc = self.buttons[0][0].get_allocation()
        w = alloc.width; h = alloc.height
        if w > h:
            size = h
        else:
            size = w
        size -= 12
        if size <= 10 or size == self.size:
            return
        #print "update buttons", size
        self.size = size

        # For each button in 3x3 we need 10 images,
        # one for initial state, and one for each of the new states
        # So there are two fonts we want.
        # First we make the initial images
        fdsingle = pango.FontDescription('sans 10')
        fdsingle.set_absolute_size(size / 1.1 * pango.SCALE)
        fdword = pango.FontDescription('sans 10')
        fdword.set_absolute_size(size / 1.5 * pango.SCALE)

        bg = self.get_style().bg_gc[gtk.STATE_NORMAL]
        fg = self.get_style().fg_gc[gtk.STATE_NORMAL]
        red = self.window.new_gc()
        red.set_foreground(self.get_colormap().alloc_color(gtk.gdk.color_parse('red')))
        base_images = {}
        for mode in keymap.keys():
            base_images[mode] = 30*[None]
            for row in range(3):
                for col in range(9+abs(1-row)):
                    if not self.buttons[row][col]:
                        continue
                    sym = keymap[mode][row][col]
                    pm = gtk.gdk.Pixmap(self.window, size, size)
                    pm.draw_rectangle(bg, True, 0, 0, size, size)
                    if len(sym) == 1:
                        self.modify_font(fdsingle)
                    else:
                        self.modify_font(fdword)
                    layout = self.create_pango_layout(sym[0:3])
                    (ink, (ex,ey,ew,eh)) = layout.get_pixel_extents()
                    pm.draw_layout(fg,
                                   int(size/2 - ew/2),
                                   int(size/2 - eh/2),
                                   layout)
                    im = gtk.Image()
                    im.set_from_pixmap(pm, None)
                    base_images[mode][row*10+col] = im
        self.base_images = base_images
        fd = pango.FontDescription('sans 10')
        fd.set_absolute_size(size / 1.5 * pango.SCALE)
        self.modify_font(fd)
        self.set_button_images()


    def set_button_images(self):
        for row in range(3):
            for col in range(9+abs(row-1)):
                b = self.buttons[row][col]
                if not b:
                    continue
                im = self.base_images[self.mode][row*10+col]
                if im:
                    b.set_image(im)
        

    def tap(self, rc):
        (row,col) = rc
        if row < 0 :
            sym = col
        else:
            sym = keymap[self.mode][row][col]
        self.emit('key', sym)

    def press(self, widget, ev, arg):
        self.dragx = int(ev.x_root)
        self.dragy = int(ev.y_root)
        self.emit('move', 0, 0)
        if True:
            # press-and-hold makes us disappear
            if not self.button_timeout:
                self.button_timeout = gobject.timeout_add(500, self.disappear)

    def release(self, widget, ev, click, arg):
        self.dragx = None
        self.dragy = None
        if arg == None:
            if self.button_timeout:
                gobject.source_remove(self.button_timeout)
                self.button_timeout = None
        if self.moved:
            self.moved = False
            # If we are half way off the screen, hide
            root = gtk.gdk.get_default_root_window()
            (rx,ry,rwidth,rheight,depth) = root.get_geometry()
            (x,y,width,height,depth) = self.window.get_geometry()
            xmid = int(x + width / 2)
            ymid = int(y + height / 2)
            if xmid < 0 or xmid > rwidth or \
               ymid < 0 or ymid > rheight:
                self.hide()
        elif arg != None and self.button_timeout:
            # quick tap in single tap mode, just enter the symbol
            gobject.source_remove(self.button_timeout)
            self.button_timeout = None
            (row,col) = arg
            sym = keymap[self.mode][row][col]
            self.emit('key', sym)
        else:
            click(arg)

    def motion(self, widget, ev):
        if self.dragx == None:
            return
        x = int(ev.x_root)
        y = int(ev.y_root)

        if abs(x-self.dragx)+abs(y-self.dragy) > 40 or self.moved:
            self.emit('move', x-self.dragx, y-self.dragy)
            self.moved = True
            if self.button_timeout:
                gobject.source_remove(self.button_timeout)
                self.button_timeout = None
        if ev.is_hint:
            gtk.gdk.flush()
            ev.window.get_pointer()

    def disappear(self):
        self.dragx = None
        self.dragy = None
        self.emit('hideme')

    def do_buttons(self):
        self.set_button_images()
        self.button_timeout = None
        return False


    def nextmode(self, a):
        if self.mode == 'lower':
            self.mode = 'UPPER'
            self.single = True
            self.shftbutton.child.set_text('Abc')
        elif self.mode == 'UPPER' and self.single:
            self.single = False
            self.shftbutton.child.set_text('ABC')
        elif self.mode == 'UPPER' and not self.single:
            self.mode = 'number'
            self.shftbutton.child.set_text('123')
        else:
            self.mode = 'lower'
            self.shftbutton.child.set_text('abc')
        self.set_button_images()

    def noprefix(self):
        
        if self.button_timeout:
            gobject.source_remove(self.button_timeout)
            self.button_timeout = None
        else:
            self.set_button_images()

        if self.single:
            self.mode = 'lower'
            self.single = False
            self.shftbutton.child.set_text('abc')
            self.set_button_images()

if __name__ == "__main__" :
    w = gtk.Window()
    w.connect("destroy", lambda w: gtk.main_quit())
    ti = TapBoard()
    w.add(ti)
    w.set_default_size(ti.width, ti.height)
    root = gtk.gdk.get_default_root_window()
    (x,y,width,height,depth) = root.get_geometry()
    x = int((width-ti.width)/2)
    y = int((height-ti.height)/2)
    w.move(x,y)
    def pkey(ti, str):
        print 'key', str
    ti.connect('key', pkey)
    def hideme(ti):
        print 'hidememe'
        w.hide()
    ti.connect('hideme', hideme)
    ti.show()
    w.show()
    
    gtk.main()

