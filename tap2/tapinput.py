#!/usr/bin/env python2.5
import os, struct, time, gtk
from fakeinput import fakeinput
from tapboard import TapBoard
import gobject


class EvDev:
    def __init__(self, path, on_event, arg=None):
        self.f = os.open(path, os.O_RDWR|os.O_NONBLOCK);
        self.ev = gobject.io_add_watch(self.f, gobject.IO_IN, self.read)
        self.on_event = on_event
        self.grabbed = False
        self.arg = arg
        self.down_count = 0
    def read(self, x, y):
        try:
            str = os.read(self.f, 16)
        except:
            return True

        if len(str) != 16:
            return True
        (sec,usec,typ,code,value) = struct.unpack_from("IIHHI", str)
        if typ == 0x01:
            # KEY event
            if value == 0:
                self.down_count -= 1
            else:
                self.down_count += 1
            if self.down_count < 0:
                self.down_count = 0
        self.on_event(self.down_count, typ, code, value, self.arg)
        return True
    def grab(self):
        if self.grabbed:
            return
        #print "grab"
        fcntl.ioctl(self.f, EVIOC_GRAB, 1)
        self.grabbed = True
    def ungrab(self):
        if not self.grabbed:
            return
        #print "release"
        fcntl.ioctl(self.f, EVIOC_GRAB, 0)
        self.grabbed = False



class KeyboardIcon(gtk.StatusIcon):
    def __init__(self, activate = None):
        gtk.StatusIcon.__init__(self)
        self.set_from_file('/usr/local/pixmaps/tapinput.png')
        self.set_activate(activate)

    def set_activate(self, activate):
        if activate:
            self.connect('activate', activate)

power_timer = None
def power_pressed(cnt, type, code, value, win):
    if type != 1:
        # not a key press
        return
    if code != 116:
        # not the power key
        return
    global power_timer
    if value != 1:
        # not a down press
        if power_timer != None:
            gobject.source_remove(power_timer)
            power_timer = None
        return
    power_timer = gobject.timeout_add(300, power_held, win)

def power_held(win):
    global power_timer
    power_timer = None
    open("/sys/class/leds/neo1973:vibrator/trigger","w").write("default-on\n")
    if win.visible:
        win.hide()
        win.visible = False
    else:
        win.hide()
        win.show()
        win.activate()
        win.visible = True
    open("/sys/class/leds/neo1973:vibrator/trigger","w").write("none\n")
    return False

last_tap = 0
def tap_pressed(cnt, type, code, value, win):
    global last_tap
    if type != 1:
        # not a key press
        return
    if code != 309:
        # not BtnZ
        return
    if value != 1:
        # only want dow, not up
        return
    now = time.time()
    print now, last_tap
    if now - last_tap < 0.2:
        # two taps
        if win.visible:
            win.hide()
            win.visible = False
        else:
            win.hide()
            win.show()
            win.activate()
            win.visible = True
        last_tap = 0
    else:
        last_tap = now


ico = KeyboardIcon()
w = gtk.Window(type=gtk.WINDOW_POPUP)
w.visible = True
w.connect("destroy", lambda w: gtk.main_quit())
ti = TapBoard()
w.add(ti)
w.set_default_size(ti.width, ti.height)
root = gtk.gdk.get_default_root_window()
(x,y,width,height,depth) = root.get_geometry()
x = int((width - ti.width)/2)
y = int((height - ti.height)/2)
w.move(x,y)
def activate(ignore):
    w.hide()
    w.show()
    w.visible = True
    w.activate()
    w.move(x,y)
ico.set_activate(activate)
fi = fakeinput()
def dokey(ti, str):
    fi.send_char(str)
ti.connect('key', dokey)
def domove(ti, x,y):
    global startx, starty
    if x == 0 and y == 0:
        (startx, starty) = w.get_position()
    x = 0
    w.move(startx+x, starty+y)

ti.connect('move', domove)
def hideme(ti):
    w.hide()
    w.visible = False
ti.connect('hideme', hideme)
try:
    pbtn = EvDev("/dev/input/event0", power_pressed, w)
    tbtn = EvDev("/dev/input/event3", tap_pressed, w)
except:
    pass
ti.show()
w.show()
fi.new_window()
gtk.main()

