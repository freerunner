#!/usr/bin/env python

# Collect SMS messages from the GSM device.
# We store a list of messages that are thought to be
# in the SIM card: number from date
#  e.g.   17 61403xxxxxx 09/02/17,20:28:36+44
# As we read messages, if we find one that is not in that list,
# we record it in the SMS store, then update the list
#
# An option can specify either 'new' or 'all.
# 'new' will only ask for 'REC UNREAD' and so will be faster and so
# is appropriate when we know that a new message has arrived.
# 'all' reads all messages and so is appropriate for an occasional
# 'sync' like when first turning the phone on.
#
# If we discover that the SMS card is more than half full, we
# deleted the oldest messages.
# We discover this by 'all' finding lots of messages, or 'new'
# finding a message with a high index.
# For now, we "know" that the SIM card can hold 30 messages.
#
# We need to be careful about long messages.  A multi-part message
# looks like e.g.
#+CMGL: 19,"REC UNREAD","61403xxxxxx",,"09/02/18,10:51:46+44",145,140
#0500031C0201A8E8F41C949E83C2207B599E07B1DFEE33A85D9ECFC3E732888E0ED34165FCB85C26CF41747419344FBBCFEC32A85D9ECFC3E732889D6EA7E9A0F91B444787E9A024681C7683E6E532E88E0ED341E939485E1E97D3F6321914A683E8E832E84D4797E5A0B29B0C7ABB41ED3CC8282F9741F2BADB5D96BB40D7329B0D9AD3D36C36887E2FBBE9
#+CMGL: 20,"REC UNREAD","61403xxxxxx",,"09/02/18,10:51:47+44",145,30
#0500031C0202F2A0B71C347F83D8653ABD2C9F83E86FD0F90D72B95C2E17

# If that was just hex you could use
#  perl -e 'print pack("H*","050....")'
# to print it.. but no...
# Looks like it decodes as:
# 05  - length of header, not including this byte
# 00  - concatentated SMS with 8 bit ref number (08 means 16 bit ref number)
# 03  - length of rest of header
# 1C  - ref number for this concat-SMS
# 02  - number of parts in this SMS
# 01  - number of this part - counting starts from 1
# A8E8F41C949E83C22.... message, 7 bits per char. so:
#  A8  - 54 *2 + 0   54 == T           1010100 0     1010100
# 0E8  - 68 *2 + 0   68 == h           1 1101000     1101000
#  F4  -             69 == i           11 110100     1101001  1
#  1C                73 == s           000 11100     1110011  11
#  94                20 == space       1001 0100     0100000  000
#  9E                69 == i           10011 110     1101001  1001
#  83                73 == s           100000 11     1110011  10011
#                    20 == space                    0100000   0100000

# 153 characters in first message. 19*8 + 1
# that uses 19*7+1 == 134 octets
# There are 6 in the header so a total of 140
# second message has 27 letters - 3*8+3
# That requires 3*7+3 == 24 octets.  30 with the 6 octet header.

# then there are VCARD messages that look lie e.g.
#+CMGL: 2,"REC READ","61403xxxxxx",,"09/01/29,13:01:26+44",145,137
#06050423F40000424547494E3A56434152440D0A56455253494F4E3A322E310D0A4E3A....0D0A454E443A56434152440D0A
#which is
#06050423F40000
#then
#BEGIN:VCARD
#VERSION:2.1
#N: ...
#...
#END:VCARD
# The 06050423f40000
# might decode like:
#  06  - length of rest of header
#  05  - magic code meaning 'user data'
#  04  - length of rest of header...
#  23  - 
#  f4  -  destination port '23f4' means 'vcard'
#  00  -   
#  00  -  0000 is the origin port.
#
#in hex/ascii
#
# For now, ignore anything longer than the specified length.

import os
os.environ['PYTRACE'] = '1'

import atchan, sys, re
from storesms import SMSmesg, SMSstore


def load_mirror(filename):
    # load an array of index address date
    # from the file and store in a hash
    rv = {}
    try:
        f = file(filename)
    except IOError:
        return rv
    l = f.readline()
    while l:
        fields = l.strip().split(None, 1)
        rv[fields[0]] = fields[1]
        l = f.readline()
    return rv

def save_mirror(filename, hash):
    n = filename + '.new'
    f = open(n, 'w')
    for i in hash:
        f.write(i + ' ' + hash[i] + '\n')
    f.close()
    os.rename(n, filename)


def sms_decode(msg):
    #msg is a 7-in-8 encoding of a longer message.
    pos = 0
    carry = 0
    str = ''
    while msg:
        c = msg[0:2]
        msg = msg[2:]
        b = int(c, 16)

        if pos == 0:
            if carry:
                str += chr(carry + (b&1)*64)
                carry = 0
            b /= 2
        else:
            b = (b << (pos-1)) | carry
            carry = (b & 0xff80) >> 7
            b &= 0x7f
        if (b & 0x7f) != 0:
            str += chr(b&0x7f)
        pos = (pos+1) % 7
    return str

def main():
    mode = 'all'
    for a in sys.argv[1:]:
        if a == '-n':
            mode = 'new'
        else:
            print "Unknown option:", a
            sys.exit(1)

    pth = None
    for p in ['/media/card','/media/disk','/var/tmp']:
        if os.path.exists(os.path.join(p,'SMS')):
            pth = p
            break

    if not pth:
        print "Cannot find SMS directory"
        sys.exit(1)

    dir = os.path.join(pth, 'SMS')

    store = SMSstore(dir)

    chan = atchan.AtChannel()
    chan.connect()
    
    if not atchan.found(chan.cmdchat('get_power'), 'OK on'):
        sys.exit(1)

    if not atchan.found(chan.cmdchat('connect'), 'OK'):
        sys.exit(1)

    chan.chat1('', ['OK', 'ERROR']);
    if chan.chat1('ATE0', ['OK', 'ERROR']) != 0:
        sys.exit(1)

    # get ID of SIM card
    n,c = chan.chat('AT+CIMI', ['OK', 'ERROR'])
    CIMI='unknown'
    for l in c:
        l = l.strip()
        if re.match('^\d+$', l):
            CIMI = l

    mfile = os.path.join(dir, '.sim-mirror-'+CIMI)
    #FIXME lock mirror file
    mirror = load_mirror(mfile)
    mirror_changed = False

    chan.chat('AT+CMGF=1', ['OK','ERROR'])
    if mode == 'new':
        chan.atcmd('+CMGL="REC UNREAD"')
    else:
        chan.atcmd('+CMGL="ALL"')

    # reading the msg list might fail for some reason, so
    # we always prime the mirror list with the previous version
    # and only replace things, never assume they aren't there
    # because we cannot see them
    newmirror = mirror

    l = ''
    state = 'waiting'
    msg = ''
    #                            indx  state    from          name       date          type len
    want = re.compile('^\+CMGL: (\d+),("[^"]*")?,("([^"]*)")?,("[^"]*")?,("([^"]*)")?,(\d+),(\d+)$')

    while state == 'reading' or not (l[0:2] == 'OK' or l[0:5] == 'ERROR' or
                                     l[0:10] == '+CMS ERROR'):
        l = chan.wait_line(1000)
        if l == None:
            sys.exit(1)
        if state == 'reading':
            if msg:
                msg += '\n'
            msg += l
            if len(msg) >= msg_len:
                state = 'waiting'
                ref = None; part = None
                if len(msg) > msg_len:
                    if msg[0:6] == '050003':
                        # concatenated message with 8bit ref number
                        ref  = msg[6:8]
                        part = ( int(msg[10:12],16), int(msg[8:10], 16))
                        msg = sms_decode(msg[12:])
                    else:
                        print "ignoring", index, sender, date, msg
                        continue
                print "found", index, sender, date, msg
                if index in mirror and mirror[index] == date + ' ' + sender:
                    print "Already have that one"
                else:
                    sms = SMSmesg(source='GSM', time=date, sender=sender,
                                  text = msg, state = 'NEW',
                                  ref= ref, part = part)
                    store.store(sms)
                newmirror[index] = date + ' ' + sender
        else:
            m = want.match(l)
            if m:
                g = m.groups()
                index = g[0]
                sender = g[3]
                date = g[6]
                typ = int(g[7])
                if typ & 16:
                    sender = '+' + sender
                msg_len = int(g[8])
                msg = ''
                state = 'reading'

    mirror = newmirror

    if len(mirror) > 10:
        rev = {}
        dlist = []
        for i in mirror:
            rev[mirror[i]] = i
            dlist.append(mirror[i])
        dlist.sort()
        for i in range(len(mirror) - 10):
            dt=dlist[i]
            ind = rev[dt]
            resp = chan.chat1('AT+CMGD=%s' % ind, ['OK', 'ERROR', '+CMS ERROR'],
                              timeout=3000)
            if resp == 0:
                del mirror[ind]

    save_mirror(mfile, mirror)

main()
