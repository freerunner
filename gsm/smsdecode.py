def sms_decode(msg):
    #msg is a 7-in-8 encoding of a longer message.
    pos = 0
    carry = 0
    str = ''
    while msg:
        c = msg[0:2]
        msg = msg[2:]
        b = int(c, 16)

        if pos == 0:
            if carry:
                str += chr(carry + (b&1)*64)
                carry = 0
            b /= 2
        else:
            b = (b << (pos-1)) | carry
            carry = (b & 0xff80) >> 7
            b &= 0x7f
        if (b & 0x7f) != 0:
            str += chr(b&0x7f)
        pos = (pos+1) % 7
    return str

import sys
print sms_decode(sys.argv[1])
