#!/usr/bin/env python

# Send an SMS message using GSM.
# Args are:
#   sender - ignored
#   recipient - phone number
#   message - no newlines
#
# We simply connect to the GSM module,
# check for a registration
# and
#   AT+CMGS="recipient"
#   >  message
#   >  control-Z
#
# Sending multipart sms messages:
#    ref: http://www.developershome.com/sms/cmgsCommand4.asp
# 1/ set PDU mode with AT+CMGF=0
# 2/ split message into 153-char bundles
# 3/ create messages as follows:
#
#  00   - this says we aren't providing an SMSC number
#  41   - TPDU header - type is SMS-SUBMIT, user-data header present
#  00   - please assign a message reference number
#  xx   - length in digits of phone number
#  91 for IDD, 81 for "don't know what sort of number this is"
#  164030...  BCD phone number, nibble-swapped, pad with F at end if needed
#  00   - protocol identifier??
#  00   - encoding - 7 bit ascii
#  XX   - length of message in septets
# Then the message which starts with 7 septets of header that looks like 6 octets.
#  05   -  length of rest of header
#  00   -  multi-path with 1 byte id number
#  03   -  length of rest
#  idnumber - random id number
#  parts    - number of parts
#  this part - this part, starts from '1'
#
# then AT+CMGS=len-of-TPDU in octets
#
# TPDU header byte:
# Structure: (n) = bits
# +--------+----------+---------+-------+-------+--------+
# | RP (1) | UDHI (1) | SRI (1) | X (1) | X (1) | MTI(2) |
# +--------+----------+---------+-------+-------+--------+
#	RP:
#		Reply path
#	UDHI:
#		User Data Header Indicator = Does the UD contains a header
#		0 : Only the Short Message
#		1 : Beginning of UD containsheader information
#	SRI:
#		Status Report Indication.
#		The SME (Short Message Entity) has requested a status report.
#	MTI:
#		00 for SMS-Deliver
#               01 for SMS-SUBMIT


import atchan, sys, re, random

def encode_number(recipient):
    # encoded number is
    # number of digits
    # 91 for international, 81 for local interpretation
    # BCD digits, nibble-swapped, F padded.
    if recipient[0] == '+':
        type = '91'
        recipient = recipient[1:]
    else:
        type = '81'
    leng = '%02X' % len(recipient)
    if len(recipient) % 2 == 1:
        recipient += 'F'
    swap = ''
    while recipient:
        swap += recipient[1] + recipient[0]
        recipient = recipient[2:]
    return leng + type + swap

def code7(pad, mesg):
    # Encode the message as 8 chars in 7 bytes.
    # pad with 'pad' 0 bits at the start (low in the byte)
    carry = 0
    # we have 'pad' low bits stored low in 'carry'
    code = ''
    while mesg:
        c = ord(mesg[0])
        mesg = mesg[1:]
        if pad + 7 >= 8:
            # have a full byte
            b = carry & ((1<<pad)-1)
            b += (c << pad) & 255
            code += '%02X' % b
            pad -= 1
            carry = c >> (7-pad)
        else:
            # not a full byte yet, just a septet
            pad = 7
            carry = c
    if pad:
        # a few bits still to emit
        b = carry & ((1<<pad)-1)
        code += '%02X' % b
    return code

def add_len(mesg):
    return ('%02X' % (len(mesg)/2)) + mesg

def send(chan, dest, mesg):
    n,c = chan.chat('AT+CMGS=%d' % (len(mesg)/2), ['OK','ERROR','>'])
    if n == 2:
        n,c = chan.chat('%s%s\r\n\032' % (dest, mesg), ['OK','ERROR'], 10000)
        if n == 0 and atchan.found(c, re.compile('^\+CMGS: \d+') ):
            return True
    return False

recipient = sys.argv[2]
mesg = sys.argv[3]

chan = atchan.AtChannel()
chan.connect()

if not atchan.found(chan.cmdchat('get_power'), 'OK on'):
    print 'GSM disabled - message not sent'
    sys.exit(1)

if not atchan.found(chan.cmdchat('connect'), 'OK'):
    print 'system error - message not sent'
    sys.exit(1)
chan.chat1(None, [ 'AT-Command Interpreter ready' ], 500)
# clear any pending error status
chan.chat1('ATE0', ['OK', 'ERROR'])
if chan.chat1('ATE0', ['OK', 'ERROR']) != 0:
    print 'system error - message not sent'
    sys.exit(1)

want = re.compile('^\+COPS:.*".+"')
n,c =  chan.chat('AT+COPS?', ['OK', 'ERROR'], 2000 )
if n != 0 or not atchan.found(c, want):
    print 'No Service - message not sent'
    sys.exit(1)

# use PDU mode
n,c = chan.chat('AT+CMGF=0', ['OK', 'ERROR'])
if n != 0:
    print 'Unknown error'
    sys.exit(1)

# SMSC header and TPDU header
SMSC = '00' # No SMSC number given, use default
REF = '00'  # ask network to assign ref number
phone_num = encode_number(recipient)
proto = '00' # don't know what this means
encode = '00' # 7 bit ascii
if len(mesg) <= 160:
    # single SMS mesg
    m1 = code7(0,mesg)
    m2 = '%02X'%len(mesg) + m1
    coded = "01" + REF + phone_num + proto + encode + m2
    if send(chan, SMSC, coded):
        print "OK message sent"
        sys.exit(0)
    else:
        print "ERROR message not sent"
        sys.exit(1)

elif len(mesg) <= 5 * 153:
    # Multiple messsage
    packets = (len(mesg) + 152) / 153
    packet = 0
    mesgid = random.getrandbits(8)
    while len(mesg) > 0:
        m = mesg[0:153]
        mesg = mesg[153:]
        id = mesgid
        packet = packet + 1
        UDH = add_len('00' + add_len('%02X%02X%02X'%(id, packets, packet)))
        m1 = UDH + code7(1, m)
        m2 = '%02X'%(7+len(m)) + m1
        coded = "41" + REF + phone_num + proto + encode + m2
        if not send(chan, SMSC, coded):
            print "ERROR message not sent at part %d/%d" % (packet,packets)
            sys.exit(1)
    print 'OK message sent in %d parts' % packets
    sys.exit(0)
else:
    print 'Message is too long'
    sys.exit(1)

