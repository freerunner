#!/usr/bin/env python
#
# This is a sound playing daemon for the freerunner.
# It watches the directory "/var/run/sound" and when a file appears
# there-in, it gets played.
# Currently the file must be a WAV file with 16 bit little-endian PCM encoding.
#
# Files can (and should) have priorities being leading digits.
# If there are multiple files, the one with the lowest number is played.
#
# Files normally appear via the creation of symlinks.
# When the file is removed, the playing stops.  When a new file of higher
# priority appears, the current file is suspended.  When the higher
# priority file is removed, the lower priority one resumes.
#
# When a file finishes playing it can do one of several thing:
#  - the file can be removed (R)
#  - the file can be replayed (P)
#  - the player can stop and wait (W)
# If the file has a timestamp (on symlink) that has changed since
# play started, it is treated as a new file by 'W'.
# An empty file produces silence.
#
# File names should start with 1 or more leading digits (if there are
# no digits the effective priority is infinite).  These form a number
# which is the reverse of priority, so a small number is played first.
# This a numerical sequence will be played in order.  After these
# digits should be an R, P, or W.  If none of these are present, R is
# assumed.
#
# The player can be stopped by creating a symlink from '0P-silence' to
# '/dev/null'.
#
# When a file is suspending, the position in the file, in microseconds
# is written to a new file with name formed by putting a '.' at the
# start of the file name.  Maybe this is continuously updated...
#


import alsaaudio, time, struct, sys, os, signal, fcntl

class PlayFile():
    def __init__(self, file, pcm):
        # Arrange to play file through pcm
        # Every time .play is called, we play some of the file
        # If something else gets played, .resume must be called
        # before .play is called again
        self.pcm = pcm
        self.filename = file
        self.posfile = os.path.join(os.path.dirname(file),
                                    '.'+os.path.basename(file))
        self.loadfile(file)
        self.update()

    def loadfile(self, file):
        # A wav file starts:
        #   0-3  "RIFF"
        #   4-7  Bytes in rest of file.
        #   8-11 "WAVE"
        #  12-15 "fmt "
        #  16-19 bytes of format
        #  20-21 ==1  Microsoft PCM
        #  22-23      channels
        #  24-27  freq
        #  28-31  byte rate
        #  32-33  bytes per frame
        #  34-35  bits per sample
        #  36-39 "data"
        #  40-43 number of bytes of data
        #  44... actual samples
        self.pos = 0
        self.rate = 8000
        self.channels = 1
        self.bytes = 2
        self.format = alsaaudio.PCM_FORMAT_S16_LE
        try:
            self.f = open(file)
        except IOError:
            self.f = None
            return
        header = self.f.read(44)
        if len(header) == 0:
            # silence
            return
        if len(header) != 44:
            raise IOError
        riff, b1, wave, fmt, b2, format, chan, rate, br, bf, bs, data, b3 = \
              struct.unpack("4si4s 4sihhiihh 4si", header)

        if riff != "RIFF" or wave != "WAVE" or fmt != "fmt " or data != "data":
            raise ValueError
        if format == 1 and bs == 16:
            self.format = alsaaudio.PCM_FORMAT_S16_LE
            self.bytes = 2
        elif format == 1 and bs == 8:
            self.format = alsaaudio.PCM_FORMAT_U8
            self.bytes = 1
        else:
            raise ValueError

        if chan < 1 or chan > 4:
            raise ValueError
        else:
            self.channels = chan

        self.rate = rate
        self.finished = False
        self.pos = 0;
        self.resume()

    def resume(self):
        try:
            self.pcm.setformat(self.format)
            self.pcm.setchannels(self.channels)
            self.pcm.setrate(self.rate)
            self.pcm.setperiodsize(640 / self.channels / self.bytes)
        except:
            pass

    def update(self):
        f = open(self.posfile, 'w')
        f.write("%d\n" % int(self.pos*1000000 / self.rate))

    def play(self):
        # play for at least 100ms
        start = time.time()
        if not self.f:
            return False
        while time.time() < start + 0.1:
            data = self.f.read(640)
            if not data:
                self.finished = True
                try:
                    os.unlink(self.posfile)
                except OSError:
                    pass
                return False
            if len(data) % (self.channels * self.bytes) == 0:
                self.pcm.write(data)
                if len(data) != 640:
                    self.pcm.write(chr(0) * (640 - len(data)))
            self.pos += len(data) / self.channels / self.bytes
        self.update()
        return True


class DirWatch:
    def __init__(self, dirname):
        self.mtime = 0
        self.dirname = dirname
        self.name = ''
        self.disp = ''

    def ping(self, *a):
        signalled = True
        
    def choose(self, wait=False):
        mtime = os.stat(self.dirname).st_mtime
        if self.mtime == mtime:
            if not wait:
                return self.name, self.disp
            # wait until it might have changed, using dnotify
            f = os.open(self.dirname, 0)
            signalled = False
            signal.signal(signal.SIGIO, self.ping)
            fcntl.fcntl(f, fcntl.F_NOTIFY, (fcntl.DN_MODIFY|fcntl.DN_RENAME|
                                            fcntl.DN_CREATE|fcntl.DN_DELETE))
            mtime = os.stat(self.dirname).st_mtime
            while not signalled and mtime == self.mtime:
                signal.pause()
                mtime = os.stat(self.dirname).st_mtime
            os.close(f)

        # Better check again
        self.mtime = mtime
        min = None
        disp = None
        name = None
        for n in os.listdir(self.dirname):
            if n[0] == '.':
                continue
            (num,d) = self.parse(n)
            if name == None:
                name, disp, min = n, d, num
            elif num == min:
                if n > name:
                    name, disp = n, d
            elif num == None:
                pass
            elif min == None or num < min:
                name, disp, min = n, d, num
        if name == None:
            return name, None
        self.name = os.path.join(self.dirname, name)
        if disp != 'R' and disp != 'P':
            disp = 'W'
        self.disp = disp
        return self.name, disp

    def parse(self, name):
        n = ''
        while name[0].isdigit():
            n += name[0]
            name = name[1:]
        disp = name[0]
        if name[0] not in 'PRW':
            disp = 'W'

        if n:
            num = int(n)
        else:
            num = None
        return num, disp

def main():
    os.nice(-20)
    dn = '/var/run/sound'
    if not os.path.exists(dn):
        os.mkdir(dn)
    d = DirWatch(dn)
    stack = []

    current = None
    disp = None
    waiting = False

    while True:
        newname, newdisp = d.choose(current == None or waiting)
        if current and current.filename == newname:
            if current.play():
                continue
            if disp == 'R':
                os.unlink(current.filename)
                current = None
                continue
            if disp == 'P':
                time.sleep(0.1)
                current.loadfile(current.filename)
                continue
            waiting = True
            continue
        waiting = False
        # need new...
        if current and not os.path.exists(current.filename):
            current = None

        if current == None and len(stack) > 0:
            current, disp = stack.pop()
            current.resume()
            continue

        if current:
            stack.append((current,disp))

        if newname == None:
            continue

        pcm = alsaaudio.PCM(alsaaudio.PCM_PLAYBACK)
        current = PlayFile(newname, pcm)
        del pcm
        disp = newdisp

main()
