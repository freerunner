#!/usr/bin/env python
# -*- Mode: Python -*-
# vi:si:et:sw=4:sts=4:ts=4

import pygtk
pygtk.require('2.0')

import sys

import gobject

import suspend

import pygst
pygst.require('0.10')
import gst
import gst.interfaces
import gtk

import urllib
import os
import random
import pango

class MusicList:
    # Allows selecting songs and moving through the list.
    # movement can be
    #  sequential (alpha order)
    #  random-album (Seq through album, then random next album)
    #  random  (random walk through all)
    #
    # We store two states.
    # 1/ The current song to play.  This is in 'album' and 'song' and mode etc.
    # 2/ The browse location, in 'dir' and 'pos'
    def __init__(self, path):
        self.albums = {}
        self.dirs = {}
        self.names = {}
        self.add_path(path)
        self.on_change = []
        for a in self.albums:
            self.albums[a].sort()
        for p in self.dirs:
            self.dirs[p].sort()

        self.albumlist = self.albums.keys()
        self.albumlist.sort()

        self.path = path
        self.dir = "/"
        self.pos = 0
        
        self.album = None
        self.song = None
        self.mode = 'seq'

        self.set_dir("")

    def set_dir(self, dir):
        self.dir = dir
        p = self.path + self.dir
        print "p is", p
        self.folders = []
        if p in self.dirs:
            self.folders = self.dirs[p]
        self.songs = []
        if p in self.albums:
            self.songs = self.albums[p]
        self.pos = 0

    def set_song(self, song):
        # this song is in the current dir,
        # update folder and song, and play
        print "dir is", self.dir
        print self.albumlist
        print "xx"
        self.album = self.albumlist.index(self.path+self.dir)
        self.song = self.albums[self.path+self.dir].index(song)
        print "set song", self.album, self.song
        self.changed()

    def namecnt(self):
        cnt = 0
        if self.dir != "":
            cnt = 1
        cnt += len(self.folders)
        cnt += len(self.songs)
        return cnt

    def getname(self, num):
        if self.dir != "":
            if num == 0:
                return ("parent", "<parent>")
            num -= 1
        if num < len(self.folders):
            return ("folder", self.folders[num])
        num -= len(self.folders)
        if num < len(self.songs):
            return ("song", self.songs[num])
        return ("end", "unknown")

    def changer(self, func):
        self.on_change.append(func)

    def changed(self):
        for func in self.on_change:
            func()

    def next(self):
        # move to the next song
        if self.mode == 'seq':
            # just return the sequentially next song
            while True:
                if self.album == None:
                    self.album = 0
                    self.song = 0
                elif self.song == None:
                    self.song = 0
                else:
                    self.song += 1
                if len(self.albumlist) == 0:
                    break
                if self.song < len(self.albums[self.albumlist[self.album]]):
                    break
                self.album += 1
                self.song = None
                if self.album >= len(self.albumlist):
                    self.album = None
                    return False

            self.changed()
            return True

    def curr_song(self):
        a = self.albumlist[self.album]
        s = self.albums[a][self.song]
        t = self.names[s]
        return (a,s,t)

    def prev(self):
        # return the 'previous' song as (path,album,name)
        if self.mode == 'seq':
            # just return the sequentially previous song
            while True:
                if self.album == None:
                    self.album = len(self.albumlist)-1
                    self.song = len(self.albums[self.albumlist[self.album]])-1
                elif self.song == None:
                    self.song = len(self.albums[self.albumlist[self.album]])-1
                else:
                    self.song -= 1
                if self.song >= 0:
                    break
                self.album -= 1
                self.song = None
                if self.album < 0:
                    self.album = None
                    return False
            self.changed()
            return True


    def add_path(self, path):
        try:
            n = os.listdir(path)
        except:
            return
        else:
            pass
        for f in n:
            p = os.path.join(path,f)
            if os.path.isdir(p):
                self.add_path(p)
            if os.path.isfile(p) and p[-4:] == ".ogg":
                self.addsong(path, f)

    def addsong(self, path, name):
        if not path in self.albums:
            self.albums[path] = []
            self.add_dir(path)
        p = path
        n = name
        while p and p != "/":
            (p, b) = os.path.split(p)
            n = n.replace(("- %s -"%b), "-")
        if n[-4:] == ".ogg":
            n = n[:-4]
        self.albums[path].append(name)
        self.names[name] = urllib.unquote_plus(n)

    def add_dir(self, path):
        (h,t) = os.path.split(path)
        if not h in self.dirs:
            self.dirs[h] = []
            self.add_dir(h)
        self.dirs[h].append(t)

    def title(self, song = None):
        if song != None:
            return self.names[song]
        if self.album == None or self.song == None:
            return "Nothing Playing"
        return self.names[self.albums[self.albumlist[self.album]][self.song]]

class GstPlayer:
    def __init__(self):
        self.playing = False
        self.player = gst.element_factory_make("playbin", "player")
        self.on_eos = False

        bus = self.player.get_bus()
        bus.enable_sync_message_emission()
        bus.add_signal_watch()
        bus.connect('message', self.on_message)

    def on_message(self, bus, message):
        t = message.type
        if t == gst.MESSAGE_ERROR:
            err, debug = message.parse_error()
            print "Error: %s" % err, debug
            self.playing = False
            if self.on_eos:
                self.on_eos()
        elif t == gst.MESSAGE_EOS:
            self.playing = False
            if self.on_eos:
                self.on_eos()

    def set_location(self, location):
        self.player.set_property('uri', location)

    def set_volume(self, volume):
        self.player.set_property('volume', volume / 100.0)

    def query_position(self):
        "Returns a (position, duration) tuple"
        try:
            position, format = self.player.query_position(gst.FORMAT_TIME)
        except:
            position = gst.CLOCK_TIME_NONE

        try:
            duration, format = self.player.query_duration(gst.FORMAT_TIME)
        except:
            duration = gst.CLOCK_TIME_NONE

        return (position, duration)

    def seek(self, location):
        """
        @param location: time to seek to, in nanoseconds
        """
        gst.debug("seeking to %r" % location)
        event = gst.event_new_seek(1.0, gst.FORMAT_TIME,
            gst.SEEK_FLAG_FLUSH | gst.SEEK_FLAG_ACCURATE,
            gst.SEEK_TYPE_SET, location,
            gst.SEEK_TYPE_NONE, 0)

        res = self.player.send_event(event)
        if res:
            gst.info("setting new stream time to 0")
            self.player.set_new_stream_time(0L)
        else:
            gst.error("seek to %r failed" % location)

    def pause(self):
        gst.info("pausing player")
        self.player.set_state(gst.STATE_PAUSED)
        self.playing = False

    def play(self):
        gst.info("playing player")
        self.player.set_state(gst.STATE_PLAYING)
        self.playing = True
        
    def stop(self):
        self.player.set_state(gst.STATE_NULL)
        self.playing = False
        gst.info("stopped player")

    def get_state(self, timeout=1):
        return self.player.get_state(timeout=timeout)

    def is_playing(self):
        return self.playing

class TitleWindow(gtk.DrawingArea):
    def __init__(self, db):
        gtk.DrawingArea.__init__(self)

        self.pixbuf = None
        self.width = self.height = 0
        self.need_redraw = True
        self.colours = None
        self.db = db

        self.pos_stack = []

        self.connect("expose-event", self.redraw)
        self.connect("configure-event", self.reconfig)
        
        self.connect("button_release_event", self.release)
        self.connect("button_press_event", self.press)
        self.set_events(gtk.gdk.EXPOSURE_MASK
                        | gtk.gdk.BUTTON_PRESS_MASK
                        | gtk.gdk.BUTTON_RELEASE_MASK
                        | gtk.gdk.STRUCTURE_MASK)

        # choose a font
        fd = self.get_pango_context().get_font_description()
        fd.set_absolute_size(25 * pango.SCALE)
        self.fd = fd
        self.modify_font(fd)
        met = self.get_pango_context().get_metrics(fd)
        self.lineheight = (met.get_ascent() + met.get_descent()) / pango.SCALE


        self.queue_draw()

    def reconfig(self, w, ev):
        alloc = w.get_allocation()
        if not self.pixbuf:
            return
        if alloc.width != self.width or alloc.height != self.height:
            self.pixbuf = None
            self.need_redraw = True

    def add_col(self, sym, col):
        c = gtk.gdk.color_parse(col)
        gc = self.window.new_gc()
        gc.set_foreground(self.get_colormap().alloc_color(c))
        self.colours[sym] = gc

    def redraw(self, w, ev):
        if self.colours == None:
            self.colours = {}
            self.add_col('song', "blue")
            self.add_col('bg', "yellow")
            self.add_col('C', "red")
            self.add_col('parent', "orange")
            self.add_col('folder', "black")
            self.add_col('end', "white")
            self.add_col('_', "black")
            self.bg = self.get_style().bg_gc[gtk.STATE_NORMAL]

        if self.need_redraw:
            self.draw_buf()

        self.window.draw_drawable(self.bg, self.pixbuf, 0, 0, 0, 0,
                                         self.width, self.height)


    def draw_buf(self):
        self.need_redraw = False
        if self.pixbuf == None:
            alloc = self.get_allocation()
            self.pixbuf = gtk.gdk.Pixmap(self.window, alloc.width, alloc.height)
            self.width = alloc.width
            self.height = alloc.height
        self.pixbuf.draw_rectangle(self.bg, True, 0, 0,
                                   self.width, self.height)

        lines = int((self.height) / self.lineheight) - 1
        entries = self.db.namecnt()
        # probably place current song in the middle
        top = self.db.pos - lines / 2
        # but try not to leave blank space at the end
        if entries - self.db.pos < lines/2:
            top = entries - lines
        # but never have blank space at the top
        if top < 0:
            top = 0
        self.top = top
        offset = 0
        for l in range(lines):
            (type, name) = self.db.getname(top + l)
            if type == "end":
                break
            if l == self.db.pos - top:
                self.fd.set_absolute_size(40 * pango.SCALE)
                self.modify_font(self.fd)
            if type == "song":
                layout = self.create_pango_layout(self.db.title(name))
            elif type == "folder":
                layout = self.create_pango_layout(urllib.unquote_plus(name))
            else:
                layout = self.create_pango_layout(name)
            #(ink, log) = layout.get_pixel_extents()
            #(ex,ey,ew,eh) = log
            #self.pixbuf.draw_layout(self.colours['X'], (self.width-ew)/2,
            #(self.height-eh)/2,
            #layout)
            if l == self.db.pos - top:
                self.pixbuf.draw_rectangle(self.colours['end'], True,
                                           0, l*self.lineheight,
                                           self.width, self.lineheight*2)
                self.pixbuf.draw_layout(self.colours[type],
                                        0, l * self.lineheight,
                                        layout)
                offset = self.lineheight
                self.fd.set_absolute_size(25 * pango.SCALE)
                self.modify_font(self.fd)
            else:
                self.pixbuf.draw_layout(self.colours[type],
                                        0, l * self.lineheight + offset,
                                        layout)

    def refresh(self):
        self.need_redraw = True
        self.queue_draw()
        
    def press(self,w,ev):
        row = int(ev.y / self.lineheight)
        if row > self.db.pos - self.top:
            row -= 1
        if self.db.pos != row + self.top:
            self.db.pos = row + self.top
        else:
            (type,name) = self.db.getname(row + self.top)
            if type == "parent":
                sl = self.db.dir.rindex('/')
                self.db.set_dir(self.db.dir[0:sl])
                (t,p) = self.pos_stack.pop()
                self.top = t
                self.db.pos = p
            elif type  == "folder":
                self.pos_stack.append((self.top, self.db.pos))
                self.db.set_dir(self.db.dir + "/" + name)
            elif type == "song":
                print "play", self.db.dir+"/"+name
                self.db.set_song(name)
                
        self.refresh()
        
    def release(self,w,ev):
        pass

class FingerScale(gtk.DrawingArea):
    def __init__(self, control):
        gtk.DrawingArea.__init__(self)
        self.control = control

        self.connect("expose-event", self.redraw)
        self.connect("configure-event", self.reconfig)
        
        self.connect("button_release_event", self.release)
        self.connect("button_press_event", self.press)
        self.set_events(gtk.gdk.EXPOSURE_MASK
                        | gtk.gdk.BUTTON_PRESS_MASK
                        | gtk.gdk.BUTTON_RELEASE_MASK
                        | gtk.gdk.STRUCTURE_MASK)

        ctx = self.get_pango_context()
        fd = ctx.get_font_description()
        fd.set_absolute_size(25 * pango.SCALE)
        self.modify_font(fd)
        met = ctx.get_metrics(fd)
        self.lineheight = (met.get_ascent() + met.get_descent()) / pango.SCALE

    def start(self, percent, widget):
        a = widget.get_allocation()
        alloc = (a.x,a.y,a.width,a.height)
        self.homewidget = alloc
        self.grab_add()
        self.tracking = False
        self.show()
        self.set(percent)

    def end(self, percent = None):
        self.hide()
        self.grab_remove()
        if percent == None:
            percent = self.percent
        if self.tracking:
            self.control(percent, True)

    def set(self, percent):
        self.percent = percent
        self.str = self.control(percent)
        self.layout = self.create_pango_layout(self.str)
        (ink, (ex,ey,ew,eh)) = self.layout.get_pixel_extents()
        self.sw = ew
        
        self.queue_draw()

    def reconfig(self, w, ev):
        self.alloc = self.get_allocation()
        self.theight = self.alloc.height - self.lineheight
        
    def redraw(self, area, ev):
        self.window.draw_rectangle(self.get_style().bg_gc[gtk.STATE_NORMAL],
                                   True, 0, 0,
                                   self.alloc.width, self.alloc.height)
        self.window.draw_rectangle(self.get_style().fg_gc[gtk.STATE_NORMAL],
                                   False, 0, 0,
                                   self.alloc.width-2, self.alloc.height-2)
        self.window.draw_layout(self.get_style().fg_gc[gtk.STATE_NORMAL],
                                int((self.alloc.width - self.sw)/2),
                                int((self.percent * self.theight / 100)),
                                self.layout)
        
    def release(self, c, ev):
        if not self.tracking:
            self.tracking = True
            return
        (gx,gy,gw,gh, gd) = ev.window.get_geometry()
        (ox,oy) = c.window.get_origin()
        y = ev.y_root - oy

        percent = (y - self.lineheight/2) * 100 / self.theight
        if percent < 0:
            percent = 0
        if percent > 100:
            percent = 100
        
        if (gx,gy,gw,gh) == self.homewidget:
            self.end()
        else:
            self.set(percent)

    def press(self, c, ev):
        pass
    def motion(self, c, ev):
        if ev.is_hint:
            x, y, state = ev.window.get_pointer()
        else:
            x = ev.x
            y = ev.y
        a = c.get_allocation()
        y -= a.y - self.offset
        x = int(x); y = int(y)
        if not self.tracking:
            if y > ((self.percent * self.theight / 100)
                    + self.lineheight/2):
                self.tracking = True
            else:
                return
        percent = (y - self.lineheight/2) * 100 / self.theight
        if percent < 0:
            percent = 0
        if percent > 100:
            percent = 100
        self.set(percent)
    
class PlayerWindow(gtk.Window):
    UPDATE_INTERVAL = 500
    def __init__(self, db):
        gtk.Window.__init__(self)
        self.set_default_size(480, 640)
        self.set_title("Music Player")

        self.db = db

        self.volume = 100
        
        self.update_id = -1
        self.changed_id = -1
        self.seek_timeout_id = -1

        self.p_position = gst.CLOCK_TIME_NONE
        self.p_duration = gst.CLOCK_TIME_NONE

        self.create_ui()
        self.soonid = None

        self.player = GstPlayer()

        self.player.on_eos = self.on_eos

        db.changer(self.new_song)

        suspend.monitor(self.on_suspend, self.on_resume)

        def on_delete_event():
            self.player.stop()
            gtk.main_quit()
        self.connect('delete-event', lambda *x: on_delete_event())

    def on_eos(self, *a):
        self.player.stop()
        if not self.db.next() and not self.db.next():
            return
        (d,b,n) = self.db.curr_song()
        self.load_file("file://" + urllib.quote(os.path.join(d,b)))
        self.player.play()
        self.tw.refresh()
        #self.play_toggled()

    def on_suspend(self):
        self.player.stop()
        self.play_button.remove(self.play_button.child)
        self.play_button.add(self.play_image)
        return True

    def on_resume(self):
        pass

    def load_file(self, location):
        self.player.set_location(location)

    def create_ui(self):

        isize = gtk.icon_size_register("big",120,120)
        vbox = gtk.VBox(); vbox.show()
        self.add(vbox)

        hbox = gtk.HBox(); hbox.show()
        vbox.pack_start(hbox, expand=False)

        fd = pango.FontDescription("sans 10")
        fd.set_absolute_size(25 * pango.SCALE)
        b = gtk.Button("Seek");  b.show()
        b.add_events(gtk.gdk.POINTER_MOTION_MASK|gtk.gdk.POINTER_MOTION_HINT_MASK)
        b.child.modify_font(fd)
        b.connect('button_press_event', self.grab_seek)
        b.connect('button_release_event', self.release_seek)
        hbox.add(b)

        b = gtk.Button("Volume");  b.show()
        b.add_events(gtk.gdk.POINTER_MOTION_MASK|gtk.gdk.POINTER_MOTION_HINT_MASK)
        b.child.modify_font(fd)
        b.connect('button_press_event', self.grab_volume)
        b.connect('button_release_event', self.release_volume)
        hbox.add(b)
        hbox.set_homogeneous(True); hbox.set_size_request(-1,70)

        hbox = gtk.HBox(); hbox.show()
        vbox.pack_end(hbox, fill=False, expand=False)
        # three button, prev, play/pause, next

        image = gtk.image_new_from_stock(gtk.STOCK_MEDIA_REWIND,
                                         isize)
        image.show()
        button = gtk.Button()
        button.add(image)
        button.show()
        hbox.pack_start(button)
        button.set_focus_on_click(False)
        button.connect('clicked', lambda *args: self.prev_song())
        

        image = gtk.image_new_from_stock(gtk.STOCK_MEDIA_FORWARD,
                                         isize)
        image.show()
        button = gtk.Button()
        button.add(image)
        button.show()
        hbox.pack_end(button)
        button.connect('clicked', lambda *args: self.next_song())

        image = gtk.image_new_from_stock(gtk.STOCK_MEDIA_PAUSE,
                                         isize)
        image.show()
        button = gtk.Button()
        button.show()
        hbox.pack_end(button)
        self.pause_image = image
        image = gtk.image_new_from_stock(gtk.STOCK_MEDIA_PLAY,
                                         isize)
        image.show()
        self.play_image = image
        self.play_button = button
        button.add(image)
        button.connect('clicked', lambda *args: self.play_toggled())


        self.dirlabel = gtk.Label("<Album>"); self.dirlabel.show()
        vbox.pack_start(self.dirlabel, expand=False)
        fd = self.dirlabel.get_pango_context().get_font_description()
        fd.set_absolute_size(25 * pango.SCALE)
        self.dirlabel.modify_font(fd)

        self.dirlabel.modify_fg(gtk.STATE_NORMAL, gtk.gdk.color_parse('magenta'))
        self.dirlabel.set_property('ellipsize', pango.ELLIPSIZE_START)

        self.songlabel = gtk.Label("No Song Playing"); self.songlabel.show()
        vbox.pack_end(self.songlabel, expand=False)
        self.db.changer(lambda : self.songlabel.set_text(self.db.title()))

        self.songlabel.modify_font(fd)
        self.songlabel.set_property('ellipsize', pango.ELLIPSIZE_MIDDLE)
        self.songlabel.modify_fg(gtk.STATE_NORMAL, gtk.gdk.color_parse('magenta'))

        h = gtk.HBox(); h.show()
        self.volumer = FingerScale(self.control_volume)
        h.add(self.volumer)
        self.tw = TitleWindow(self.db); self.tw.show()
        h.add(self.tw)
        self.seeker = FingerScale(self.control_seek)
        h.add(self.seeker)

        vbox.pack_end(h, padding=5)

    def play_toggled(self):
        self.play_button.remove(self.play_button.child)
        if self.player.is_playing():
            self.player.pause()
            self.play_button.add(self.play_image)
        else:
            self.player.play()
            #if self.update_id == -1:
            #    self.update_id = gobject.timeout_add(self.UPDATE_INTERVAL,
            #                                         self.update_scale_cb)
            self.play_button.add(self.pause_image)

    def soon_play(self, d,b):
        # play d/b in 40msec if nothing else is suggested
        self.player.stop()
        if self.soonid != None:
            gobject.source_remove(self.soonid)
        self.soonfile = "file://" + urllib.quote(os.path.join(d,b))
        self.soonid = gobject.timeout_add(40, self.soon)
    def soon(self):
        self.soonid = None
        self.load_file(self.soonfile)
        self.player.stop()
        self.play_toggled()

    def next_song(self):
        if self.player.is_playing():
            self.player.stop()
        if not self.db.next():
            self.db.next()

    def prev_song(self):
        if self.player.is_playing():
            pos,dur = self.player.query_position()
            self.player.stop()
            if pos > 2*1000*1000*1000:
                # 2 billion nanoseconds
                self.player.seek(0)
                self.player.play()
                return
        if not self.db.prev():
            self.db.prev()


    def new_song(self):
        (d,b,n) = self.db.curr_song()
        self.tw.refresh()
        self.dirlabel.set_text(urllib.unquote_plus(d))
        self.soon_play(d,b)

    def scale_format_value_cb(self, scale, value):
        if self.p_duration == -1:
            real = 0
        else:
            real = value * self.p_duration / 100
        
        seconds = real / gst.SECOND

        return "%02d:%02d" % (seconds / 60, seconds % 60)

    def scale_button_press_cb(self, widget, event):
        # see seek.c:start_seek
        gst.debug('starting seek')
        
        self.play_button.set_sensitive(False)
        self.was_playing = self.player.is_playing()
        if self.was_playing:
            self.player.pause()

        # don't timeout-update position during seek
        if self.update_id != -1:
            gobject.source_remove(self.update_id)
            self.update_id = -1

        # make sure we get changed notifies
        if self.changed_id == -1:
            self.changed_id = self.hscale.connect('value-changed',
                self.scale_value_changed_cb)
            
    def scale_value_changed_cb(self, scale):
        # see seek.c:seek_cb
        real = long(scale.get_value() * self.p_duration / 100) # in ns
        gst.debug('value changed, perform seek to %r' % real)
        self.player.seek(real)
        # allow for a preroll
        self.player.get_state(timeout=50*gst.MSECOND) # 50 ms

    def scale_button_release_cb(self, widget, event):
        # see seek.cstop_seek
        widget.disconnect(self.changed_id)
        self.changed_id = -1

        self.play_button.set_sensitive(True)
        if self.seek_timeout_id != -1:
            gobject.source_remove(self.seek_timeout_id)
            self.seek_timeout_id = -1
        else:
            gst.debug('released slider, setting back to playing')
            if self.was_playing:
                self.player.play()

        if self.update_id != -1:
            self.error('Had a previous update timeout id')
        else:
            self.update_id = gobject.timeout_add(self.UPDATE_INTERVAL,
                self.update_scale_cb)

    def update_scale_cb(self):
        self.p_position, self.p_duration = self.player.query_position()
        if self.p_position != gst.CLOCK_TIME_NONE:
            value = self.p_position * 100.0 / self.p_duration
            self.adjustment.set_value(value)

        return True

    def grab_seek(self, w, *a):
        self.p_position, self.p_duration = self.player.query_position()
        percent = self.p_position * 100 / self.p_duration
        self.seeker.start(percent, w)
    def release_seek(self, *a):
        self.seeker.grab_remove()
        self.seeker.hide()
    def control_seek(self, percent, commit = False):
        # return  "string"
        pos = percent * self.p_duration / 100
        seconds = pos / gst.SECOND
        str = "%02d:%02d" % (seconds / 60, seconds % 60)
        if commit:
            self.player.seek(pos)
        return str

    def grab_volume(self, w, *a):
        a = w.get_allocation()
        self.volumer.start(self.volume, w)
    def release_volume(self, *a):
        self.volumer.grab_remove()
        self.volumer.hide()
    def control_volume(self, percent, commit = False):
        self.volume = percent
        self.player.set_volume(percent)
        str = "%d%%" % percent
        return str

def main(args):

    # Need to register our derived widget types for implicit event
    # handlers to get called.

    gobject.type_register(PlayerWindow)

    db = MusicList("/home/music/ogg")
    w = PlayerWindow(db)
    w.show()

    w.on_eos()
    gtk.main()

if __name__ == '__main__':
    sys.exit(main(sys.argv))
