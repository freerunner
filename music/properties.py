
import gtk

class RootProp():
    def __init__(self):
        self.root = gtk.gdk.get_default_root_window()

    def setstr(self, name, val):
        self.root.property_change(name, "STRING", 8,
                                  gtk.gdk.PROP_MODE_REPLACE, val)

    def getstr(self, name):
        (type, format, value) = self.root.property_get(name)
        if type != "STRING" or format != 8:
            return None
        return value

    def watchstr(self, name, fn):
        m = self.root.get_events()
        self.root.set_events(m | gtk.gdk.PROPERTY_CHANGE_MASK)
        self.root.add_filter(self.gotev, True)

    def gotev(self, ev, tr):
        print ev, dir(ev)
        print ev.type, ev.get_state()
        if ev.type == gtk.gdk.PROPERTY_NOTIFY:
            print ev.atom
        else:
            print ev.type

        ev2 = gtk.gdk.event_get()
        print "and", ev2.type
        return gtk.gdk.FILTER_CONTINUE

def ping(*a):
    print 'ping'

a= RootProp()
a.watchstr('song', ping)
gtk.main()
