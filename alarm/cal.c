#define _XOPEN_SOURCE
#define _GNU_SOURCE

#include <unistd.h>
#include <stdlib.h>

#include <string.h>
#include <malloc.h>
#include <math.h>
#include <time.h>

#include <gtk/gtk.h>

#include "listsel.h"

struct event {
	struct event *next;
	time_t when, first;
	long recur;
	char *mesg;
};


char *days[] = { "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat" };

GtkWidget *calblist[42], *month, *date_display, *date_time_display;
GtkWidget *window, *clockw, *cal, *reasonw, *timerw;
GtkWidget *browse_buttons, *event_buttons, *move_buttons, *move_event;
GtkWidget *timer_display, *time_display;
GtkWidget *today_btn, *undelete_btn;
GtkWidget *once_btn, *weekly_btn, *fort_btn, *freq_buttons;
GtkWidget *timers_list;
GtkTextBuffer *reason_buffer;
time_t dlist[42], chosen_date;
int prev_mday, hour, minute;
int freq;

struct event *evlist, *active_event;
struct event *deleted_event;
struct list_entry_text *alarm_entries;
time_t alarm_date = 0;
int evcnt = -1;
int moving = 0;
struct sellist *alarm_selector;

char *filename = NULL;

PangoLayout *layout;
GdkGC *colour = NULL;

int size(struct list_entry *i, int *width, int*height)
{
	PangoRectangle ink, log;
	struct list_entry_text *item = (void*)i;

	if (i->height) {
		*width = item->true_width;
		*height = i->height;
		return 0;
	}
	pango_layout_set_text(layout, item->text, -1);
	pango_layout_get_extents(layout, &ink, &log);
	*width = log.width / PANGO_SCALE;
	*height = log.height / PANGO_SCALE;
	item->true_width = i->width = *width;
	i->height = *height;
	return 0;
}

int render(struct list_entry *i, int selected, GtkWidget *d)
{
	PangoRectangle ink, log;
	struct list_entry_text *item = (void*)i;
	int x;
	GdkColor col;

	pango_layout_set_text(layout, item->text, -1);

	if (colour == NULL) {
		colour = gdk_gc_new(gtk_widget_get_window(d));
		gdk_color_parse("purple", &col);
		gdk_gc_set_rgb_fg_color(colour, &col);
	}
	if (selected) {
		gdk_color_parse("pink", &col);
		gdk_gc_set_rgb_fg_color(colour, &col);
		gdk_draw_rectangle(gtk_widget_get_window(d),
				   colour, TRUE,
				   item->head.x, item->head.y,
				   item->head.width, item->head.height);
		gdk_color_parse("purple", &col);
		gdk_gc_set_rgb_fg_color(colour, &col);
	}
#if 0
	x = (i->width - item->true_width)/2;
	if (x < 0)
		x = 0;
#else
	x = 0;
#endif
	gdk_draw_layout(gtk_widget_get_window(d),
			colour,
			item->head.x+x, item->head.y, layout);
	return 0;
}



void set_cal(time_t then);

void set_date(GtkButton *button, int num)
{
	set_cal(dlist[num]);
}

void prev_year(GtkButton *button)
{
	set_cal(chosen_date - 365*24*3600);
}
void next_year(GtkButton *button)
{
	set_cal(chosen_date + 365*24*3600);
}

void finish(void)
{
	gtk_main_quit();
}

void set_time(GtkButton *button, int num)
{
	struct tm now;
	time_t then;
	char buf[100];
	int hr24, hr12;
	char m = 'A';
	if (num >= 100)
		hour = num/100;
	else
		minute = num;

	hr24 = hour;
	if (hour == 24)
		hr24 = 0;
	hr12 = hour;
	if (hr12 > 12) {
		hr12 -= 12;
		m = 'P';
	}
	if (hr12 == 12)
		m = 'P' + 'A' - m;

	then = chosen_date + hour * 3600 + minute * 60;
	localtime_r(&then, &now);
	strftime(buf, sizeof(buf), "%a, %d %B %Y, %H:%M", &now);
	gtk_label_set_text(GTK_LABEL(date_display), buf);
	gtk_label_set_text(GTK_LABEL(date_time_display), buf);
#if 0
	sprintf(buf, "%02d:%02dhrs\n%2d:%02d%cM", hr24, minute,
		hr12, minute, m);
	gtk_label_set_text(GTK_LABEL(time_label), buf);
#endif
}

GtkWidget *add_button(GtkWidget **blist, char *name,
		PangoFontDescription *fd,
		GCallback handle)
{
	GtkWidget *l, *b;
	if (*blist == NULL) {
		*blist = gtk_hbox_new(TRUE, 0);
		gtk_widget_show(*blist);
		gtk_widget_set_size_request(*blist, -1, 80);
	}
	
	l = *blist;

	b = gtk_button_new_with_label(name);
	gtk_widget_show(b);
	pango_font_description_set_size(fd, 12 * PANGO_SCALE);
	gtk_widget_modify_font(GTK_BIN(b)->child, fd);
	gtk_container_add(GTK_CONTAINER(l), b);
	g_signal_connect((gpointer)b, "clicked", handle, NULL);
	return b;
}
int delay = 0;
int show_time(void *d);
int timer_tick = -1;
void add_time(int mins);

void load_timers(void);
void select_timer(void)
{
	gtk_container_remove(GTK_CONTAINER(window), cal);
	show_time(NULL);
	delay = 0;
	add_time(0);
	load_timers();
	gtk_container_add(GTK_CONTAINER(window), timerw);
	timer_tick = g_timeout_add(1000, show_time, NULL);
}

void events_select(void)
{
	/* the selected event becomes current and can be moved */
	if (!active_event)
		return;
	set_cal(active_event->when);
}

void move_confirm(void)
{
	struct tm now;
	time_t then;
	char buf[100];

	if (active_event) {
		then = active_event->when;
		localtime_r(&then, &now);
		hour = now.tm_hour;
		minute = now.tm_min;
	} else
		hour = minute = 0;
	then = chosen_date + hour * 3600 + minute * 60;
	localtime_r(&then, &now);
	strftime(buf, sizeof(buf), "%a, %d %B %Y, %H:%M", &now);
	gtk_label_set_text(GTK_LABEL(date_display), buf);
	gtk_label_set_text(GTK_LABEL(date_time_display), buf);

	gtk_container_remove(GTK_CONTAINER(window), cal);
	gtk_container_add(GTK_CONTAINER(window), clockw);
}

void update_freq(void);
void events_move(void)
{
	if (!active_event)
		return;

	moving = 1;
	freq = active_event->recur;
	set_cal(active_event->when);

	gtk_widget_hide(event_buttons);
	gtk_widget_show(move_buttons);
	update_freq();
	gtk_widget_show(freq_buttons);

	gtk_widget_hide(alarm_selector->drawing);
	gtk_widget_show(move_event);

	gtk_text_buffer_set_text(reason_buffer, active_event->mesg, -1);
}

void events_new(void)
{
	moving = 2;
	freq = 0;
	gtk_widget_hide(event_buttons);
	gtk_widget_hide(browse_buttons);
	gtk_widget_show(move_buttons);
	update_freq();
	gtk_widget_show(freq_buttons);

	gtk_widget_hide(alarm_selector->drawing);
	gtk_widget_show(move_event);
	gtk_text_buffer_set_text(reason_buffer, "", -1);
}

void move_abort(void)
{
	gtk_widget_hide(move_buttons);
	gtk_widget_hide(freq_buttons);
	if (active_event) {
		gtk_widget_hide(browse_buttons);
		gtk_widget_show(event_buttons);
	} else {
		gtk_widget_hide(event_buttons);
		gtk_widget_show(browse_buttons);
	}

	gtk_widget_show(alarm_selector->drawing);
	gtk_widget_hide(move_event);
	moving = 0;
	if (active_event)
		set_cal(active_event->when);
	else
		set_cal(chosen_date);

}

void cal_today(void)
{
	/* jump to today */
	time_t now;
	time(&now);
	set_cal(now);
	if (deleted_event) {
		gtk_widget_hide(today_btn);
		gtk_widget_show(undelete_btn);
	}
}

void cal_restore(void)
{
	gtk_container_remove(GTK_CONTAINER(window), clockw);
	gtk_container_add(GTK_CONTAINER(window), cal);
}

void reason_confirm(void);
void clock_confirm(void)
{
	gtk_container_remove(GTK_CONTAINER(window), clockw);
	gtk_container_add(GTK_CONTAINER(window), reasonw);
	reason_confirm();
}

void update_freq(void)
{
	if (freq == 0)
		gtk_label_set_markup(GTK_LABEL(GTK_BIN(once_btn)->child), "<span background=\"pink\">Once</span>");
	else
		gtk_label_set_markup(GTK_LABEL(GTK_BIN(once_btn)->child), "Once");

	if (freq == 7*24*3600)
		gtk_label_set_markup(GTK_LABEL(GTK_BIN(weekly_btn)->child), "<span background=\"pink\">Weekly</span>");
	else
		gtk_label_set_markup(GTK_LABEL(GTK_BIN(weekly_btn)->child), "Weekly");

	if (freq == 14*24*3600)
		gtk_label_set_markup(GTK_LABEL(GTK_BIN(fort_btn)->child), "<span background=\"pink\">Fortnightly</span>");
	else
		gtk_label_set_markup(GTK_LABEL(GTK_BIN(fort_btn)->child), "Fortnightly");
}
void set_once(void)
{
	freq = 0;
	update_freq();
}
void set_weekly(void)
{
	freq = 7 * 24 * 3600;
	update_freq();
}
void set_fort(void)
{
	freq = 14 * 24 * 3600;
	update_freq();
}

/********************************************************************/


void event_free(struct event *ev)
{
	while (ev) {
		struct event *n = ev->next;
		free(ev->mesg);
		free(ev);
		ev = n;
	}
}

void update_event(struct event *ev, time_t now)
{
	/* make sure 'when' is in the future if possible */
	ev->when = ev->first;
	while (ev->when < now && ev->recur > 0)
		ev->when += ev->recur;
}

void update_events(struct event *ev, time_t now)
{
	while (ev) {
		update_event(ev, now);
		ev = ev->next;
	}
}

void sort_events(struct event **evtp)
{
	/* sort events list in *evtp by ->when
	 * use merge sort
	 */
	int cnt=0;

	struct event *ev[2];
	ev[0] = *evtp;
	ev[1] = NULL;

	do {
		struct event **evp[2], *e[2];
		int current = 0;
		time_t prev = 0;
		int next = 0;
		cnt++;
		evp[0] = &ev[0];
		evp[1] = &ev[1];
		e[0] = ev[0];
		e[1] = ev[1];

		/* take least of e[0] and e[1]
		 * if it is larger than prev, add to
		 * evp[current], else swap current then add
		 */
		while (e[0] || e[1]) {
			if (e[next] == NULL ||
			    (e[1-next] != NULL && 
			     !((prev <= e[1-next]->when)
			       ^(e[1-next]->when <= e[next]->when)
			       ^(e[next]->when <= prev)))
				)
				next = 1 - next;

			if (e[next]->when < prev)
				current = 1 - current;
			prev = e[next]->when;
			*evp[current] = e[next];
			evp[current] = &e[next]->next;
			e[next] = e[next]->next;
		}
		*evp[0] = NULL;
		*evp[1] = NULL;
	} while (ev[0] && ev[1]);
	if (ev[0])
		*evtp = ev[0];
	else
		*evtp = ev[1];
}

struct event *event_parse(char *line)
{
	struct event *rv;
	char *recur, *mesg;
	struct tm tm;
	long rsec;
	recur = strchr(line, ':');
	if (!recur)
		return NULL;
	*recur++ = 0;
	mesg = strchr(recur, ':');
	if (!mesg)
		return NULL;
	*mesg++ = 0;
	line = strptime(line, "%Y-%m-%d-%H-%M-%S", &tm);
	if (!line)
		return NULL;
	rsec = atoi(recur);
	if (rsec < 0)
		return NULL;
	rv = malloc(sizeof(*rv));
	rv->next = NULL;
	tm.tm_isdst = -1;
	rv->when = mktime(&tm);
	rv->first = rv->when;
	rv->recur = rsec;
	rv->mesg = strdup(mesg);
	return rv;
}

struct event *event_load_all(char *file)
{
	/* load the file and return a linked list */
	char line[2048];
	struct event *rv = NULL, *ev;
	FILE *f;

	f = fopen(file, "r");
	if (!f)
		return NULL;
	while (fgets(line, sizeof(line), f) != NULL) {
		char *eol = line + strlen(line);
		if (eol > line && eol[-1] == '\n')
			eol[-1] = 0;

		ev = event_parse(line);
		if (!ev)
			continue;
		ev->next = rv;
		rv = ev;
	}
	return rv;
}

void event_save_all(char *file, struct event *list)
{
	FILE *f;
	char *tmp = strdup(file);
	char *c;

	c = tmp + strlen(tmp);
	while (c > tmp && c[-1] != '/')
		c--;
	if (*c) *c = '.';

	f = fopen(tmp, "w");
	while (list) {
		struct event *ev = list;
		struct tm *tm;
		char buf[100];
		list = ev->next;

		tm = localtime(&ev->first);
		strftime(buf, sizeof(buf), "%Y-%m-%d-%H-%M-%S", tm);
		fprintf(f, "%s:%d:%s\n", buf, ev->recur, ev->mesg);
	}
	fflush(f);
	fsync(fileno(f));
	fclose(f);
	rename(tmp, file);
}


void load_timers(void)
{
	time_t now = time(0);
	struct event *timers = event_load_all("/etc/alarms/timer");
	struct event *t;
	char buf[1024];
	int len = 0;

	update_events(timers, now);
	sort_events(&timers);
	strcpy(buf,"");
	for (t = timers ; t; t = t->next) {
		struct tm *tm;
		if (t->when < now)
			continue;
		tm = localtime(&t->when);
		strftime(buf+len, sizeof(buf)-len, "%H:%M\n", tm);
		len += strlen(buf+len);
	}
	event_free(timers);
	gtk_label_set_text(GTK_LABEL(timers_list), buf);
}


struct list_entry *alarms_item(void *list, int n)
{
	struct event *ev;

	if (alarm_date != chosen_date) {
		int i;
		struct tm *tm, today;

		if (evlist == NULL) {
			filename = "/home/neilb/ALARMS";
			evlist = event_load_all(filename);
			if (evlist == NULL) {
				filename = "/etc/alarms/cal";
				evlist = event_load_all(filename);
			}
		}
		update_events(evlist, chosen_date);
		sort_events(&evlist);
		evcnt = 0;
		ev = evlist;
		while (ev && ev->when < chosen_date)
			ev = ev->next;
		for ( ; ev ; ev = ev->next)
			evcnt++;
		free(alarm_entries);
		alarm_entries = calloc(evcnt, sizeof(alarm_entries[0]));
		ev = evlist;
		while (ev && ev->when < chosen_date)
			ev = ev->next;
		tm = localtime(&chosen_date);
		today = *tm;
		for (i=0; ev ; i++, ev = ev->next) {
			char buf[50], o, c, r;
			struct tm *tm = localtime(&ev->when);
			if (tm->tm_mday == today.tm_mday &&
			    tm->tm_mon == today.tm_mon &&
			    tm->tm_year == today.tm_year)
				strftime(buf, 50, "%H:%M\t", tm);
			else if (tm->tm_year == today.tm_year ||
				 (tm->tm_year == today.tm_year + 1 &&
				  tm->tm_mon < today.tm_mon)
				)
				strftime(buf, 50, "%b%d\t", tm);
			else
				strftime(buf, 50, "%Y\t", tm);

			o = ' ';c=' ';
			if (ev->when < time(0)) {
				o = '(';
				c = ')';
			}
			if (ev->recur == 0)
				r = ' ';
			else if (ev->recur == 3600*24*7)
				r = '+'; /* weekly */
			else if (ev->recur == 3600*24*14)
				r = '*'; /* fortnightly */
			else
				r = '#'; /* other period */
			asprintf(&alarm_entries[i].text, "%c%c %s %s%c",
				 o, r,
				 buf, ev->mesg, c);
			alarm_entries[i].bg = "white";
			alarm_entries[i].fg = "blue";
			alarm_entries[i].underline = 0;
		}
		alarm_date = chosen_date;
	}
	if (n >= evcnt)
		return NULL;

	return &alarm_entries[n].head;
}


void events_delete(void)
{
	struct event **evp;
	if (!active_event)
		return;

	for (evp = &evlist; *evp; evp = &(*evp)->next) {
		if (*evp != active_event)
			continue;
		*evp = active_event->next;
		break;
	}
	if (deleted_event) {
		free(deleted_event->mesg);
		free(deleted_event);
	}
	deleted_event = active_event;
	active_event = NULL;
	event_save_all(filename, evlist);
	alarm_date = 0;
	move_abort();
}

void events_undelete(void)
{
	if (!deleted_event)
		return;
	active_event = deleted_event;
	deleted_event = NULL;
	active_event->next = evlist;
	evlist = active_event;
	event_save_all(filename, evlist);
	alarm_date = 0;
	move_abort();
}

void alarms_selected(void *list, int s)
{
	struct event *e;


	if (s < 0 || s >= evcnt)
		return;
	e = evlist;
	while (e && e->when < chosen_date)
		e = e->next;
	while (s && e) {
		s--;
		e = e->next;
	}
	active_event = e;

	gtk_widget_hide(browse_buttons);
	gtk_widget_show(event_buttons);
}

struct list_handlers alarms_han = {
	.getitem = alarms_item,
	.get_size = size,
	.render = render,
	.selected = alarms_selected,
};


GtkWidget *create_cal_window(void)
{
	GtkWidget *v, *t, *l;
	GtkWidget *h;
	GtkWidget *blist = NULL;
	int i,r,c;
	PangoFontDescription *desc;
	struct sellist *sl;

	desc = pango_font_description_new();
	pango_font_description_set_size(desc, 11 * PANGO_SCALE);

	v = gtk_vbox_new(FALSE, 0);
	gtk_widget_show(v);

	h = gtk_hbox_new(FALSE, 0);
	gtk_container_add_with_properties(GTK_CONTAINER(v), h, "expand", 0, NULL);
	gtk_widget_show(h);

	l = gtk_button_new_with_label(" << ");
	gtk_widget_modify_font(GTK_BIN(l)->child, desc);
	gtk_button_set_relief(GTK_BUTTON(l), GTK_RELIEF_NONE);
	g_signal_connect((gpointer)l, "clicked",
			 G_CALLBACK(prev_year), NULL);
	gtk_container_add(GTK_CONTAINER(h), l);
	gtk_widget_show(l);
	gtk_widget_set(l, "can-focus", 0, NULL);

	l = gtk_label_new("Month 9999");
	gtk_widget_modify_font(l, desc);
	gtk_container_add(GTK_CONTAINER(h), l);
	gtk_widget_show(l);
	month = l;


	l = gtk_button_new_with_label(" >> ");
	gtk_widget_modify_font(GTK_BIN(l)->child, desc);
	gtk_button_set_relief(GTK_BUTTON(l), GTK_RELIEF_NONE);
	g_signal_connect((gpointer)l, "clicked",
			 G_CALLBACK(next_year), NULL);
	gtk_container_add(GTK_CONTAINER(h), l);
	gtk_widget_show(l);
	gtk_widget_set(l, "can-focus", 0, NULL);


	t = gtk_table_new(7, 7, FALSE);
	gtk_widget_show(t);

	gtk_container_add_with_properties(GTK_CONTAINER(v), t, "expand", 0, NULL);

	for (i=0; i<7; i++) {
		l = gtk_label_new(days[i]);
		gtk_widget_modify_font(l, desc);
		gtk_widget_show(l);
		gtk_table_attach(GTK_TABLE(t), l, i, i+1, 0, 1, GTK_EXPAND | GTK_FILL, GTK_FILL, 0, 0);
	}

	for (r=1; r<7; r++)
		for (c=0; c<7; c++) {
			int p = (r-1)*7+c;
			l = gtk_button_new_with_label(" 99 ");
			gtk_widget_modify_font(GTK_BIN(l)->child, desc);
			gtk_button_set_relief(GTK_BUTTON(l), GTK_RELIEF_NONE);
			gtk_table_attach(GTK_TABLE(t), l, c,c+1, r,r+1, GTK_FILL, GTK_FILL, 0, 0);
			gtk_widget_show(l);
			g_signal_connect((gpointer)l, "clicked",
					 G_CALLBACK(set_date), (void*)(long)p);
			calblist[p] = l;
			dlist[p] = 0;
		}

	/* list of alarms from this date */
	sl = listsel_new(NULL, &alarms_han);
	pango_font_description_set_size(desc, 15 * PANGO_SCALE);
	gtk_widget_modify_font(sl->drawing, desc);
	gtk_container_add(GTK_CONTAINER(v), sl->drawing);
	gtk_widget_show(sl->drawing);
	alarm_selector = sl;

	l = gtk_text_view_new_with_buffer(reason_buffer);
	gtk_widget_modify_font(l, desc);
	gtk_text_view_set_wrap_mode(GTK_TEXT_VIEW(l), GTK_WRAP_WORD_CHAR);
	gtk_widget_hide(l);
	gtk_container_add(GTK_CONTAINER(v), l);
	move_event = l;


	add_button(&blist, "timer", desc, select_timer);
	add_button(&blist, "new", desc, events_new);
	today_btn = add_button(&blist, "today", desc, cal_today);
	undelete_btn = add_button(&blist, "undelete", desc, events_undelete);
	gtk_widget_hide(undelete_btn);
	gtk_box_pack_end(GTK_BOX(v), blist, FALSE, FALSE, 0);
	browse_buttons = blist;
	blist = NULL;

	add_button(&blist, "go to", desc, events_select);
	add_button(&blist, "change", desc, events_move);
	add_button(&blist, "delete", desc, events_delete);
	gtk_box_pack_end(GTK_BOX(v), blist, FALSE, FALSE, 0);
	event_buttons = blist;
	blist = NULL;

	add_button(&blist, "confirm", desc, move_confirm);
	add_button(&blist, "abort", desc, move_abort);
	gtk_widget_hide(blist);
	gtk_box_pack_end(GTK_BOX(v), blist, FALSE, FALSE, 0);

	move_buttons = blist;

	blist = NULL;
	once_btn = add_button(&blist, "Once", desc, set_once);
	weekly_btn = add_button(&blist, "Weekly", desc, set_weekly);
	fort_btn = add_button(&blist, "Fortnightly", desc, set_fort);
	gtk_widget_hide(blist);
	gtk_box_pack_end(GTK_BOX(v), blist, FALSE, FALSE, 0);
	freq_buttons = blist;

	return v;
}

void set_cal(time_t then)
{
	struct tm now, first, today, *tm;
	int d, x;
	time_t today_s;
	char buf[400];

	time(&today_s);
	localtime_r(&today_s, &today);
	localtime_r(&then, &now);

	then -= now.tm_sec;
	then -= now.tm_min*60;
	then -= now.tm_hour*3600;
	chosen_date = then;

	localtime_r(&then, &now);

	tm = localtime(&then);

	strftime(buf, sizeof(buf), "%a, %d %B %Y", &now);
	gtk_label_set_text(GTK_LABEL(date_display), buf);
	
	/* previous month */
	while (tm->tm_mon == now.tm_mon) {
		then -= 22*3600;
		tm = localtime(&then);
	}
	/* Start of week */
	while (tm->tm_wday != 0) {
		then -= 22*3600;
		tm = localtime(&then);
	}
	first = *tm;

	if (abs(dlist[0] - then) > 48*3600) {
		strftime(buf, 40, "%B %Y", &now);
		gtk_label_set_text(GTK_LABEL(month), buf);
	}

	for (d=0; d<42; d++) {
		char *bg = "", *fg = "black", *today_fg = "blue";

		if (tm->tm_mon != now.tm_mon) {
			fg = "grey"; today_fg = "pink";
		} else if (tm->tm_mday == now.tm_mday)
			bg = "background=\"green\"";

		if (tm->tm_year == today.tm_year &&
		    tm->tm_mon == today.tm_mon &&
		    tm->tm_mday == today.tm_mday)
			fg = today_fg;

		sprintf(buf, "<span %s foreground=\"%s\"> %02d </span>",
			bg, fg, tm->tm_mday);
		if (abs(dlist[d] - then) > 48*3600 ||
		    tm->tm_mday == now.tm_mday ||
		    tm->tm_mday == prev_mday)
			gtk_label_set_markup(GTK_LABEL(GTK_BIN(calblist[d])->child), buf);
		dlist[d] = then;
		x = tm->tm_mday;
		while (x == tm->tm_mday) {
			then += 22*3600;
			tm = localtime(&then);
		}
	}
	prev_mday = now.tm_mday;

	alarm_selector->selected = -1;
	if (!moving) {
		active_event = NULL;
		gtk_widget_hide(event_buttons);
		gtk_widget_show(browse_buttons);
	}
	g_signal_emit_by_name(alarm_selector->drawing, "configure-event",
			      NULL, alarm_selector);


	gtk_widget_show(today_btn);
	gtk_widget_hide(undelete_btn);
}

void center_me(GtkWidget *label, void *xx, int pos)
{
	/* I have just been realised.  Find size and
	 * adjust position
	 */
	GtkWidget *button = gtk_widget_get_parent(label);
	GtkWidget *parent = gtk_widget_get_parent(button);
	GtkAllocation alloc;
	int x = pos / 10000;
	int y = pos % 10000;

	gtk_widget_get_allocation(button, &alloc);
	printf("move %d %d/%d by %d/%d from %d/%d\n", pos, x, y,
	       alloc.width/2, alloc.height/2, alloc.x, alloc.y);
	x -= alloc.width / 2;
	y -= alloc.height / 2;
	if (x != alloc.x || y != alloc.y) {
	printf("move %d %d/%d by %d/%d from %d/%d\n", pos, x, y,
	       alloc.width/2, alloc.height/2, alloc.x, alloc.y);
		gtk_fixed_move(GTK_FIXED(parent), button, x, y);
	}
}


void add_nums(GtkWidget *f, int radius, int start, int end, int step,
	      int div, int scale,
	      char *colour, PangoFontDescription *desc)
{
	int i;
	for (i=start; i<end; i+= step) {
		char buf[400];
		double a, s, c, r;
		int x, y;
		GtkWidget *l;
		long scaled;
		sprintf(buf, "<span background=\"%s\">%02d</span>", colour, i);
		l = gtk_button_new_with_label(" 99 ");
		gtk_widget_modify_font(GTK_BIN(l)->child, desc);
		gtk_label_set_markup(GTK_LABEL(GTK_BIN(l)->child),
				     buf);
		gtk_widget_show(l);
		scaled = i * scale;
		g_signal_connect((gpointer)l, "clicked",
				 G_CALLBACK(set_time), (void*)scaled);
		a = i * 2.0 * M_PI / div;
		s = sin(a); c= cos(a);
		r = (double)radius;
		if (fabs(s) < fabs(c))
			r = r / fabs(c);
		else
			r = r / fabs(s);
		x = 210 + (int)(r * s) - r/18;
		y = 210 - (int)(r * c) - r/18;
		gtk_fixed_put(GTK_FIXED(f), l, x, y);
	}
}
		
GtkWidget *create_clock_window(void)
{
	PangoFontDescription *desc;
	GtkWidget *f, *l, *v;
	GtkWidget *blist = NULL;

	desc = pango_font_description_new();
	
	v = gtk_vbox_new(FALSE, 0);
	gtk_widget_show(v);

	l = gtk_label_new(" Date "); /* Date for which time is being chosen */
	pango_font_description_set_size(desc, 12 * PANGO_SCALE);
	gtk_widget_modify_font(l, desc);
	gtk_container_add_with_properties(GTK_CONTAINER(v), l, "expand", 0, NULL);
	gtk_widget_show(l);
	date_display = l;

	f = gtk_fixed_new();
	gtk_widget_show(f);
	gtk_container_add(GTK_CONTAINER(v), f);

	pango_font_description_set_size(desc, 17 * PANGO_SCALE);
	add_nums(f, 200, 6, 18, 1, 12, 100, "light green", desc);
	pango_font_description_set_size(desc, 15 * PANGO_SCALE);
	add_nums(f, 140, 1, 6, 1, 12, 100, "light blue", desc);
	add_nums(f, 140,18,25, 1, 12, 100, "light blue", desc);
	pango_font_description_set_size(desc, 12 * PANGO_SCALE);
	add_nums(f, 95, 0, 60, 5, 60, 1, "pink", desc);

#if 0
	l = gtk_label_new("09:00\n9:00AM");
	hour = 9;
	minute = 0;
	pango_font_description_set_size(desc, 12 * PANGO_SCALE);
	gtk_widget_modify_font(l, desc);
	gtk_widget_show(l);
	gtk_fixed_put(GTK_FIXED(f), l, 170,180);
	time_label = l;
#endif

	add_button(&blist, "confirm", desc, clock_confirm);
	add_button(&blist, "back", desc, cal_restore);
	gtk_box_pack_end(GTK_BOX(v), blist, FALSE, FALSE, 0);

	return v;
}

char *reasons[] = {"awake", "go", "meet", "buy", "call", "doctor", "dentist", "ok", "thanks", "coming"};
struct list_entry_text *reason_entries;
int selected_reason = -1;
int num_reasons;

struct list_entry *item(void *list, int n)
{
	if (n < num_reasons)
		return &reason_entries[n].head;
	else
		return NULL;
}
void selected(void *list, int n)
{
	selected_reason = n;
}

struct list_handlers reason_han =  {
	.getitem = item,
	.get_size = size,
	.render = render,
	.selected = selected,
};

void reason_back(void)
{
	gtk_container_remove(GTK_CONTAINER(window), reasonw);
	gtk_container_add(GTK_CONTAINER(window), clockw);
}

void reason_confirm(void)
{
	struct event *ev;
	GtkTextIter start, finish;

	if (!active_event) {
		ev = malloc(sizeof(*ev));
		ev->next = evlist;
		evlist = ev;
	} else {
		ev = active_event;
		free(ev->mesg);
	}
	ev->recur = freq;
	ev->when = ev->first = chosen_date + hour*3600 + minute*60;
	gtk_text_buffer_get_bounds(reason_buffer, &start, &finish);
	ev->mesg = gtk_text_buffer_get_text(reason_buffer,
					    &start, &finish, FALSE);
	gtk_container_remove(GTK_CONTAINER(window), reasonw);
	gtk_container_add(GTK_CONTAINER(window), cal);

	event_save_all(filename, evlist);

	active_event = ev;
	alarm_date = 0; /* force refresh */
	move_abort();
}

void reason_select(void)
{
	if (selected_reason < 0 ||
	    selected_reason >= num_reasons)
		return;
	gtk_text_buffer_delete_selection(reason_buffer, TRUE, TRUE);
	gtk_text_buffer_insert_at_cursor(reason_buffer, " ", 1);
	gtk_text_buffer_insert_at_cursor(reason_buffer, reasons[selected_reason],
					 strlen(reasons[selected_reason]));
}

GtkWidget *create_reason_window(void)
{
	/* The "reason" window allows the reason for the alarm
	 * to be set.
	 * It has:
	 *  a label to show date and time
	 *  a text editing widget containing the reason
	 *  a selectable list of canned reasons
	 *  buttons for: confirm select clear
	 *
	 * confirm adds the alarm, or not if the reason is clear
	 * select adds the selected canned reason to the editor
	 * clear clears the reason
	 */

	PangoFontDescription *desc;
	GtkWidget *v, *blist=NULL, *l;
	struct sellist *sl;
	int i, num;

	desc = pango_font_description_new();
	v = gtk_vbox_new(FALSE, 0);
	gtk_widget_show(v);

	l = gtk_label_new(" Date and Time ");
	pango_font_description_set_size(desc, 10 * PANGO_SCALE);
	gtk_widget_modify_font(l, desc);
	gtk_widget_show(l);
	gtk_container_add_with_properties(GTK_CONTAINER(v), l, "expand", 0, NULL);
	date_time_display = l;

	l = gtk_text_view_new_with_buffer(reason_buffer);
	gtk_widget_modify_font(l, desc);
	gtk_text_view_set_wrap_mode(GTK_TEXT_VIEW(l), GTK_WRAP_WORD_CHAR);
	gtk_widget_show(l);
	gtk_container_add(GTK_CONTAINER(v), l);
	gtk_text_buffer_set_text(reason_buffer, "Sample Reason", -1);

	num = sizeof(reasons)/sizeof(reasons[0]);
	reason_entries = calloc(num+1, sizeof(reason_entries[0]));
	for (i=0; i<num; i++) {
		reason_entries[i].text = reasons[i];
		reason_entries[i].bg = "white";
		reason_entries[i].fg = "blue";
		reason_entries[i].underline = 0;
	}
	num_reasons = num;

	sl = listsel_new(NULL, &reason_han);
	pango_font_description_set_size(desc, 15 * PANGO_SCALE);
	gtk_widget_modify_font(sl->drawing, desc);
	gtk_container_add(GTK_CONTAINER(v), sl->drawing);
	gtk_widget_show(sl->drawing);

	add_button(&blist, "confirm", desc, reason_confirm);
	add_button(&blist, "select", desc, reason_select);
	add_button(&blist, "back", desc, reason_back);
	gtk_box_pack_end(GTK_BOX(v), blist, FALSE, FALSE, 0);
	return v;
}

void add_time(int mins)
{
	time_t now;
	char buf[30];
	struct tm *tm;
	delay += mins;
	if (delay <= 0)
		delay = 0;
	now = time(0) + (delay?:1)*60;
	tm = localtime(&now);
	sprintf(buf, "+%d:%02d - ",
		delay/60, delay%60);
	strftime(buf+strlen(buf),
		 sizeof(buf)-strlen(buf),
		 "%H:%M", tm);
	gtk_label_set_text(GTK_LABEL(timer_display), buf);

}
void add_time_60(void) { add_time(60); }
void add_time_10(void) { add_time(10); }
void add_time_1(void) { add_time(1); }
void del_time_60(void) { add_time(-60); }
void del_time_10(void) { add_time(-10); }
void del_time_1(void) { add_time(-1); }

int show_time(void *d)
{
	time_t now;
	char buf[30];
	struct tm *tm;
	now = time(0);
	tm = localtime(&now);
	strftime(buf, sizeof(buf), "%H:%M:%S", tm);
	gtk_label_set_text(GTK_LABEL(time_display), buf);
}

void to_cal(void)
{
	gtk_container_remove(GTK_CONTAINER(window), timerw);
	g_source_remove(timer_tick);
	gtk_container_add(GTK_CONTAINER(window), cal);
}
void set_timer(void)
{
	FILE *f = fopen("/etc/alarms/timer", "a");
	char buf[100];
	time_t now = time(0) + delay*60;
	struct tm *tm = localtime(&now);

	strftime(buf, sizeof(buf), "%Y-%m-%d-%H-%M-%S::TIMER\n", tm);
	if (f) {
		fputs(buf, f);
		fclose(f);
	}
	to_cal();
}
void clear_timers(void)
{
	unlink("/etc/alarms/timer");
	to_cal();
}

GtkWidget *create_timer_window(void)
{
	/* The timer window lets you set a one-shot alarm
	 * some number of minutes in the future.
	 * There are buttons for +1hr +10 +1 and - all those
	 * The timer window also shows a large digital clock with seconds
	 */

	PangoFontDescription *desc;
	GtkWidget *v, *blist, *l;

	desc = pango_font_description_new();
	v = gtk_vbox_new(FALSE, 0);
	gtk_widget_show(v);

	l = gtk_label_new("  Timer ");
	pango_font_description_set_size(desc, 25 * PANGO_SCALE);
	gtk_widget_modify_font(l, desc);
	gtk_widget_show(l);
	gtk_container_add_with_properties(GTK_CONTAINER(v), l, "expand", 0, NULL);
	
	l = gtk_label_new(" 99:99:99 ");
	pango_font_description_set_size(desc, 40 * PANGO_SCALE);
	gtk_widget_modify_font(l, desc);
	gtk_widget_show(l);
	gtk_container_add_with_properties(GTK_CONTAINER(v), l, "expand", 0, NULL);
	time_display = l;

	l = gtk_label_new(" ");
	pango_font_description_set_size(desc, 10 * PANGO_SCALE);
	gtk_widget_modify_font(l, desc);
	gtk_widget_show(l);
	gtk_container_add_with_properties(GTK_CONTAINER(v), l, "expand", 1, NULL);
	timers_list = l;


	/* now from the bottom up */
	blist = NULL;
	pango_font_description_set_size(desc, 10 * PANGO_SCALE);
	add_button(&blist, "Return", desc, to_cal);
	add_button(&blist, "Set", desc, set_timer);
	add_button(&blist, "Clear All", desc, clear_timers);
	gtk_box_pack_end(GTK_BOX(v), blist, FALSE, FALSE, 0);

	blist = NULL;
	pango_font_description_set_size(desc, 10 * PANGO_SCALE);
	add_button(&blist, "-1hr", desc, del_time_60);
	add_button(&blist, "-10m", desc, del_time_10);
	add_button(&blist, "-1m", desc, del_time_1);
	gtk_box_pack_end(GTK_BOX(v), blist, FALSE, FALSE, 0);

	
	l = gtk_label_new("+1:34 - 99:99 ");
	pango_font_description_set_size(desc, 15 * PANGO_SCALE);
	gtk_widget_modify_font(l, desc);
	gtk_widget_show(l);
	
	gtk_box_pack_end(GTK_BOX(v), l, FALSE, FALSE, 0);
	timer_display = l;

	blist = NULL;
	pango_font_description_set_size(desc, 10 * PANGO_SCALE);
	add_button(&blist, "+1hr", desc, add_time_60);
	add_button(&blist, "+10m", desc, add_time_10);
	add_button(&blist, "+1m", desc, add_time_1);
	gtk_box_pack_end(GTK_BOX(v), blist, FALSE, FALSE, 0);

	return v;
}	

main(int argc, char *argv[])
{
	GtkSettings *set;

	gtk_set_locale();
	gtk_init_check(&argc, &argv);

	set = gtk_settings_get_default();
	gtk_settings_set_long_property(set, "gtk-xft-dpi", 151 * PANGO_SCALE, "code");


	window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	gtk_window_set_default_size(GTK_WINDOW(window), 480, 640);
	{
	PangoContext *context;
	PangoFontDescription *desc;
	desc = pango_font_description_new();

	pango_font_description_set_size(desc, 12 * PANGO_SCALE);
	gtk_widget_modify_font(window, desc);
	context = gtk_widget_get_pango_context(window);
	layout = pango_layout_new(context);
	}

	reason_buffer = gtk_text_buffer_new(NULL);
	cal = create_cal_window();
	clockw = create_clock_window();
	reasonw = create_reason_window();
	timerw = create_timer_window();
	gtk_container_add(GTK_CONTAINER(window), cal);
	g_signal_connect((gpointer)window, "destroy",
			 G_CALLBACK(finish), NULL);
	gtk_widget_show(window);
	gtk_widget_ref(cal);
	gtk_widget_ref(clockw);
	gtk_widget_ref(reasonw);
	gtk_widget_ref(timerw);
	set_cal(time(0));



	gtk_main();
}
