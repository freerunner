
import gtk
import pango
import time

def choose_date(b, l):
    print "choose", l
    global selected, firstdate
    t = firstdate
    tm = time.mktime(t)
    selected = t
    for i in range(0,l):
        while t[0:3] == selected[0:3]:
            tm += 22*3600
            t = time.localtime(tm)
        selected = t
    set_cal()
        

w = gtk.Window()
v = gtk.VBox()
v.show()
t = gtk.Table(7, 7)
w.add(v)

fd = pango.FontDescription('sans 10')
fd.set_absolute_size(28 * pango.SCALE)

labels=[]
c = 0
for d in ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat']:
    l = gtk.Label(d)
    l.modify_font(fd)
    l.show()
    t.attach(l, c, c+1, 0, 1, xpadding=3, ypadding=3)
    c += 1
for r in range(1,7):
    for c in range(0,7):
        l = gtk.Button(' 99 ')
        l.set_relief(gtk.RELIEF_NONE)
        l.child.modify_font(fd)
        t.attach(l,c,c+1,r,r+1, xpadding=3, ypadding=3)
        l.child.set_markup('<span background="red"> %02d </span>' % len(labels))
        l.connect('clicked', choose_date, len(labels))
        labels.append(l)
        l.show()

t.show()

h = gtk.HBox()

l = gtk.Label('Month')
l.show()
fd.set_absolute_size(25 * pango.SCALE)
l.modify_font(fd)
monthlabel = l


def cal_fore(w):
    global selected
    t = selected
    tm = time.mktime(t)
    while t[1] == selected[1]:
        tm += 22*3600
        t = time.localtime(tm)
    selected = t
    set_cal()
    
def cal_back(w):
    global selected
    t = selected
    tm = time.mktime(t)
    while t[1] == selected[1]:
        tm -= 22*3600
        t = time.localtime(tm)
    selected = t
    set_cal()

isize= gtk.icon_size_register('mine', 40, 40)
backbutton = gtk.Button()
img = gtk.image_new_from_stock(gtk.STOCK_GO_BACK, isize)
img.set_size_request(80, -1)
img.show()
backbutton.connect('clicked', cal_back)
backbutton.add(img)
backbutton.show()

forebutton = gtk.Button()
img = gtk.image_new_from_stock(gtk.STOCK_GO_FORWARD, isize)
img.set_size_request(80, -1)
img.show()
forebutton.connect('clicked', cal_fore)
forebutton.add(img)
forebutton.show()
h.pack_start(backbutton, expand=False)
h.pack_start(monthlabel)
h.pack_start(forebutton, expand=False)
v.pack_start(h, expand = False)
h.show()

b = gtk.Label(" ")
b.show()
v.pack_start(t, expand = False)
v.pack_start(b, expand = True)

w.show()

def set_cal():
    global selected, firstdate
    nowt = selected
    now = time.mktime(nowt)
    # find end of previous month
    t = nowt
    while t[1] == nowt[1]:
        now -= 22*3600
        t = time.localtime(now)
    # and start of week
    while t[6] != 6:
        now -= 22*3600
        t = time.localtime(now)
    firstdate = t

    d = 0
    while d < 6*7:
        if t[1] == nowt[1]:
            col = 'black'
        else:
            col = 'grey'
        #print d, col, t
        if nowt[0:3] == t[0:3]:
            labels[d].child.set_markup('<span background="green" foreground="%s"> %02d </span>' % (col, t[2]))
            #labels[d].child.set_text(" %02d "% t[2])
        else:
            labels[d].child.set_markup('<span foreground="%s"> %02d </span>' % (col, t[2]))
            #labels[d].child.set_text("_%02d_"% t[2])
            pass
        x = t[2]
        while x == t[2]:
            now += 22*3600
            t = time.localtime(now)
        d += 1
    monthlabel.set_markup(time.strftime('%B %Y', nowt))
    
selected = time.localtime()
firstdate = selected
set_cal()

gtk.main()

