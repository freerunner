
/*
 * generate alarm messages as required.
 * Alarm information is stored in /etc/alarms
 * format is:
 *   date:recurrence:message
 *
 * date is yyyy-mm-dd-hh-mm-ss
 *  recurrence is currently nndays
 * message is any text
 *
 * When the time comes, a txt message is generated
 *
 * We use dnotify to watch the file
 *
 * We never report any event that is before the timestamp
 * on the file - that guards against replaying lots of 
 * events if the clock gets set backwards.
 */

#define _XOPEN_SOURCE
#define _BSD_SOURCE
#define _GNU_SOURCE
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>
#include <fcntl.h>
#include <time.h>
#include <string.h>
#include <stdio.h>
#include <sys/ioctl.h>
#include <linux/rtc.h>
#include <sys/dir.h>

struct event {
	struct event *next;
	time_t when;
	long recur;
	char *mesg;
};

void event_free(struct event *ev)
{
	while (ev) {
		struct event *n = ev->next;
		free(ev->mesg);
		free(ev);
		ev = n;
	}
}

void update_event(struct event *ev, time_t now)
{
	/* make sure 'when' is in the future if possible */
	while (ev->when < now && ev->recur > 0)
		ev->when += ev->recur;
}

struct event *event_parse(char *line)
{
	struct event *rv;
	char *recur, *mesg;
	struct tm tm;
	long rsec;
	recur = strchr(line, ':');
	if (!recur)
		return NULL;
	*recur++ = 0;
	mesg = strchr(recur, ':');
	if (!mesg)
		return NULL;
	*mesg++ = 0;
	line = strptime(line, "%Y-%m-%d-%H-%M-%S", &tm);
	if (!line)
		return NULL;
	rsec = atoi(recur);
	if (rsec < 0)
		return NULL;
	rv = malloc(sizeof(*rv));
	rv->next = NULL;
	tm.tm_isdst = -1;
	rv->when = mktime(&tm);
	rv->recur = rsec;
	rv->mesg = strdup(mesg);
	return rv;
}

struct event *event_load_soonest(char *file, time_t now)
{
	/* load the file and return only the soonest event at or after now */
	char line[2048];
	struct stat stb;
	struct event *rv = NULL, *ev;
	FILE *f;

	f = fopen(file, "r");
	if (!f)
		return NULL;
	if (fstat(fileno(f), &stb) == 0 &&
	    stb.st_mtime > now)
		now = stb.st_mtime;

	while (fgets(line, sizeof(line), f) != NULL) {
		char *eol = line + strlen(line);
		if (eol > line && eol[-1] == '\n')
			eol[-1] = 0;

		ev = event_parse(line);
		if (!ev)
			continue;
		update_event(ev, now);
		if (ev->when < now) {
			event_free(ev);
			continue;
		}
		if (rv == NULL) {
			rv = ev;
			continue;
		}
		if (rv->when < ev->when) {
			event_free(ev);
			continue;
		}
		event_free(rv);
		rv = ev;
	}
	fclose(f);
	return rv;
}

struct event *event_load_soonest_dir(char *dir, time_t now)
{
	DIR *d = opendir(dir);
	struct dirent *de;
	struct event *rv = NULL;

	if (!d)
		return NULL;
	while ((de = readdir(d)) != NULL) {
		char *p = NULL;
		struct event *e;
		if (de->d_ino == 0)
			continue;
		if (de->d_name[0] == '.')
			continue;
		asprintf(&p, "%s/%s", dir, de->d_name);
		e = event_load_soonest(p, now);
		free(p);
		if (e == NULL)
			;
		else if (rv == NULL)
			rv = e;
		else if (rv->when < e->when)
			event_free(e);
		else {
			event_free(rv);
			rv = e;
		}
	}
	closedir(d);
	return rv;
}

void event_deliver(struct event *ev)
{
	char line[2048];
	char file[1024];
	struct tm *tm;
	char *z;
	char *m;
	int f;
	time_t now;

	time(&now);
	tm = gmtime(&now);
	strftime(line, 2048, "%Y%m%d-%H%M%S", tm);
	tm = localtime(&ev->when);
	strftime(line+strlen(line), 1024, " ALARM %Y%m%d-%H%M%S", tm);
	z = line + strlen(line);
	sprintf(z, "%+02d alarm ", tm->tm_gmtoff/60/15);

	z = line + strlen(line);
	m = ev->mesg;

	while (*m) {
		switch (*m) {
		default:
			*z++ = *m;
			break;
		case ' ':
		case '\t':
		case '\n':
			sprintf(z, "%%%02x", *m);
			z += 3;
			break;
		}
		m++;
	}
	*z++ = '\n';
	*z = 0;
	
	sprintf(file, "/media/card/SMS/%.6s", line);
	f = open(file, O_WRONLY | O_APPEND | O_CREAT, 0600);
	if (f >= 0) {
		write(f, line, strlen(line));
		close(f);
		f = open("/media/card/SMS/newmesg", O_WRONLY | O_APPEND | O_CREAT,
			 0600);
		if (f >= 0) {
			write(f, line, 15);
			write(f, "\n", 1);
			close(f);
		}
		f = open("/var/run/alert/alarm", O_WRONLY | O_CREAT, 0600);
		if (f) {
			write(f, "new\n", 4);
			close(f);
		}
	}
}

static int read_alarm(struct rtc_wkalrm *alarm, int fd)
{
        int res;

        return ioctl(fd, RTC_WKALM_RD, alarm);
}

static void read_time(struct rtc_time *tm, int fd)
{
        int res;

        res = ioctl(fd, RTC_RD_TIME, tm);
        if (res < 0) {
                perror("ioctl(RTC_RD_TIME)");
                exit(1);
        }
}

static void write_alarm(const struct rtc_wkalrm *alarm, int fd)
{
        int res;

        res = ioctl(fd, RTC_WKALM_SET, alarm);
        if (res < 0) {
                perror("ioctl(RTC_WKALM_SET)");
                exit(1);
        }
}



void set_alarm(time_t then)
{
	struct rtc_wkalrm alarm;
	struct tm *tm, tm2;
	time_t now;
	int fd = open("/dev/rtc0", O_RDWR);

	if (read_alarm(&alarm, fd) == 0) {
		alarm.enabled = 0;
		write_alarm(&alarm, fd);
	}
	read_time(&alarm.time, fd);
	memset(&tm2, 0, sizeof(tm2));
	tm2.tm_sec = alarm.time.tm_sec;
        tm2.tm_min = alarm.time.tm_min;
        tm2.tm_hour = alarm.time.tm_hour;
        tm2.tm_mday = alarm.time.tm_mday;
        tm2.tm_mon = alarm.time.tm_mon;
        tm2.tm_year = alarm.time.tm_year;
        tm2.tm_isdst = -1;
        now = timegm(&tm2);
	now += (then - time(0));
 
	tm = gmtime(&now);
	alarm.time.tm_sec = tm->tm_sec;
	alarm.time.tm_min = tm->tm_min;
	alarm.time.tm_hour = tm->tm_hour;
	alarm.time.tm_mday = tm->tm_mday;
	alarm.time.tm_mon = tm->tm_mon;
	alarm.time.tm_year = tm->tm_year;
	alarm.enabled = 1;
	write_alarm(&alarm, fd);
	close(fd);
}

struct timespec to;
void catchio(int sig)
{
	to.tv_sec = 0;
	/* in case we enter select */
	alarm(1);
	return;
}
void catchalarm(int sig)
{
	return;
}

main(int argc, char *argv[])
{
	struct event *ev;
	time_t now;
	int efd;
	int sdfd;
	int sfd;
	sigset_t set;

	signal(SIGIO, catchio);
	signal(SIGALRM, catchalarm);
	efd = open("/etc/alarms", O_DIRECTORY|O_RDONLY);
	if (efd < 0)
		exit(1);
	sdfd = open("/var/lock/suspend", O_DIRECTORY|O_RDONLY);
	if (sdfd < 0)
		exit(1);

	sfd = open("/var/lock/suspend/suspend", O_RDONLY);
	if (sfd < 0)
		exit(1);
	flock(sfd, LOCK_SH);
	
	time(&now);

	sigemptyset(&set);
	sigaddset(&set, SIGIO);
	sigprocmask(SIG_BLOCK, &set, NULL);
	sigdelset(&set, SIGIO);
	for(;;) {
		struct stat stb;
		fcntl(efd, F_NOTIFY, DN_MODIFY|DN_CREATE|DN_RENAME);
		fcntl(sdfd, F_NOTIFY, DN_MODIFY);
		ev = event_load_soonest_dir("/etc/alarms", now);

		fstat(sfd, &stb);
		if (stb.st_size > 0) {
			/* suspend has been requested */
			int s2;
			if (ev)
				set_alarm(ev->when-5);
			s2 = open("/var/lock/suspend/next_suspend", O_RDONLY);
			flock(s2, LOCK_SH);
			flock(sfd, LOCK_UN);
			/* suspend should happen now. we know that it has
			 * because sfd gets unlinked
			 */
			while (fstat(sfd, &stb) == 0 &&
			       stb.st_nlink > 0)
				sleep(4);
			close(sfd);
			sfd = s2;
		}

		to.tv_nsec = 0;
		if (ev) {
			to.tv_sec = 0;
			if (ev->when > time(0))
				to.tv_sec = ev->when - time(0);
		} else
			to.tv_sec = 3600;
		pselect(0, NULL, NULL, NULL, &to, &set);
		
		if (ev && time(0) >= ev->when) {
			event_deliver(ev);
			now = ev->when+1;
		}
		event_free(ev);
	}
}
