# try to draw clock labels.

import gtk
import math
import pango

w = gtk.Window()
f = gtk.Fixed()
w.add(f)
f.show()

fd = pango.FontDescription('sans 10')
fd.set_absolute_size(40 * pango.SCALE)
r = 220
off=20
for i in range(6,18):
    l = gtk.Label()
    l.set_markup('<span background="red">%2d</span>'%i)
    l.modify_font(fd)
    l.show()
    a = i/6.0 * math.pi
    x = math.sin(a)*r + 240 - off
    y = -math.cos(a)*r + 320 - off
    f.put(l, int(x), int(y))

fd.set_absolute_size(25 * pango.SCALE)
r = 170
off = 12
for i in range(1,6) + range(18,25):
    l = gtk.Label("%d"%i)
    l.set_markup('<span background="blue" foreground="white">%2d</span>'%i)
    l.modify_font(fd)
    l.show()
    a = i/6.0 * math.pi
    x = math.sin(a)*r + 240 - off
    y = -math.cos(a)*r + 320 - off
    f.put(l, int(x), int(y))

fd.set_absolute_size(20 * pango.SCALE)
r = 120
off = 8
for i in range(0,60,5):
    l = gtk.Label("%d"%i)
    l.set_markup('<span background="green">%2d</span>'%i)
    l.modify_font(fd)
    l.show()
    a = i/30.0 * math.pi
    x = math.sin(a)*r + 240 - off
    y = -math.cos(a)*r + 320 - off
    f.put(l, int(x), int(y))

w.show()
gtk.main()


          
    
