#include <Python.h>
#include <fakekey/fakekey.h>



static PyMethodDef FakekeyMethods[] = {
    ...
    {"fakekey", fakekey_class, METH_VARARGS,
     "Send synthesised key events to an X client"},
    ...
    {NULL, NULL, 0, NULL}        /* Sentinel */
};


PyMODINIT_FUNC
initfakekey(void)
{
    (void) Py_InitModule("fakekey", FakekeyMethods);
}

