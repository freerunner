
# Python library to play sounds using ALSA from inside a
# glib event loop
#
# We use the 'non-blocking' output routines, write until
# we can write no more, or we hit the stop-latency limit.
# Then set a timer to try again when we estimate the buffer
# will be 3/4 full.
#
# playing can be interrupted at any time - we allow the buffers
# to flush.
#
# Currently only wav files

import gobject, alsaaudio, time, struct, sys

class Play():
    def __init__(self, file, latency_ms = 1000, done = None):
        # Arrange to play 'file' - which is the name of a .wav file
        self.pcm = alsaaudio.PCM(alsaaudio.PCM_PLAYBACK, alsaaudio.PCM_NONBLOCK)
        self.latency_ms = latency_ms
        self.finished = False
        self.done = done
        self.setfile(file)

    def setfile(self, file):
        # A wav file starts:
        #   0-3  "RIFF"
        #   4-7  Bytes in rest of file.
        #   8-11 "WAVE"
        #  12-15 "fmt "
        #  16-19 bytes of format
        #  20-21 ==1  Microsoft PCM
        #  22-23      channels
        #  24-27  freq
        #  28-31  byte rate
        #  32-33  bytes per frame
        #  34-35  bits per sample
        #  36-39 "data"
        #  40-43 number of bytes of data
        #  44... actual samples
        self.f = open(file)
        header = self.f.read(44)
        if len(header) != 44:
            raise IOError
        riff, b1, wave, fmt, b2, format, chan, rate, br, bf, bs, data, b3 = \
              struct.unpack("4si4s 4sihhiihh 4si", header)

        if riff != "RIFF" or wave != "WAVE" or fmt != "fmt " or data != "data":
            raise ValueError
        if format != 1 or bs != 16:
            raise ValueError
        else:
            self.pcm.setformat(alsaaudio.PCM_FORMAT_S16_LE)

        if chan < 1 or chan > 4:
            raise ValueError
        else:
            self.pcm.setchannels(chan)

        self.pcm.setrate(rate)
        self.bytes_per_second = rate * 2 * chan

        # choose the period to be 1/8 of the latency,
        # probably need to set an upper bound
        frames_per_latency = rate * self.latency_ms / 1000
        self.bytes_per_latency = frames_per_latency * chan * 2;
        #self.bytes_per_period = (frames_per_latency / 8) * chan * 2
        self.bytes_per_period = 320

        self.data = None
        
        self.pcm.setperiodsize(self.bytes_per_period / chan / 2)
        #print "bytes_per_period", self.bytes_per_period
        #print "period size", self.bytes_per_period / chan / 2

        self.start = time.time()
        self.loaded = 0
        self.finished = False
        self.playsome()

    def playsome(self):
        if self.finished:
            return
        now = time.time()

        self.now = now
        pos = int( (time.time() - self.start) * self.bytes_per_second)
        buffered = self.loaded - pos
        cnt = 0
        data = self.data
        while buffered < self.bytes_per_latency + self.bytes_per_period:
            if not data:
                data = self.f.read(self.bytes_per_period)
            if not data:
                self.finished = True
                self.data = None
                if self.done:
                    self.done()
                return
            if not self.pcm.write(data):
                break
            data = None

            cnt += 1
            buffered += self.bytes_per_period

        self.data = data
        self.loaded = buffered + pos

        pos = int( (time.time() - self.start) * self.bytes_per_second)
        buffered = self.loaded - pos
        delay = int(buffered /4 * 1000 / self.bytes_per_second)
        print "wrote", cnt, "delay" ,delay
        if delay < 20:
            self.start += float( 20 - delay) / 1000
            delay = 10
        gobject.timeout_add(delay, self.playsome, priority = gobject.PRIORITY_HIGH)


if __name__ == "__main__":
    # test code.
    # play given wav file in a loop for 20 seconds, then stop
    p = None
    def done():
        p.setfile(sys.argv[1])
    p = Play(sys.argv[1], 400, done)
    c = gobject.main_context_default()
    def abort():
        p.finished = True
    gobject.timeout_add(20000, abort)
    while not p.finished:
        c.iteration()
