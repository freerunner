#!/usr/bin/env python

# FingerScroll is a simple widget to wrap around TextView
# so that the TextBuffer can be scrolled with finger-wipes.

import gtk

class FingerScroll(gtk.TextView):
    def __init__(self):
        gtk.TextView.__init__(self)
        self.hadj = gtk.Adjustment()
        self.vadj = gtk.Adjustment()
        self.set_size_request(1,1)
        self.set_scroll_adjustments(self.hadj, self.vadj)

        self.add_events(gtk.gdk.POINTER_MOTION_MASK
                        | gtk.gdk.POINTER_MOTION_HINT_MASK
                        | gtk.gdk.BUTTON_PRESS_MASK
                        | gtk.gdk.BUTTON_RELEASE_MASK)
        self.connect("button_press_event", self.press)
        self.connect("button_release_event", self.release)
        self.connect("motion_notify_event", self.motion)
        self.drag_start = None

    def press(self, w, ev):
        w.stop_emission("button_press_event")

        self.drag_start = int(ev.x), int(ev.y)
        self.xstart = self.hadj.value
        self.ystart = self.vadj.value

    def release(self, w, ev):
        self.drag_start = None
        w.stop_emission("button_release_event")

    def motion(self, w, ev):
        if self.drag_start == None:
            return

        if ev.is_hint:
            x, y, state = ev.window.get_pointer()
        else:
            x = ev.x
            y = ev.y
        x = int(x)
        y = int(y)
        dx = x - self.drag_start[0]
        dy = y - self.drag_start[1]
        newx, newy = self.xstart, self.ystart
        if abs(dx) > abs(dy):
            newx = newx - dx
        else:
            newy = newy - dy

        if newx >= self.hadj.upper - self.hadj.page_size:
            newx = self.hadj.upper - self.hadj.page_size
        if newx <= self.hadj.lower: newx = self.hadj.lower
        if newy >= self.vadj.upper - self.vadj.page_size:
            newy = self.vadj.upper - self.vadj.page_size
        if newy <= self.vadj.lower: newy = self.vadj.lower
        self.hadj.value = newx
        self.vadj.value = newy
        w.stop_emission("motion_notify_event")

if __name__ == "__main__":
    # test app for FingerScroll
    import sys
    w = gtk.Window(gtk.WINDOW_TOPLEVEL)
    w.connect("destroy", lambda w: gtk.main_quit())
    w.set_title("FingerScroll test")
    w.show()
    w.set_default_size(200,200)
    
    sw = FingerScroll(); sw.show()
    w.add(sw)

    b = sw.get_buffer()

    f = open(sys.argv[-1], "r")
    l = f.readline()
    while len(l):
        b.insert(b.get_end_iter(), l)
        l = f.readline()

    gtk.main()

    
