
#
# experiment with tap input.
# Have a 3x4 array of buttons.
# Enter any symbol by tapping two buttons from the top 3x3
# Bottom buttons are:  mode  cancel/delete  enter
# mode cycles : upper lower symbol
# cancel is effective after a single tap, delete when no pending tap
#
# The 3x3 keys normally show a 3x3 matrix of what they enable
# When one is tapped, all keys change to show a single image.

import gtk, pango, gobject

keymap = {}

# 4 in each corner, 6 on the sides plus 9 in the middle is 49.
# 26 + 10 leaves 13  for symbols
# 9 most common in middle leaves 15 in the corners (with .)
# digits with + - on two sides, symbols on other two
# e t a o i n s r h l d c u m f p g w y b v k x j q z
# from http://www.deafandblind.com/word_frequency.htm
keymap['lower'] = [
    '01 23 ?@#',
    'bcdfgh   ',
    '<45>67 {}',
    'jk~lm`np ',
    'aeio urst',
    '=;:\\\'"|()',
    '[] 89 +-_',
    '   qvwxyz',
    '!$%^*/&,.'
    ]
keymap['UPPER'] = [
    '01 23 ?@#',
    'BCDFGH   ',
    '<45>67 {}',
    'JK~LM`NP ',
    'AEIO URST',
    '=;:\\\'"|()',
    '[] 89 +-_',
    '   QVWXYZ',
    '!$%^*/&,.'
    ]
keymap['number'] = [
    '1        ',
    ' 2       ',
    '  3      ',
    '   4     ',
    '    5 *0#',
    '     6   ',
    '      7  ',
    '       8 ',
    '        9'
    ]


class X(gtk.Window):
    def __init__(self):
        gtk.Window.__init__(self, type=gtk.WINDOW_POPUP)
        self.set_default_size(320, 420)
        root = gtk.gdk.get_default_root_window()
        (x,y,width,height,depth) = root.get_geometry()
        x = int((width-320)/2)
        y = int((height-420)/2)
        self.move(x,y)

        self.dragx = None
        self.dragy = None
        self.moved = False

        self.button_timeout = None

        self.buttons = []
        v1 = gtk.VBox()
        v1.show()
        self.add(v1)

        self.entry = gtk.Entry()
        self.entry.show()
        v1.pack_start(self.entry, expand=False)


        v = gtk.VBox()
        v.show()
        v1.add(v)
        v.set_homogeneous(True)

        for row in range(3):
            h = gtk.HBox()
            h.show()
            h.set_homogeneous(True)
            v.add(h)
            bl = []
            for col in range(3):
                #b = gtk.Button("%d/%d" %(row, col))
                b = gtk.Button()
                b.show()
                b.connect('button_press_event', self.press)
                b.connect('button_release_event', self.release, row, col)
                b.connect('motion_notify_event', self.motion)
                b.add_events(gtk.gdk.POINTER_MOTION_MASK|
                             gtk.gdk.POINTER_MOTION_HINT_MASK)
                
                h.add(b)
                bl.append(b)
            self.buttons.append(bl)


        h = gtk.HBox()
        h.show()
        h.set_homogeneous(True)
        v.add(h)

        b = gtk.Button('mode')
        fd = pango.FontDescription('sans 10')
        fd.set_absolute_size(30 * pango.SCALE)
        b.child.modify_font(fd)
        b.show()
        b.connect('clicked', self.nextmode)
        h.add(b)
        self.modebutton = b

        b = gtk.Button(stock=gtk.STOCK_UNDO)
        b.show()
        b.connect('clicked', self.delete)
        h.add(b)

        b = gtk.Button(stock=gtk.STOCK_OK)
        b.show()
        b.connect('clicked', self.enter)
        h.add(b)

        self.show()
        self.mode = 'lower'
        self.single = False
        self.prefix = None
        self.size = 0
        self.update_buttons()
        self.connect("configure-event", self.update_buttons)

    def update_buttons(self, *a):
        alloc = self.buttons[0][0].get_allocation()
        w = alloc.width; h = alloc.height
        if w > h:
            size = h
        else:
            size = w
        size -= 12
        if size <= 10 or size == self.size:
            return
        self.size = size

        # For each button in 3x3 we need 10 images,
        # one for initial state, and one for each of the new states
        # So there are two fonts we want.
        # First we make the initial images
        fd = pango.FontDescription('sans 10')
        fd.set_absolute_size(size / 4.5 * pango.SCALE)
        self.modify_font(fd)

        bg = self.get_style().bg_gc[gtk.STATE_NORMAL]
        fg = self.get_style().fg_gc[gtk.STATE_NORMAL]
        red = self.window.new_gc()
        red.set_foreground(self.get_colormap().alloc_color(gtk.gdk.color_parse('red')))
        base_images = {}
        for mode in keymap.keys():
            base_images[mode] = 9*[None]
            for row in range(3):
                for col in range(3):
                    syms = keymap[mode][row*3+col]
                    pm = gtk.gdk.Pixmap(self.window, size, size)
                    pm.draw_rectangle(bg, True, 0, 0, size, size)
                    for r in range(3):
                        for c in range(3):
                            sym = syms[r*3+c]
                            if sym == ' ':
                                continue
                            xpos = ((c-col+1)*2+1)
                            ypos = ((r-row+1)*2+1)
                            colour = fg
                            if xpos != xpos%6:
                                xpos = xpos%6
                                colour = red
                            if ypos != ypos%6:
                                ypos = ypos%6
                                colour = red
                            layout = self.create_pango_layout(sym)
                            (ink, (ex,ey,ew,eh)) = layout.get_pixel_extents()
                            pm.draw_layout(colour,
                                           int(xpos*size/6 - ew/2),
                                           int(ypos*size/6 - eh/2),
                                           layout)
                    im = gtk.Image()
                    im.set_from_pixmap(pm, None)
                    base_images[mode][row*3+col] = im
        self.base_images = base_images
        fd.set_absolute_size(size / 1.5 * pango.SCALE)
        self.modify_font(fd)
        sup_images = {}
        for mode in keymap.keys():
            sup_images[mode] = 9*[None]
            for row in range(3):
                for col in range(3):
                    ilist = 9 * [None]
                    for r in range(3):
                        for c in range(3):
                            sym = keymap[mode][r*3+c][row*3+col]
                            if sym == ' ':
                                continue
                            pm = gtk.gdk.Pixmap(self.window, size, size)
                            pm.draw_rectangle(bg, True, 0, 0, size, size)
                            layout = self.create_pango_layout(sym)
                            (ink, (ex,ey,ew,eh)) = layout.get_pixel_extents()
                            pm.draw_layout(fg,
                                           int((size - ew)/2), int((size - eh)/2),
                                           layout)
                            im = gtk.Image()
                            im.set_from_pixmap(pm, None)
                            ilist[r*3+c] = im
                    sup_images[mode][row*3+col] = ilist
        self.sup_images = sup_images
        self.set_button_images()

    
    def set_button_images(self):
        for row in range(3):
            for col in range(3):
                b = self.buttons[row][col]
                if self.prefix == None:
                    im = self.base_images[self.mode][row*3+col]
                else:
                    im = self.sup_images[self.mode][row*3+col][self.prefix]
                if im:
                    b.set_image(im)
        

    def tap(self, widget, ev, row, col):
        if row == 3:
            self.update_buttons()
            self.set_button_images()
            return
            
        if self.prefix == None:
            self.prefix = row*3 + col
            self.button_timeout = gobject.timeout_add(500, self.do_buttons)
        else:
            sym = keymap[self.mode][self.prefix][row*3+col]
            self.entry.emit("insert-at-cursor", sym)
            self.noprefix()

    def press(self, widget, ev):
        self.dragx = int(ev.x_root)
        self.dragy = int(ev.y_root)
        self.startx, self.starty  = self.get_position()

    def release(self, widget, ev, row, col):
        self.dragx = None
        self.dragy = None
        if self.moved:
            self.moved = False
        else:
            self.tap(widget, ev, row, col)
    def motion(self, widget, ev):
        if self.dragx == None:
            return
        x = int(ev.x_root)
        y = int(ev.y_root)

        if abs(x-self.dragx)+abs(y-self.dragy) > 40 or self.moved:
            self.move(self.startx+x-self.dragx,
                      self.starty+y-self.dragy);
            self.moved = True
        if ev.is_hint:
            gtk.gdk.flush()
            ev.window.get_pointer()


    def do_buttons(self):
        self.set_button_images()
        self.button_timeout = None
        return False


    def nextmode(self, w):
        if self.prefix:
            return self.noprefix()
        if self.mode == 'lower':
            self.mode = 'UPPER'
            self.single = True
            w.child.set_text('Mode')
        elif self.mode == 'UPPER' and self.single:
            self.single = False
            w.child.set_text('MODE')
        elif self.mode == 'UPPER' and not self.single:
            self.mode = 'number'
            w.child.set_text('123')
        else:
            self.mode = 'lower'
            w.child.set_text('mode')
        self.set_button_images()

    def delete(self, w):
        if self.prefix == None:
            self.entry.emit("backspace")
        else:
            self.noprefix()

    def noprefix(self):
        self.prefix = None
        
        if self.button_timeout:
            gobject.source_remove(self.button_timeout)
            self.button_timeout = None
        else:
            self.set_button_images()

        if self.single:
            self.mode = 'lower'
            self.single = False
            self.modebutton.child.set_text('mode')
            self.set_button_images()

    def enter(self, w):
        if self.prefix == None:
            text = self.entry.get_text()
            print "Answer is", text
            self.entry.set_text('')
            root = gtk.gdk.get_default_root_window()
            app = root.property_get('_MB_CURRENT_APP_WINDOW')
            if app and app[0] == 'WINDOW':
                try:
                    appw = gtk.gdk.window_foreign_new(app[2][0])
                    appw.property_change('_INPUT_TEXT', 'STRING', 8,
                                         gtk.gdk.PROP_MODE_REPLACE, text)
                except:
                    pass
            gtk.main_quit()
        else:
            self.noprefix()

    

x = X()

gtk.main()

