
#
# interact with apm/events.d/interlock to provide
# suspend notification
#
# Also parse resume_reason files to get the reason

import dnotify, fcntl, os

lock_watcher = None

class monitor:
    def __init__(self, suspend_callback, resume_callback):
        """
        Arrange that suspend_callback is called before we suspend, and
        resume_callback is called when we resume.
        If suspend_callback returns False, it must have arranged for
        'release' to be called soon to allow suspend to continue.
        """
        global lock_watcher
        if not lock_watcher:
            lock_watcher = dnotify.dir('/var/lock/suspend')

        self.f = open('/var/lock/suspend/suspend', 'r')
        self.getlock()
        while os.fstat(self.f.fileno()).st_nlink == 0:
            self.f.close()
            self.f = open('/var/lock/suspend/suspend', 'r')
            self.getlock()

        self.suspended = False
        self.suspend = suspend_callback
        self.resume = resume_callback
        self.watch = lock_watcher.watch("suspend", self.change)

    def getlock(self):
        # lock file, protecting againt getting IOError when we get signalled.
        locked = False
        while not locked:
            try:
                fcntl.flock(self.f, fcntl.LOCK_SH)
                locked = True
            except IOError:
                pass
    

    def change(self, watched):
        if os.fstat(self.f.fileno()).st_size == 0:
            if self.suspended and os.stat('/var/lock/suspend/suspend').st_size == 0:
                self.suspended = False
                if self.resume:
                    self.resume()
            return
        if not self.suspended and (not self.suspend or self.suspend()):
            # ready for suspend
            self.release()

    def release(self):
        # ready for suspend
        old = self.f
        self.f = open('/var/lock/suspend/next_suspend', 'r')
        self.getlock()
        self.suspended = True
        fcntl.flock(old, fcntl.LOCK_UN)
        old.close()

# PMU reasons
# experimentally
# 4000000000 (38) alarm
# 0400000000 (34) usb power insert
# 0002000000 (25) power button

# From some random document ???
#    adpins    34
#    adprem
#    usbins
#    usbrem
#    rtcalarm  38
#    second
#    onkeyr
#    onkeyf
#    exton1r
#    exton1f
#    exton2r
#    exton2f
#    exton3r
#    exton3f
#    batfull
#    chghalt
#    thlimon
#    thlimoff
#    usblimon
#    usblimoff
#    adcrdy
#    onkey1s
#    lowsys
#    lowbat
#    hightmp
#    autopwrfail
#    dwn1pwrfail
#    dwn2pwrfail
#    ledpwrfail
#    ledovp
#    ldo1pwrfail
#    ldo2pwrfail
#    ldo3pwrfail
#    ldo4pwrfail
#    ldo5pwrfail
#    ldo6pwrfail
#    hcidopwrfail
#    hcidoovl

minor_reason = {}
minor_reason['4000000000'] = 'rtc-alarm'
minor_reason['0400000000'] = 'adapter-insert'
minor_reason['0002000000'] = 'power-button-pressed'


def resume_reason():
    f = open("/sys/class/i2c-adapter/i2c-0/0-0073/neo1973-resume.0/resume_reason")
    major = ""
    for i in f:
        i = i.strip()
        if i[0] != '*':
            continue
        if i[2:6] == "EINT" and i[8] == '_':
            i = i[9:]
        major = i
        break

    if major == 'PMU':
        f = open("/sys/class/i2c-adapter/i2c-0/0-0073/resume_reason")
        i = f.readline().strip()
        if i in minor_reason:
            minor = minor_reason[i]
        else:
            minor = i
        return major + ' ' + minor
    else:
        return major
    
        


if __name__ == '__main__':
    import signal
    def sus(): print "Suspending"; return True
    def res(): print "Resuming"
    monitor(sus, res)
    print "ready"
    while True:
        signal.pause()
