
#include <stdio.h>
main(int argc, char *argv[])
{

	int pos = 0;
	char *c;
	int carry = 0;

	for (c = argv[1]; *c; c+= 2) {
		int b;
		char c1, c2;
		c1 = c[0]; c2 = c[1];
		if (c1 > '9')
			c1 = 10 + (c1-'A');
		else
			c1 = c1 - '0';

		if (c2 > '9')
			c2 = 10 + (c2-'A');
		else
			c2 = c2 - '0';

		b = c1*16 + c2;

		if (pos == 0) {
			if (carry) {
				printf("%c", carry + ((b&1) << 6));
				carry = 0;
			}
			b = b >> 1;
		} else {
			b = (b << (pos-1)) | carry;
			carry = (b & 0xff80) >> 7;
			b &=  0x7f;
		}
		printf("%c", b);
		pos++;
		if (pos == 7)
			pos = 0;
	}
	printf("\n");
	exit(0);
}

