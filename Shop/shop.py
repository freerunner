#!/usr/bin/env python

#
# TO FIX
# - re-order places?
# - document
# - use separate hand-writing code
# - use separate list-select code


import sys, os, time
import pygtk, gtk, pango
import gobject

###########################################################
# Writing recognistion code
import math


def LoadDict(dict):
    # Upper case.
    # Where they are like lowercase, we either double
    # the last stroke (L, J, I) or draw backwards (S, Z, X)
    # U V are a special case

    dict.add('A', "R(4)6,8")
    dict.add('B', "R(4)6,4.R(7)1,6")
    dict.add('B', "R(4)6,4.L(4)2,8.R(7)1,6")
    dict.add('B', "S(6)7,1.R(4)6,4.R(7)0,6")
    dict.add('C', "R(4)8,2")
    dict.add('D', "R(4)6,6")
    dict.add('E', "L(1)2,8.L(7)2,8")
    # double the stem for F
    dict.add('F', "L(4)2,6.S(3)7,1")
    dict.add('F', "S(1)5,3.S(3)1,7.S(3)7,1")

    dict.add('G', "L(4)2,5.S(8)1,7")
    dict.add('G', "L(4)2,5.R(8)6,8")
    # FIXME I need better straight-curve alignment
    dict.add('H', "S(3)1,7.R(7)6,8.S(5)7,1")
    dict.add('H', "L(3)0,5.R(7)6,8.S(5)7,1")
    # capital I is down/up
    dict.add('I', "S(4)1,7.S(4)7,1")

    # Capital J has a left/right tail
    dict.add('J', "R(4)1,6.S(7)3,5")

    dict.add('K', "L(4)0,2.R(4)6,6.L(4)2,8")

    # Capital L, like J, doubles the foot
    dict.add('L', "L(4)0,8.S(7)4,3")

    dict.add('M', "R(3)6,5.R(5)3,8")
    dict.add('M', "R(3)6,5.L(1)0,2.R(5)3,8")

    dict.add('N', "R(3)6,8.L(5)0,2")

    # Capital O is CW, but can be CCW in special dict
    dict.add('O', "R(4)1,1", bot='0')

    dict.add('P', "R(4)6,3")
    dict.add('Q', "R(4)7,7.S(8)0,8")

    dict.add('R', "R(4)6,4.S(8)0,8")

    # S is drawn bottom to top.
    dict.add('S', "L(7)6,1.R(1)7,2")

    # Double the stem for capital T
    dict.add('T', "R(4)0,8.S(5)7,1")

    # U is L to R, V is R to L for now
    dict.add('U', "L(4)0,2")
    dict.add('V', "R(4)2,0")

    dict.add('W', "R(5)2,3.L(7)8,6.R(3)5,0")
    dict.add('W', "R(5)2,3.R(3)5,0")

    dict.add('X', "R(4)6,0")

    dict.add('Y',"L(1)0,2.R(5)4,6.S(5)6,2")
    dict.add('Y',"L(1)0,2.S(5)2,7.S(5)7,2")

    dict.add('Z', "R(4)8,2.L(4)6,0")

    # Lower case
    dict.add('a', "L(4)2,2.L(5)1,7")
    dict.add('a', "L(4)2,2.L(5)0,8")
    dict.add('a', "L(4)2,2.S(5)0,8")
    dict.add('b', "S(3)1,7.R(7)6,3")
    dict.add('c', "L(4)2,8", top='C')
    dict.add('d', "L(4)5,2.S(5)1,7")
    dict.add('d', "L(4)5,2.L(5)0,8")
    dict.add('e', "S(4)3,5.L(4)5,8")
    dict.add('e', "L(4)3,8")
    dict.add('f', "L(4)2,6", top='F')
    dict.add('f', "S(1)5,3.S(3)1,7", top='F')
    dict.add('g', "L(1)2,2.R(4)1,6")
    dict.add('h', "S(3)1,7.R(7)6,8")
    dict.add('h', "L(3)0,5.R(7)6,8")
    dict.add('i', "S(4)1,7", top='I', bot='1')
    dict.add('j', "R(4)1,6", top='J')
    dict.add('k', "L(3)0,5.L(7)2,8")
    dict.add('k', "L(4)0,5.R(7)6,6.L(7)1,8")
    dict.add('l', "L(4)0,8", top='L')
    dict.add('l', "S(3)1,7.S(7)3,5", top='L')
    dict.add('m', "S(3)1,7.R(3)6,8.R(5)6,8")
    dict.add('m', "L(3)0,2.R(3)6,8.R(5)6,8")
    dict.add('n', "S(3)1,7.R(4)6,8")
    dict.add('o', "L(4)1,1", top='O', bot='0')
    dict.add('p', "S(3)1,7.R(4)6,3")
    dict.add('q', "L(1)2,2.L(5)1,5")
    dict.add('q', "L(1)2,2.S(5)1,7.R(8)6,2")
    dict.add('q', "L(1)2,2.S(5)1,7.S(5)1,7")
    # FIXME this double 1,7 is due to a gentle where the
    # second looks like a line because it is narrow.??
    dict.add('r', "S(3)1,7.R(4)6,2")
    dict.add('s', "L(1)2,7.R(7)1,6", top='S', bot='5')
    dict.add('t', "R(4)0,8", top='T', bot='7')
    dict.add('t', "S(1)3,5.S(5)1,7", top='T', bot='7')
    dict.add('u', "L(4)0,2.S(5)1,7")
    dict.add('v', "L(4)0,2.L(2)0,2")
    dict.add('w', "L(3)0,2.L(5)0,2", top='W')
    dict.add('w', "L(3)0,5.R(7)6,8.L(5)3,2", top='W')
    dict.add('w', "L(3)0,5.L(5)3,2", top='W')
    dict.add('x', "L(4)0,6", top='X')
    dict.add('y', "L(1)0,2.R(5)4,6", top='Y') # if curved
    dict.add('y', "L(1)0,2.S(5)2,7", top='Y')
    dict.add('z', "R(4)0,6.L(4)2,8", top='Z', bot='2')

    # Digits
    dict.add('0', "L(4)7,7")
    dict.add('0', "R(4)7,7")
    dict.add('1', "S(4)7,1")
    dict.add('2', "R(4)0,6.S(7)3,5")
    dict.add('2', "R(4)3,6.L(4)2,8")
    dict.add('3', "R(1)0,6.R(7)1,6")
    dict.add('4', "L(4)7,5")
    dict.add('5', "L(1)2,6.R(7)0,3")
    dict.add('5', "L(1)2,6.L(4)0,8.R(7)0,3")
    dict.add('6', "L(4)2,3")
    dict.add('7', "S(1)3,5.R(4)1,6")
    dict.add('7', "R(4)0,6")
    dict.add('7', "R(4)0,7")
    dict.add('8', "L(4)2,8.R(4)4,2.L(3)6,1")
    dict.add('8', "L(1)2,8.R(7)2,0.L(1)6,1")
    dict.add('8', "L(0)2,6.R(7)0,1.L(2)6,0")
    dict.add('8', "R(4)2,6.L(4)4,2.R(5)8,1")
    dict.add('9', "L(1)2,2.S(5)1,7")

    dict.add(' ', "S(4)3,5")
    dict.add('<BS>', "S(4)5,3")
    dict.add('-', "S(4)3,5.S(4)5,3")
    dict.add('_', "S(4)3,5.S(4)5,3.S(4)3,5")
    dict.add("<left>", "S(4)5,3.S(3)3,5")
    dict.add("<right>","S(4)3,5.S(5)5,3")
    dict.add("<left>", "S(4)7,1.S(1)1,7") # "<up>"
    dict.add("<right>","S(4)1,7.S(7)7,1") # "<down>"
    dict.add("<newline>", "S(4)2,6")


class DictSegment:
    # Each segment has for elements:
    #   direction: Right Straight Left (R=cw, L=ccw)
    #   location: 0-8.
    #   start: 0-8
    #   finish: 0-8
    # Segments match if there difference at each element
    # is 0, 1, or 3 (RSL coded as 012)
    # A difference of 1 required both to be same / 3
    # On a match, return number of 0s
    # On non-match, return -1
    def __init__(self, str):
        # D(L)S,R
        # 0123456
        self.e = [0,0,0,0]
        if len(str) != 7:
            raise ValueError
        if str[1] != '(' or str[3] != ')' or str[5] != ',':
            raise ValueError
        if str[0] == 'R':
            self.e[0] = 0
        elif str[0] == 'L':
            self.e[0] = 2
        elif str[0] == 'S':
            self.e[0] = 1
        else:
            raise ValueError

        self.e[1] = int(str[2])
        self.e[2] = int(str[4])
        self.e[3] = int(str[6])

    def match(self, other):
        cnt = 0
        for i in range(0,4):
            diff = abs(self.e[i] - other.e[i])
            if diff == 0:
                cnt += 1
            elif diff == 3:
                pass
            elif diff == 1 and (self.e[i]/3 == other.e[i]/3):
                pass
            else:
                return -1
        return cnt

class DictPattern:
    # A Dict Pattern is a list of segments.
    # A parsed pattern matches a dict pattern if
    # the are the same nubmer of segments and they
    # all match.  The value of the match is the sum
    # of the individual matches.
    # A DictPattern is printers as segments joined by periods.
    #
    def __init__(self, str):
        self.segs = map(DictSegment, str.split("."))
    def match(self,other):
        if len(self.segs) != len(other.segs):
            return -1
        cnt = 0
        for i in range(0,len(self.segs)):
            m = self.segs[i].match(other.segs[i])
            if m < 0:
                return m
            cnt += m
        return cnt


class Dictionary:
    # The dictionary hold all the pattern for symbols and
    # performs lookup
    # Each pattern in the directionary can be associated
    # with  3 symbols.  One when drawing in middle of screen,
    # one for top of screen, one for bottom.
    # Often these will all be the same.
    # This allows e.g. s and S to have the same pattern in different
    # location on the touchscreen.
    # A match requires a unique entry with a match that is better
    # than any other entry.
    #
    def __init__(self):
        self.dict = []
    def add(self, sym, pat, top = None, bot = None):
        if top == None: top = sym
        if bot == None: bot = sym
        self.dict.append((DictPattern(pat), sym, top, bot))

    def _match(self, p):
        max = -1
        val = None
        for (ptn, sym, top, bot) in self.dict:
            cnt = ptn.match(p)
            if cnt > max:
                max = cnt
                val = (sym, top, bot)
            elif cnt == max:
                val = None
        return val

    def match(self, str, pos = "mid"):
        p = DictPattern(str)
        m = self._match(p)
        if m == None:
            return m
        (mid, top, bot) = self._match(p)
        if pos == "top": return top
        if pos == "bot": return bot
        return mid


class Point:
    # This represents a point in the path and all the points leading
    # up to it.  It allows us to find the direction and curvature from
    # one point to another
    # We store x,y, and sum/cnt of points so far
    def __init__(self,x,y) :
        self.xsum = x
        self.ysum = y
        self.x = x
        self.y = y
        self.cnt = 1

    def copy(self):
        n = Point(0,0)
        n.xsum = self.xsum
        n.ysum = self.ysum
        n.x = self.x
        n.y = self.y
        n.cnt = self.cnt
        return n

    def add(self,x,y):
        if self.x == x and self.y == y:
            return
        self.x = x
        self.y = y
        self.xsum += x
        self.ysum += y
        self.cnt += 1

    def xlen(self,p):
        return abs(self.x - p.x)
    def ylen(self,p):
        return abs(self.y - p.y)
    def sqlen(self,p):
        x = self.x - p.x
        y = self.y - p.y
        return x*x + y*y

    def xdir(self,p):
        if self.x > p.x:
            return 1
        if self.x < p.x:
            return -1
        return 0
    def ydir(self,p):
        if self.y > p.y:
            return 1
        if self.y < p.y:
            return -1
        return 0
    def curve(self,p):
        if self.cnt == p.cnt:
            return 0
        x1 = p.x ; y1 = p.y
        (x2,y2) = self.meanpoint(p)
        x3 = self.x; y3 = self.y

        curve = (y3-y1)*(x2-x1) - (y2-y1)*(x3-x1)
        curve = curve * 100 / ((y3-y1)*(y3-y1)
                               + (x3-x1)*(x3-x1))
        if curve > 6:
            return 1
        if curve < -6:
            return -1
        return 0

    def Vcurve(self,p):
        if self.cnt == p.cnt:
            return 0
        x1 = p.x ; y1 = p.y
        (x2,y2) = self.meanpoint(p)
        x3 = self.x; y3 = self.y

        curve = (y3-y1)*(x2-x1) - (y2-y1)*(x3-x1)
        curve = curve * 100 / ((y3-y1)*(y3-y1)
                               + (x3-x1)*(x3-x1))
        return curve

    def meanpoint(self,p):
        x = (self.xsum - p.xsum) / (self.cnt - p.cnt)
        y = (self.ysum - p.ysum) / (self.cnt - p.cnt)
        return (x,y)

    def is_sharp(self,A,C):
        # Measure the cosine at self between A and C
        # as A and C could be curve, we take the mean point on
        # self.A and self.C as the points to find cosine between
        (ax,ay) = self.meanpoint(A)
        (cx,cy) = self.meanpoint(C)
        a = ax-self.x; b=ay-self.y
        c = cx-self.x; d=cy-self.y
        x = a*c + b*d
        y = a*d - b*c
        h = math.sqrt(x*x+y*y)
        if h > 0:
            cs = x*1000/h
        else:
            cs = 0
        return (cs > 900)

class BBox:
    # a BBox records min/max x/y of some Points and
    # can subsequently report row, column, pos of each point
    # can also locate one bbox in another

    def __init__(self, p):
        self.minx = p.x
        self.maxx = p.x
        self.miny = p.y
        self.maxy = p.y

    def width(self):
        return self.maxx - self.minx
    def height(self):
        return self.maxy - self.miny

    def add(self, p):
        if p.x > self.maxx:
            self.maxx = p.x
        if p.x < self.minx:
            self.minx = p.x

        if p.y > self.maxy:
            self.maxy = p.y
        if p.y < self.miny:
            self.miny = p.y
    def finish(self, div = 3):
        # if aspect ratio is bad, we adjust max/min accordingly
        # before setting [xy][12].  We don't change self.min/max
        # as they are used to place stroke in bigger bbox.
        # Normally divisions are at 1/3 and 2/3. They can be moved
        # by setting div e.g. 2 = 1/2 and 1/2
        (minx,miny,maxx,maxy) = (self.minx,self.miny,self.maxx,self.maxy)
        if (maxx - minx) * 3 < (maxy - miny) * 2:
            # too narrow
            mid = int((maxx + minx)/2)
            halfwidth = int ((maxy - miny)/3)
            minx = mid - halfwidth
            maxx = mid + halfwidth
        if (maxy - miny) * 3 < (maxx - minx) * 2:
            # too wide
            mid = int((maxy + miny)/2)
            halfheight = int ((maxx - minx)/3)
            miny = mid - halfheight
            maxy = mid + halfheight

        div1 = div - 1
        self.x1 = int((div1*minx + maxx)/div)
        self.x2 = int((minx + div1*maxx)/div)
        self.y1 = int((div1*miny + maxy)/div)
        self.y2 = int((miny + div1*maxy)/div)

    def row(self, p):
        # 0, 1, 2 - top to bottom
        if p.y <= self.y1:
            return 0
        if p.y < self.y2:
            return 1
        return 2
    def col(self, p):
        if p.x <= self.x1:
            return 0
        if p.x < self.x2:
            return 1
        return 2
    def box(self, p):
        # 0 to 9
        return self.row(p) * 3 + self.col(p)

    def relpos(self,b):
        # b is a box within self.  find location 0-8
        if b.maxx < self.x2 and b.minx < self.x1:
            x = 0
        elif b.minx > self.x1 and b.maxx > self.x2:
            x = 2
        else:
            x = 1
        if b.maxy < self.y2 and b.miny < self.y1:
            y = 0
        elif b.miny > self.y1 and b.maxy > self.y2:
            y = 2
        else:
            y = 1
        return y*3 + x


def different(*args):
    cur = 0
    for i in args:
        if cur != 0 and i != 0 and cur != i:
            return True
        if cur == 0:
            cur = i
    return False

def maxcurve(*args):
    for i in args:
        if i != 0:
            return i
    return 0

class PPath:
    # a PPath refines a list of x,y points into a list of Points
    # The Points mark out segments which end at significant Points
    # such as inflections and reversals.

    def __init__(self, x,y):

        self.start = Point(x,y)
        self.mid = Point(x,y)
        self.curr = Point(x,y)
        self.list = [ self.start ]

    def add(self, x, y):
        self.curr.add(x,y)

        if ( (abs(self.mid.xdir(self.start) - self.curr.xdir(self.mid)) == 2) or
             (abs(self.mid.ydir(self.start) - self.curr.ydir(self.mid)) == 2) or
             (abs(self.curr.Vcurve(self.start))+2 < abs(self.mid.Vcurve(self.start)))):
            pass
        else:
            self.mid = self.curr.copy()

        if self.curr.xlen(self.mid) > 4 or self.curr.ylen(self.mid) > 4:
            self.start = self.mid.copy()
            self.list.append(self.start)
            self.mid = self.curr.copy()

    def close(self):
        self.list.append(self.curr)

    def get_sectlist(self):
        if len(self.list) <= 2:
            return [[0,self.list]]
        l = []
        A = self.list[0]
        B = self.list[1]
        s = [A,B]
        curcurve = B.curve(A)
        for C in self.list[2:]:
            cabc = C.curve(A)
            cab = B.curve(A)
            cbc = C.curve(B)
            if B.is_sharp(A,C) and not different(cabc, cab, cbc, curcurve):
                # B is too pointy, must break here
                l.append([curcurve, s])
                s = [B, C]
                curcurve = cbc
            elif not different(cabc, cab, cbc, curcurve):
                # all happy
                s.append(C)
                if curcurve == 0:
                    curcurve = maxcurve(cab, cbc, cabc)
            elif not different(cabc, cab, cbc)  :
                # gentle inflection along AB
                # was: AB goes in old and new section
                # now: AB only in old section, but curcurve
                #      preseved.
                l.append([curcurve,s])
                s = [A, B, C]
                curcurve =maxcurve(cab, cbc, cabc)
            else:
                # Change of direction at B
                l.append([curcurve,s])
                s = [B, C]
                curcurve = cbc

            A = B
            B = C
        l.append([curcurve,s])

        return l

    def remove_shorts(self, bbox):
        # in self.list, if a point is close to the previous point,
        # remove it.
        if len(self.list) <= 2:
            return
        w = bbox.width()/10
        h = bbox.height()/10
        n = [self.list[0]]
        leng = w*h*2*2
        for p in self.list[1:]:
            l = p.sqlen(n[-1])
            if l > leng:
                n.append(p)
        self.list = n

    def text(self):
        # OK, we have a list of points with curvature between.
        # want to divide this into sections.
        # for each 3 consectutive points ABC curve of ABC and AB and BC
        # If all the same, they are all in a section.
        # If not B starts a new section and the old ends on B or C...
        BB = BBox(self.list[0])
        for p in self.list:
            BB.add(p)
        BB.finish()
        self.bbox = BB
        self.remove_shorts(BB)
        sectlist = self.get_sectlist()
        t = ""
        for c, s in sectlist:
            if c > 0:
                dr = "R"  # clockwise is to the Right
            elif c < 0:
                dr = "L"  # counterclockwise to the Left
            else:
                dr = "S"  # straight
            bb = BBox(s[0])
            for p in s:
                bb.add(p)
            bb.finish()
            # If  all points are in some row or column, then
            # line is S
            rwdiff = False; cldiff = False
            rw = bb.row(s[0]); cl=bb.col(s[0])
            for p in s:
                if bb.row(p) != rw: rwdiff = True
                if bb.col(p) != cl: cldiff = True
            if not rwdiff or not cldiff: dr = "S"

            t1 = dr
            t1 += "(%d)" % BB.relpos(bb)
            t1 += "%d,%d" % (bb.box(s[0]), bb.box(s[-1]))
            t += t1 + '.'
        return t[:-1]





class text_input:
    def __init__(self, page, callout):

        self.page = page
        self.callout = callout
        self.colour = None
        self.line = None
        self.dict = Dictionary()
        LoadDict(self.dict)

        page.connect("button_press_event", self.press)
        page.connect("button_release_event", self.release)
        page.connect("motion_notify_event", self.motion)
        page.set_events(page.get_events()
                        | gtk.gdk.BUTTON_PRESS_MASK
                        | gtk.gdk.BUTTON_RELEASE_MASK
                        | gtk.gdk.POINTER_MOTION_MASK
                        | gtk.gdk.POINTER_MOTION_HINT_MASK)

    def set_colour(self, col):
        self.colour = col
    
    def press(self, c, ev):
        # Start a new line
        self.line = [ [int(ev.x), int(ev.y)] ]
        return
    def release(self, c, ev):
        if self.line == None:
            return
        if len(self.line) == 1:
            self.callout('click', ev)
            self.line = None
            return

        sym = self.getsym()
        if sym:
            self.callout('sym', sym)
        self.callout('redraw', None)
        self.line = None
        return

    def motion(self, c, ev):
        if self.line:
            if ev.is_hint:
                x, y, state = ev.window.get_pointer()
            else:
                x = ev.x
                y = ev.y
            x = int(x)
            y = int(y)
            prev = self.line[-1]
            if abs(prev[0] - x) < 10 and abs(prev[1] - y) < 10:
                return
            if self.colour:
                c.window.draw_line(self.colour, prev[0],prev[1],x,y)
            self.line.append([x,y])
        return

    def getsym(self):
        alloc = self.page.get_allocation()
        pagebb = BBox(Point(0,0))
        pagebb.add(Point(alloc.width, alloc.height))
        pagebb.finish(div = 2)

        p = PPath(self.line[1][0], self.line[1][1])
        for pp in self.line[1:]:
            p.add(pp[0], pp[1])
        p.close()
        patn = p.text()
        pos = pagebb.relpos(p.bbox)
        tpos = "mid"
        if pos < 3:
            tpos = "top"
        if pos >= 6:
            tpos = "bot"
        sym = self.dict.match(patn, tpos)
        if sym == None:
            print "Failed to match pattern:", patn
        return sym





########################################################################


def extend_array(ra, leng, val=None):
    while len(ra) <= leng:
        ra.append(val)
           

class Prod:
    # A product that might be purchased
    # These are stored in a list index by product number
    def __init__(self, num, line):
        # line is read from file, or string typed in for new
        # product in which case it contains no comma.
        # otherwise "Name,[R|I]{,Ln:m}"
        self.num = num
        words = line.split(',')
        self.name = words[0]
        self.regular = (len(words) > 1 and words[1] == 'R')
        self.loc = []
        for loc in words[2:]:
            if len(loc) == 0:
                continue
            n = loc[1:].split(':')
            pl = int(n[0])
            lc = int(n[1])
            extend_array(self.loc, pl, -1)
            self.loc[pl] = lc

    def format(self,f):
        str = "I%d," % self.num
        str += self.name + ','
        if self.regular:
            str += 'R'
        else:
            str += 'I'
        for i in range(len(self.loc)):
            if self.loc[i] >= 0:
                str += ",L%d:%d"%(i, self.loc[i])
        str += '\n'
        f.write(str)


class Purch:
    # A purchase that could be made
    # A list of these is the current shopping list.
    def __init__(self,source):
        # source is a string read from a file, or
        # a product being added to the list.
        if source.__class__ == Prod:
            self.prod = source.num
            self.state = 'X'
            self.comment = ""
        elif source.__class__ == str:
            l = source.split(',', 2)
            self.prod = int(l[0])
            self.state = l[1]
            self.comment = l[2]
        else:
            raise ValueError

    def format(self, f):
        str = '%d,%s,%s\n' % (self.prod, self.state, self.comment)
        f.write(str)

    def loc(self):
        global place
        p = products[self.prod]
        if len(p.loc) <= place:
            return -1
        if p.loc[place] == None:
            return -1
        return p.loc[place]

    def locord(self):
        global place
        p = products[self.prod]
        if len(p.loc) <= place:
            return -1
        if p.loc[place] == -1 or p.loc[place] == None:
            return -1
        return locorder[place].index(p.loc[place])

def purch_cmp(a,b):
    pa = products[a.prod]
    pb = products[b.prod]
    la = a.locord()
    lb = b.locord()

    if la < lb:
        return -1
    if la > lb:
        return 1
    # same location
    return cmp(pa.name, pb.name)


def parse_places(l):
    # P,n:name,...
    w = l.split(',')
    if w[0] != 'P':
        return
    for p in w[1:]:
        w2 = p.split(':',1)
        pos = int(w2[0])
        extend_array(places, pos,0)
        places[pos] = w2[1]

def parse_locations(l):
    # Ln,m:loc,m2:loc2,
    w = l.split(',')
    if w[0][0] != 'L':
        return
    lnum = int(w[0][1:])
    loc = []
    order = []
    for l in w[1:]:
        w2 = l.split(':',1)
        pos = int(w2[0])
        extend_array(loc, pos)
        loc[pos] = w2[1]
        order.append(pos)
    extend_array(locations, lnum)
    extend_array(locorder, lnum)
    locations[lnum] = loc
    locorder[lnum] = order

def parse_item(l):
    # In,rest
    w = l.split(',',1)
    if w[0][0] != 'I':
        return
    lnum = int(w[0][1:])
    itm = Prod(lnum, w[1])
    extend_array(products, lnum)
    products[lnum] = itm
        
def load_table(f):
    # read P L and I lines
    l = f.readline()
    while len(l) > 0:
        l = l.strip()
        if l[0] == 'P':
            parse_places(l)
        elif l[0] == 'L':
            parse_locations(l)
        elif l[0] == 'I':
            parse_item(l)
        l = f.readline()

def save_table(name):
    try:
        f = open(name+".new", "w")
    except:
        return
    f.write("P")
    for i in range(len(places)):
        f.write(",%d:%s" % (i, places[i]))
    f.write("\n")

    for i in range(len(places)):
        f.write("L%d" % i)
        for j in locorder[i]:
            f.write(",%d:%s" % (j, locations[i][j]))
        f.write("\n")
    for p in products:
        if p:
            p.format(f)
    f.close()
    os.rename(name+".new", name)

table_timeout = None
def table_changed():
    global table_timeout
    if table_timeout:
        gobject.source_remove(table_timeout)
        table_timeout = None
    table_timeout = gobject.timeout_add(15*1000, table_tick)

def table_tick():
    global table_timeout
    if table_timeout:
        gobject.source_remove(table_timeout)
        table_timeout = None
        save_table("Products")

def load_list(f):
    # Read item,state,comment from file to 'purch' list
    l = f.readline()
    while len(l) > 0:
        l = l.strip()
        purch.append(Purch(l))
        l = f.readline()

def save_list(name):
    try:
        f = open(name+".new", "w")
    except:
        return
    for p in purch:
        if p.state != 'X':
            p.format(f)
    f.close()
    os.rename(name+".new", name)


list_timeout = None
def list_changed():
    global list_timeout
    if list_timeout:
        gobject.source_remove(list_timeout)
        list_timeout = None
    list_timeout = gobject.timeout_add(15*1000, list_tick)

def list_tick():
    global list_timeout
    if list_timeout:
        gobject.source_remove(list_timeout)
        list_timeout = None
        save_list("Purchases")

def merge_list(purch, prod):
    # add to purch any products not already there
    have = []
    for p in purch:
        extend_array(have, p.prod, False)
        have[p.prod] = True
    for p in prod:
        if p and (p.num >= len(have) or not have[p.num]) :
            purch.append(Purch(p))

def locname(purch):
    if purch.loc() < 0:
        return "Unknown"
    else:
        return locations[place][purch.loc()]

class PurchView:
    # A PurchView is the view on the list of possible purchases.
    # We draw the names in a DrawingArea
    # When a name is tapped, we call-out to possibly update it.
    # We get a callback when:
    #   item state changes, so we need to change colour
    #   list (or sort-order) changes so complete redraw is needed
    #   zoom changes
    #

    def __init__(self, zoom, callout, entry):
        p = gtk.DrawingArea()
        p.show()
        self.widget = p

        fd = p.get_pango_context().get_font_description()
        self.fd = fd

        self.callout = callout
        self.zoom = 0
        self.set_zoom(zoom)
        self.pixbuf = None
        self.width = self.height = 0
        self.need_redraw = True

        self.colours = None

        self.plist = None
        self.search = None
        self.current = None
        self.gonext = False
        self.top = None
        self.all_headers = False

        p.connect("expose-event", self.redraw)
        p.connect("configure-event", self.reconfig)
        
        #p.connect("button_release_event", self.click)
        p.set_events(gtk.gdk.EXPOSURE_MASK
                     | gtk.gdk.STRUCTURE_MASK)

        self.entry = entry
        self.writing = text_input(p, self.stylus)

    def stylus(self, cmd, info):
        if cmd == "click":
            self.click(None, info)
            return
        if cmd == "redraw":
            self.widget.queue_draw()
            return
        if cmd == "sym":

            if info == "<BS>":
                self.entry.emit("backspace")
            elif info == "<newline>":
                self.entry.emit("activate")
            else:
                self.entry.emit("insert-at-cursor",info)
            #print "Got Sym ", info

    def add_col(self, sym, col):
        c = gtk.gdk.color_parse(col)
        gc = self.widget.window.new_gc()
        gc.set_foreground(self.widget.get_colormap().alloc_color(c))
        self.colours[sym] = gc

    def set_zoom(self, zoom):
        if zoom > 50:
            zoom = 50
        if zoom < 20:
            zoom = 20
        if zoom == self.zoom:
            return
        self.need_redraw = True
        self.zoom = zoom
        s = pango.SCALE
        for i in range(zoom):
            s = s * 11 / 10
        self.fd.set_absolute_size(s)
        self.widget.modify_font(self.fd)
        met = self.widget.get_pango_context().get_metrics(self.fd)

        self.lineheight = (met.get_ascent() + met.get_descent()) / pango.SCALE
        self.lineascent = met.get_ascent() / pango.SCALE
        self.widget.queue_draw()

    def set_search(self, str):
        self.search = str
        self.need_redraw = True
        self.widget.queue_draw()

    def reconfig(self, w, ev):
        alloc = w.get_allocation()
        if not self.pixbuf:
            return
        if alloc.width != self.width or alloc.height != self.height:
            self.pixbuf = None
            self.need_redraw = True

    def redraw(self, w, ev):
        if self.colours == None:
            self.colours = {}
            self.add_col('N', "blue")  # need
            self.add_col('F', "darkgreen") # found
            self.add_col('C', "red")   # Cannot find
            self.add_col('R', "orange")# Regular
            self.add_col('X', "black") # No Need
            self.add_col(' ', "white") # selected background
            self.add_col('_', "black") # location separator
            self.add_col('title', "cyan")
            self.bg = self.widget.get_style().bg_gc[gtk.STATE_NORMAL]
            self.writing.set_colour(self.colours['_'])

            
        if self.need_redraw:
            self.draw_buf()

        self.widget.window.draw_drawable(self.bg, self.pixbuf, 0, 0, 0, 0,
                                         self.width, self.height)

    def draw_buf(self):
        self.need_redraw = False
        p = self.widget
        if self.pixbuf == None:
            alloc = p.get_allocation()
            self.pixbuf = gtk.gdk.Pixmap(p.window, alloc.width, alloc.height)
            self.width = alloc.width
            self.height = alloc.height
        self.pixbuf.draw_rectangle(self.bg, True, 0, 0,
                                   self.width, self.height)
        
        if self.plist == None:
            # Empty list, say so.
            layout = self.widget.create_pango_layout("List Is Empty")
            (ink, log) = layout.get_pixel_extents()
            (ex,ey,ew,eh) = log
            self.pixbuf.draw_layout(self.colours['X'], (self.width-ew)/2,
                                    (self.height-eh)/2,
                                    layout)
            return

        # find max width and height
        maxw = 1; maxh = 1; longest = "nothing"
        curr = None; top = 0
        visible = []
        curloc = None
        for p in self.plist:

            if self.search == None and p.state == 'X':
                # Don't normally show "noneed" entries
                if p.prod == self.current and self.gonext:
                    curr = len(visible)
                continue
            if self.search != None and products[p.prod].name.lower().find(self.search.lower()) < 0:
                # doesn't contain search string
                continue
            while p.loc() != curloc:
                if not self.all_headers:
                    curloc = p.loc()
                elif curloc == None:
                    curloc = -1
                else:
                    if curloc < 0:
                        i = -1
                    else:
                        i = locorder[place].index(curloc)

                    if i < len(locorder[place]) - 1:
                        curloc = locorder[place][i+1]
                    else:
                        break
                if curloc < 0:
                    locstr = "Unknown"
                elif curloc < len(locations[place]):
                    locstr = locations[place][curloc]
                else:
                    locstr = None

                if locstr == None:
                    break

                if self.top == locstr:
                    top = len(visible)
                visible.append(locstr)

                layout = self.widget.create_pango_layout(locstr)
                (ink, (ex,ey,ew,eh)) = layout.get_pixel_extents()
                if ew > maxw: maxw = ew; longest = products[p.prod].name
                if eh > maxh: maxh = eh

            if p.prod == self.top:
                top = len(visible)
            if curr != None and self.gonext and self.gonext == p.state:
                self.gonext = False
                self.current = p.prod
                self.callout(p, 'auto')
            if p.prod == self.current:
                curr = len(visible)
            visible.append(p)
            layout = self.widget.create_pango_layout(products[p.prod].name)
            (ink, (ex,ey,ew,eh)) = layout.get_pixel_extents()
            if ew > maxw: maxw = ew; longest = products[p.prod].name
            if eh > maxh: maxh = eh
        # print "mw=%d mh=%d lh=%d la=%d" % (maxw, maxh, self.lineheight, self.lineascent)

        if self.all_headers:
            # any following headers with no items visible
            while True:
                if curloc == None:
                    curloc = -1
                else:
                    if curloc < 0:
                        i = -1
                    else:
                        i = locorder[place].index(curloc)

                    if i < len(locorder[place]) - 1:
                        curloc = locorder[place][i+1]
                    else:
                        break
                if curloc < 0:
                    locstr = "Unknown"
                elif curloc < len(locations[place]):
                    locstr = locations[place][curloc]
                else:
                    locstr = None

                if locstr == None:
                    break

                if self.top == locstr:
                    top = len(visible)
                visible.append(locstr)

                layout = self.widget.create_pango_layout(locstr)
                (ink, (ex,ey,ew,eh)) = layout.get_pixel_extents()
                if ew > maxw: maxw = ew; longest = products[p.prod].name
                if eh > maxh: maxh = eh

        self.gonext = False
        truemaxw = maxw
        maxw = int(maxw * 11/10)
        if maxh > self.lineheight:
            self.lineheight = maxh
        # Find max rows/columns
        rows = int(self.height / self.lineheight)
        cols = int(self.width / maxw) 
        if rows < 1 or cols < 1:
            if self.zoom > 10:
                self.set_zoom(self.zoom - 1)
                self.need_redraw = True
                self.widget.queue_draw()
            return
        #print "rows=%d cols=%s" % (rows,cols)
        colw = int(self.width / cols)
        offset = (colw - truemaxw)/2
        self.offset = offset

        # check 'curr' is in appropriate range and
        # possibly adjust 'top'.  Then trim visible to top.
        # Allow one blank line at the end.
        cells = rows * cols
        if cells >= len(visible):
            # display everything
            top = 0
        elif curr != None:
            # make sure curr is in good range
            if curr - top < rows/3:
                top = curr - (cells - rows/3)
                if top < 0:
                    top = 0
            if (cells - (curr - top)) < rows/3:
                top = curr - rows / 3
            if len(visible) - top < cells-1:
                top = len(visible) - (cells-1)
            if top < 0:
                top = 0
        else:
            if len(visible) - top < cells-1:
                top = len(visible) - (cells-1)
            if top < 0:
                top = 0
                
        visible = visible[top:]
        self.top = None

        self.visible = visible
        self.rows = rows
        self.cols = cols

        for r in range(rows):
            for c in range(cols):
                pos = c*rows+r
                uline = False
                if pos < len(visible):
                    if type(visible[pos]) == str:
                        strng = visible[pos]
                        state = 'title'
                        comment = False
                        if self.top == None:
                            self.top = visible[pos]
                    else:
                        strng = products[visible[pos].prod].name
                        uline =  products[visible[pos].prod].regular
                        state = visible[pos].state
                        if self.top == None:
                            self.top = visible[pos].prod
                        comment = (not not visible[pos].comment)
                else:
                    break
                layout = self.widget.create_pango_layout(strng)
                if uline:
                    a = pango.AttrList()
                    a.insert(pango.AttrUnderline(pango.UNDERLINE_SINGLE,0,len(strng)))
                    layout.set_attributes(a)
                if curr != None and c*rows+r == curr - top:
                    self.pixbuf.draw_rectangle(self.colours[' '], True,
                                               c*colw, r*self.lineheight,
                                               colw, self.lineheight)
                if state == 'title':
                    self.pixbuf.draw_rectangle(self.colours['_'], True,
                                               c*colw+2, r*self.lineheight,
                                               colw-4, self.lineheight)
                    self.pixbuf.draw_rectangle(self.colours['_'], False,
                                               c*colw+2, r*self.lineheight,
                                               colw-4-1, self.lineheight-1)

                if comment:
                    if offset > self.lineheight * 0.8:
                        w = int (self.lineheight * 0.8 * 0.9)
                        o = (offset - w) / 2
                    else:
                        w = int(offset*0.9)
                        o = int((offset-w)/2)
                    vo = int((self.lineheight-w)/2)
                    self.pixbuf.draw_rectangle(self.colours[state], True,
                                               c * colw + o, r * self.lineheight + vo,
                                               w, w)

                    
                self.pixbuf.draw_layout(self.colours[state],
                                        c * colw + offset,
                                        r * self.lineheight,
                                        layout)


    def click(self, w, ev):
        cw = self.width / self.cols
        rh = self.lineheight
        col = int(ev.x/cw)
        row = int(ev.y/rh)
        cell = col * self.rows + row
        cellpos = ev.x - col * cw
        pos = 0
        if cellpos < self.lineheight or cellpos < self.offset:
            pos = -1
        elif cellpos > cw - self.lineheight:
            pos = 1
        
        if cell < len(self.visible) and type(self.visible[cell]) != str:
            if pos == 0:
                prod = self.visible[cell].prod
                layout = self.widget.create_pango_layout(products[prod].name)
                (ink, (ex,ey,ew,eh)) = layout.get_pixel_extents()
                if cellpos > self.offset + ew:
                    pos = 1
            self.callout(self.visible[cell], pos)
        if cell < len(self.visible) and type(self.visible[cell]) == str:
            self.callout(self.visible[cell], 'heading')


    def set_purch(self, purch):
        if purch:
            self.plist = purch
        self.need_redraw = True
        self.widget.queue_draw()

    def select(self, item = None, gonext = False):
        if gonext:
            self.gonext = gonext
        elif item != None:
            self.current = item
        self.need_redraw = True
        self.widget.queue_draw()

    def show_headers(self, all):
        self.all_headers = all
        self.need_redraw = True
        self.widget.queue_draw()

class ShoppingList:

    def __init__(self):
        window = gtk.Window(gtk.WINDOW_TOPLEVEL)
        window.connect("destroy", self.close_application)
        window.set_title("Shopping List")

        self.purch = None
        self.isize = gtk.icon_size_register("mine", 40, 40)

        # try to guess where user is so when he tried to change the
        # location of something, we try 'here' first.
        # Update this whenever we find something, or set a location.
        # We clear to -1 when we change place
        self.current_loc = -1

        vb = gtk.VBox()
        window.add(vb)
        vb.show()

        top = gtk.HBox()
        top.set_size_request(-1,40)
        vb.pack_start(top, expand=False)
        top.show()

        glob = gtk.HBox()
        glob.set_size_request(-1,80)
        glob.set_homogeneous(True)
        vb.pack_end(glob, expand=False)
        glob.show()
        self.glob_control = glob

        locedit = gtk.HBox()
        locedit.set_size_request(-1, 80)
        vb.pack_end(locedit, expand=False)
        locedit.hide()
        self.locedit = locedit

        loc = gtk.HBox()
        loc.set_size_request(-1,80)
        vb.pack_end(loc, expand=False)
        loc.hide()
        self.loc = loc

        placeb = gtk.HBox()
        placeb.set_size_request(-1,80)
        vb.pack_end(placeb, expand=False)
        placeb.hide()
        self.place = placeb

        filemenu = gtk.HBox()
        filemenu.set_homogeneous(True)
        filemenu.set_size_request(-1,80)
        vb.pack_end(filemenu, expand=False)
        filemenu.hide()
        self.filemenu = filemenu

        curr = gtk.HBox()
        curr.set_size_request(-1,80)
        curr.set_homogeneous(True)
        vb.pack_end(curr, expand=False)
        curr.show()
        self.curr = curr
        self.mode = 'curr'

        e = gtk.Entry()

        l = PurchView(34, self.item_selected, e)
        vb.add(l.widget)
        self.lview = l

        # multi use text-entry body
        # used for search-string, comment-entry, item-entry/rename
        #
        ctx = e.get_pango_context()
        fd = ctx.get_font_description()
        fd.set_absolute_size(25*pango.SCALE)
        e.modify_font(fd)
        e.show()
        top.add(e)
        self.button(gtk.STOCK_OK, top, self.enter_change, expand = False)
        self.entry = e
        global XX
        XX = e
        self.entry_ignore = True
        self.entry_mode = "comment"
        e.connect("changed", self.entry_changed)
        self.ecol_search = None
        self.ecol_comment = None
        self.ecol_item = None
        self.ecol_loc = None

        # global control buttons
        self.button(gtk.STOCK_REFRESH, glob, self.choose_place)
        self.button(gtk.STOCK_PREFERENCES, glob, self.show_controls)
        self.search_toggle = self.button(gtk.STOCK_FIND, glob, self.toggle_search, toggle=True)
        self.button(gtk.STOCK_ADD, glob, self.add_item)
        self.button(gtk.STOCK_EDIT, glob, self.change_name)

        # buttons to control current entry
        self.button(gtk.STOCK_APPLY, curr, self.tick)
        self.button(gtk.STOCK_CANCEL, curr, self.cross)
        self.button(gtk.STOCK_JUMP_TO, curr, self.choose_loc)
        self.button(gtk.STOCK_ZOOM_IN, curr, self.zoomin)
        self.button(gtk.STOCK_ZOOM_OUT, curr, self.zoomout)

        # buttons for whole-list operations
        self.button(gtk.STOCK_SAVE_AS, filemenu, self.record)
        self.button(gtk.STOCK_HOME, filemenu, self.reset)
        a = self.button(gtk.STOCK_CONVERT, filemenu, self.add_regulars)
        self.add_reg_clicked = False
        a.connect('leave',self.clear_regulars)
        self.save_button = self.button(gtk.STOCK_SAVE, filemenu, self.save)
        self.revert_button = self.button(gtk.STOCK_REVERT_TO_SAVED, filemenu, self.revert_to_saved)
        self.revert_button.hide()
        self.button(gtk.STOCK_QUIT, filemenu, self.close_application)

        # Buttons to change location of current entry
        self.button(gtk.STOCK_GO_BACK, loc, self.prevloc, expand = False)
        self.curr_loc = self.button("here", loc, self.curr_switch)
        self.button(gtk.STOCK_GO_FORWARD, loc, self.nextloc, expand = False)
        l = self.curr_loc.child
        ctx = l.get_pango_context()
        fd = ctx.get_font_description()
        fd.set_absolute_size(25*pango.SCALE)
        l.modify_font(fd)

        # buttons to edit the current location
        self.button(gtk.STOCK_DELETE, locedit, self.locdelete)
        self.button(gtk.STOCK_GO_UP, locedit, self.loc_move_up)
        self.button(gtk.STOCK_GO_DOWN, locedit, self.loc_move_down)
        self.button(gtk.STOCK_ADD, locedit, self.add_item)
        self.button(gtk.STOCK_EDIT, locedit, self.change_name)
        
 
        # Buttons to change current 'place'
        self.button(gtk.STOCK_MEDIA_REWIND, placeb, self.prevplace, expand = False)
        self.curr_place = self.button("HOME", placeb, self.curr_switch)
        self.button(gtk.STOCK_MEDIA_FORWARD, placeb, self.nextplace, expand = False)
        l = self.curr_place.child
        ctx = l.get_pango_context()
        fd = ctx.get_font_description()
        fd.set_absolute_size(25*pango.SCALE)
        l.modify_font(fd)
 

        window.set_default_size(480, 640)
        window.show()


    def close_application(self, widget):
        gtk.main_quit()
        if table_timeout:
            table_tick()
        if list_timeout:
            list_tick()

    def item_selected(self, purch, pos):
        if pos == 'heading':
            if self.mode != 'loc':
                return
            newloc = None
            for i in range(len(locations[place])):
                if locations[place][i] == purch:
                    newloc = i
            if i == None:
                return
            extend_array(products[self.purch.prod].loc, place, -1)
            products[self.purch.prod].loc[place] = newloc
            table_changed()
            self.lview.plist.sort(purch_cmp)
            self.set_loc()
            self.lview.select()
            return

        if self.entry_mode == "comment":
            self.lview.select(purch.prod)
            self.purch = purch
            self.entry_ignore = True
            self.entry.set_text(purch.comment)
            self.entry_ignore = False
            self.set_loc()
            #if pos < 0:
            #    self.tick(None)
            #if pos > 0:
            #    self.cross(None)
        if self.entry_mode == "search":
            if pos != 'auto':
                if purch.state == 'X' or purch.state == 'R':
                    purch.state = 'N'
                elif purch.state == 'N':
                    purch.state = 'X'
            self.lview.select(purch.prod)
            self.purch = purch
            self.set_loc()
            list_changed()


    def entry_changed(self, widget):
        if self.entry_ignore:
            return
        if self.entry_mode == "search":
            self.lview.set_search(self.entry.get_text())
        if self.entry_mode == "comment" and self.purch != None:
            self.purch.comment = self.entry.get_text()
            list_changed()
            
    def set_purch(self, purch):
        self.lview.set_purch(purch)

    def button(self, name, bar, func, expand = True, toggle = False):
        if toggle:
            btn = gtk.ToggleButton()
        else:
            btn = gtk.Button()
        if type(name) == str and name[0:3] != "gtk":
            if not expand:
                name = " " + name + " "
            btn.set_label(name)
        else:
            img = gtk.image_new_from_stock(name, self.isize)
            if not expand:
                img.set_size_request(80, -1)
            img.show()
            btn.add(img)
        btn.show()
        bar.pack_start(btn, expand = expand)
        btn.connect("clicked", func)
        btn.set_focus_on_click(False)
        return btn

    def record(self, widget):
        # Record current purchase file in datestamped storage
        save_list(time.strftime("purch-%Y%m%d-%H"))
        
    def reset(self, widget):
        # Reset the shopping list.
        # All regular to noneed
        # if there were no regular, then
        #   found -> noneed
        #   cannot find -> need
        found = False
        for p in purch:
            if p.state == 'R':
                p.state = 'X'
                found = True
        if not found:
            for p in purch:
                if p.state == 'F':
                    p.state = 'X'
                if p.state == 'C':
                    p.state = 'N'
        list_changed()
        self.lview.select()

    def add_regulars(self, widget):
        if self.add_reg_clicked:
            return self.all_regulars(widget)
        # Mark all regulars (not already selected) as regulars
        for p in purch:
            if products[p.prod].regular and p.state == 'X':
                p.state = 'R'
        list_changed()
        self.lview.select()
        self.add_reg_clicked = True

    def all_regulars(self, widget):
        # Mark all regulars and don'twant (not already selected) as regulars
        for p in purch:
            if p.state == 'X':
                p.state = 'R'
        list_changed()
        self.lview.select()
    def clear_regulars(self, widget):
        self.add_reg_clicked = False

    def save(self, widget):
        table_tick()
        list_tick()
        self.curr_switch(widget)


    def setecol(self):
        if self.ecol_search != None:
            return
        c = gtk.gdk.color_parse("yellow")
        self.ecol_search = c
        
        c = gtk.gdk.color_parse("white")
        self.ecol_comment = c

        c = gtk.gdk.color_parse("pink")
        self.ecol_item = c

        c = gtk.gdk.color_parse('grey90')
        self.ecol_loc = c
        
    def toggle_search(self, widget):
        self.setecol()
        if self.entry_mode == "item":
            self.search_toggle.set_active(False)
            return
        if self.entry_mode == "search":
            self.entry_ignore = True
            self.entry.set_text(self.purch.comment)
            self.entry.modify_base(gtk.STATE_NORMAL, self.ecol_comment)
            self.entry_mode = "comment"
            self.lview.set_search(None)
            self.entry_ignore = False
            return
        self.entry_mode = "search"
        self.entry.modify_base(gtk.STATE_NORMAL, self.ecol_search)
        self.entry_ignore = True
        self.entry.set_text("")
        self.lview.set_search("")
        self.entry_ignore = False
        self.search_toggle.set_active(True)

    def choose_loc(self, widget):
        # replace 'curr' buttons with 'loc' buttons
        if self.purch == None:
            return
        self.curr.hide()
        self.filemenu.hide()
        self.loc.show()
        self.locedit.show()
        self.glob_control.hide()
        self.mode = 'loc'
        self.set_loc()
        self.lview.show_headers(True)
        
    def set_loc(self):
        loc = locname(self.purch)
        self.current_loc = self.purch.loc()
        self.curr_loc.child.set_text(places[place]+" / "+loc)

    def curr_switch(self, widget):
        # set current item to current location, and switch back
        self.lview.show_headers(False)
        self.loc.hide()
        self.locedit.hide()
        self.glob_control.show()
        self.place.hide()
        self.filemenu.hide()
        self.curr.show()
        self.mode = 'curr'

    def show_controls(self, widget):
        if self.mode == 'filemenu':
            self.curr_switch(widget)
        else:
            self.lview.show_headers(False)
            self.loc.hide()
            self.place.hide()
            self.curr.hide()
            self.locedit.hide()
            self.glob_control.show()
            self.filemenu.show()
            self.mode = 'filemenu'
        

    def nextloc(self, widget):
        if self.entry_mode != 'comment':
            self.enter_change(None)
        if self.current_loc != -1 and self.current_loc != self.purch.loc():
            newloc = self.current_loc
            self.current_loc = -1
        elif self.purch.loc() < 0:
            newloc = locorder[place][0]
        else:
            i = locorder[place].index(self.purch.loc())
            if i < len(locorder[place])-1:
                newloc = locorder[place][i+1]
            else:
                return

            
        if newloc < -1 or newloc >= len(locations[place]):
            return
        extend_array(products[self.purch.prod].loc, place, -1)
        products[self.purch.prod].loc[place] = newloc
        table_changed()
        self.lview.plist.sort(purch_cmp)
        self.set_loc()
        self.lview.select()

    def prevloc(self, widget):
        if self.entry_mode != 'comment':
            self.enter_change(None)
        if self.current_loc != -1 and self.current_loc != self.purch.loc():
            newloc = self.current_loc
            self.current_loc = -1
        elif self.purch.loc() < 0:
            return
        else:
            i = locorder[place].index(self.purch.loc())
            if i > 0:
                newloc = locorder[place][i-1]
            else:
                newloc = -1

        if newloc < -1:
            return
        extend_array(products[self.purch.prod].loc, place, -1)
        products[self.purch.prod].loc[place] = newloc
        table_changed()
        self.lview.plist.sort(purch_cmp)
        self.set_loc()
        self.lview.select()

    def locdelete(self, widget):
        # merge this location with the previous one
        # So every product with this location needs to be changed,
        # and the locorder updated.
        l = self.purch.loc()
        if l < 0:
            # cannot delete 'Unknown'
            return
        i = locorder[place].index(l)
        if i == 0:
            # nothing to merge with
            return
        safe.backup_table()
        newl = locorder[place][i-1]
        for p in products:
            if p != None:
                if len(p.loc) > place:
                    if p.loc[place] == l:
                        p.loc[place] = newl
        locorder[place][i:i+1] = []

        table_changed()
        self.lview.plist.sort(purch_cmp)
        self.set_loc()
        self.lview.select()

    def loc_move_up(self, widget):
        l = self.purch.loc()
        if l < 0:
            # Cannot move 'unknown'
            return
        i = locorder[place].index(l)
        if i == 0:
            # nowhere to move
            pass
        else:
            o = locorder[place][i-1:i+1]
            locorder[place][i-1:i+1] = [o[1],o[0]]
            table_changed()
            self.lview.plist.sort(purch_cmp)
            self.set_loc()
            self.lview.select()
            
    def loc_move_down(self, widget):
        l = self.purch.loc()
        if l < 0:
            # Cannot move 'unknown'
            return
        i = locorder[place].index(l)
        if i+1 >= len(locorder[place]):
            # nowhere to move
            pass
        else:
            o = locorder[place][i:i+2]
            locorder[place][i:i+2] = [o[1],o[0]]
            table_changed()
            self.lview.plist.sort(purch_cmp)
            self.set_loc()
            self.lview.select()


    def choose_place(self, widget):
        if self.entry_mode != 'comment':
            self.enter_change(None)
        if self.mode == 'place':
            self.curr_switch(widget)
            return
        self.pl_visible = True
        self.lview.show_headers(False)
        self.loc.hide()
        self.locedit.hide()
        self.glob_control.show()
        self.curr.hide()
        self.filemenu.hide()
        self.mode = 'place'
        self.place.show()
        self.set_place()

    def set_place(self):
        global place
        if place >= len(places):
            place = len(places) - 1
        if place < 0:
            place = 0
        if place >= len(places):
            pl = "Unknown Place"
        else:
            pl = places[place]
        self.curr_place.child.set_text(pl)
        self.current_loc = -1

    def nextplace(self, widget):
        global place
        if place >= len(places)-1:
            return
        place += 1
        self.lview.plist.sort(purch_cmp)
        self.set_place()
        if self.purch:
            self.lview.select()
        else:
            self.lview.set_purch(None)

    def prevplace(self, widget):
        global place
        if place <= 0:
            return
        place -= 1
        self.lview.plist.sort(purch_cmp)
        self.set_place()
        if self.purch:
            self.lview.select()
        else:
            self.lview.set_purch(None)


    def zoomin(self, widget):
        self.lview.set_zoom(self.lview.zoom+1)
    def zoomout(self, widget):
        self.lview.set_zoom(self.lview.zoom-1)

    def tick(self, widget):
        if self.purch != None:
            if self.entry_mode == "search":
                # set to regular
                products[self.purch.prod].regular = True
                #self.purch.state = 'R'
                self.lview.select(gonext=self.purch.state)
                list_changed(); table_changed()
                return
            oldstate = self.purch.state
            if self.purch.state == 'N': self.purch.state = 'F'
            elif self.purch.state == 'C': self.purch.state = 'F'
            elif self.purch.state == 'R': self.purch.state = 'N'
            elif self.purch.state == 'X': self.purch.state = 'N'
            if self.purch.state == 'F':
                self.current_loc = self.purch.loc()
            self.lview.select(gonext = oldstate)
            list_changed()
    def cross(self, widget):
        if self.purch != None:
            oldstate = self.purch.state
            if self.entry_mode == "search":
                # set to regular
                products[self.purch.prod].regular = False
                if self.purch.state == 'R':
                    self.purch.state = 'X'
                self.lview.select(gonext=oldstate)
                list_changed(); table_changed()
                return
                
            if self.purch.state == 'N': self.purch.state = 'C'
            elif self.purch.state == 'F': self.purch.state = 'N'
            elif self.purch.state == 'C': self.purch.state = 'X'
            elif self.purch.state == 'R': self.purch.state = 'X'
            elif self.purch.state == 'X': self.purch.state = 'X'
            self.lview.select(gonext = oldstate)
            list_changed()

    def add_item(self, widget):
        global place
        self.setecol()
        if self.entry_mode == "search":
            self.search_toggle.set_active(False)
        if self.entry_mode != "comment":
            return
        if self.mode == 'curr':
            self.entry_mode = "item"
            self.entry.modify_base(gtk.STATE_NORMAL, self.ecol_item)
            self.entry_ignore = True
            self.entry.set_text("")
            self.purch = None
            self.lview.select()
            self.entry_ignore = False
        elif self.mode == 'loc':
            if self.purch == None:
                return
            if None in locations[place]:
                lnum = locations[place].index(None)
            else:
                lnum = len(locations[place])
                locations[place].append(None)
            locations[place][lnum] = 'NewLocation'
            if self.purch.loc() == -1:
                so = 0
            else:
                so = locorder[place].index(self.purch.loc())+1
            locorder[place][so:so] = [lnum]
            self.nextloc(None)
            self.entry_mode = 'location'
            self.entry.modify_base(gtk.STATE_NORMAL, self.ecol_loc)
            self.entry_ignore = True
            self.entry.set_text('NewLocation')
            self.entry_ignore = False
        elif self.mode == 'place':
            if None in places:
                pnum = places.index(None)
            else:
                pnum = len(places)
                places.append(None)
            places[pnum] = 'NewPlace'
            extend_array(locations, pnum)
            locations[pnum] = []
            extend_array(locorder, pnum)
            locorder[pnum] = []
            place = pnum
            self.lview.plist.sort(purch_cmp)
            self.set_place()
            self.entry_mode = 'place'
            self.entry.modify_base(gtk.STATE_NORMAL, self.ecol_loc)
            self.entry_ignore = True
            self.entry.set_text('NewPlace')
            self.entry_ingore = False
    def change_name(self, widget):
        self.setecol()
        if self.entry_mode == "search":
            self.search_toggle.set_active(False)
        if self.entry_mode == "item":
            if self.purch != None:
                self.entry.set_text(products[self.purch.prod].name)
            return
        if self.entry_mode == "location":
            if self.purch != None:
                self.entry.modify_base(gtk.STATE_NORMAL, self.ecol_loc)
                self.entry.set_text(locname(self.purch))
            return
        if self.entry_mode == 'place':
            self.entry.modify_base(gtk.STATE_NORMAL, self.ecol_loc)
            set.entry.set_text(places[place])
            return
        if self.entry_mode != "comment":
            return
        if self.mode == 'curr':
            if self.purch == None:
                return
            self.entry_mode = "item"
            self.entry.modify_base(gtk.STATE_NORMAL, self.ecol_item)
            self.entry_ignore = True
            self.entry.set_text(products[self.purch.prod].name)
            self.entry_ignore = False
        elif self.mode == 'loc':
            if self.purch == None:
                return
            if self.purch.loc() < 0:
                return
            self.entry_mode = "location"
            self.entry.modify_base(gtk.STATE_NORMAL, self.ecol_loc)
            self.entry_ignore = True
            self.entry.set_text(locname(self.purch))
            self.entry_ignore = False
        elif self.mode == 'place':
            self.entry_mode = 'place'
            self.entry.modify_base(gtk.STATE_NORMAL, self.ecol_loc)
            self.entry_ignore = True
            self.entry.set_text(places[place])
            self.entry_inode = False

    #
    # An item is being added or renamed.  Commit the change
    # If the new name is empty, that implys a delete.
    # We only allow the delete if the state is 'X' and not regular
    def update_item(self, name):
        if len(name) > 0:
            if self.purch == None:
                # check for duplicates FIXME
                num = len(products)
                prod = Prod(num, name)
                products.append(prod)
                p = Purch(prod)
                purch.append(p)
                p.state = "N";
                self.purch = p
                self.set_purch(purch)
                self.lview.select(num)
                self.lview.plist.sort(purch_cmp)
            else:
                products[self.purch.prod].name = name
                self.lview.select()
                self.lview.plist.sort(purch_cmp)
            self.forget_backup()
            table_changed()
            list_changed()
        elif self.purch:
            # delete?
            if self.purch.state == 'N':
                # OK to delete
                products[self.purch.prod] = None
                try:
                    i = purch.index(self.purch)
                except:
                    pass
                else:
                    if i == 0:
                        new = -1
                    else:
                        new = purch[i-1].prod
                    del purch[i]
                    self.lview.plist.sort(purch_cmp)
                    self.lview.select(new)
                table_changed()
                list_changed()
                self.forget_backup()

    def update_location(self, name):
        if len(name) > 0:
            locations[place][self.purch.loc()] = name
            self.set_loc()
            self.lview.select()
            table_changed()
            return
        # See if we can delete this location
        # need to check all products that they aren't 'here'
        for p in products:
            if p and p.num != self.purch.prod and place < len(p.loc):
                if p.loc[place] == self.purch.loc():
                    return
        # nothing here except 'purch'
        l = self.purch.loc()
        self.prevloc(None)
        locations[place][l] = None
        locorder[place].remove(l)
        self.lview.plist.sort(purch_cmp)
        self.lview.select()
        table_changed()
        list_changed()

    def update_place(self, name):
        global place
        if len(name) > 0:
            places[place] = name
            self.lview.select()
            self.set_place()
            table_changed()
            return
        if len(places) <= 1:
            return

        self.backup_table()
        places[place:place+1] = []
        locations[place:place+1] = []
        locorder[place:place+1] = []
        if place >= len(places):
            place -= 1
        table_changed()
        self.set_place()
        self.lview.select()

    def backup_table(self):
        save_table("Products.backup")
        self.save_button.hide()
        self.revert_button.show()

    def forget_backup(self):
        self.save_button.show()
        self.revert_button.hide()

    def revert_to_saved(self, widget):
        try:
            f = open("Products.backup")
            products = []
            locations = []
            locorder = []
            places = []
            load_table(f)
            f.close()
        except:
            pass

        self.forget_backup()

        
    def enter_change(self, widget):
        name = self.entry.get_text()
        mode = self.entry_mode
        # This is need to avoid recursion as update_* calls {next,prev}loc
        self.entry_mode = 'comment'
        if mode == 'item':
            self.update_item(name)
        elif mode == 'location':
            self.update_location(name)
        elif mode == 'place':
            self.update_place(name)

        self.entry_mode = "comment"
        self.entry.modify_base(gtk.STATE_NORMAL, self.ecol_comment)
        self.entry_ignore = True
        self.entry.set_text("")
        self.entry_ignore = False
        
def main():
    gtk.main()
    return 0

if __name__ == "__main__":

    home = os.getenv("HOME")
    p = os.path.join(home, "shopping")
    if os.path.exists(p):
        os.chdir(p)
    else:
        os.chdir(home)

    products = []
    locations = []
    locorder = []
    places = []
    try:
        f = open("Products")
        load_table(f)
        f.close()
    except:
        places = ['Home']
        locorder = [[]]
        locations =[[]]

    purch = []
    try:
        f = open("Purchases")
    except:
        pass
    else:
        load_list(f)
        f.close()
    merge_list(purch, products)


    place = 0
    purch.sort(purch_cmp)

    sl = ShoppingList()
    sl.set_purch(purch)
    ss = gtk.settings_get_default()
    ss.set_long_property("gtk-cursor-blink", 0, "shop")
    main()
