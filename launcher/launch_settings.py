import os, stat

def alert(cmd, obj):
    if len(obj.state) == 0:
        try:
            obj.state['curr'] = os.readlink("/etc/alert/normal")
        except:
            obj.state['curr'] = '??'
        o = []
        for i in os.listdir("/etc/alert"):
            if stat.S_ISDIR(os.lstat("/etc/alert/"+i)[0]):
                o.append(i)
        obj.state['options'] = o


    if cmd == '_name':
        return ('cmd', 'mode: ' + obj.state['curr'])
    if cmd == '_options':
        return obj.state['options']
    if cmd >= 0 and cmd < len(obj.state['options']):
        o = obj.state['options'][cmd]
        os.unlink("/etc/alert/normal")
        os.symlink(o, "/etc/alert/normal")
        obj.state['curr'] = o

        
