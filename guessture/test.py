#!/usr/bin/env python

import pygtk
import gtk
import math
import sys
import time

def LoadDict(dict):
    # Upper case.
    # Where they are like lowercase, we either double
    # the last stroke (L, J, I) or draw backwards (S, Z, X)
    # U V are a special case

    # A doesn't need cross-stroke and can be pointy or curved
    dict.add('A', "R4:6-8")
    # or can have the cross stroke
    dict.add('A', "R4:6-8.L7:8-0")

    # B is drawn from bottom of spine, up, then in two loops
    dict.add('B', "R4:6-34H.R7:?-6")
    dict.add('B', "R4:6-5.R7:?-6") # Why no 'H', sample in pixelpath.2008-09-03-12-30-20.bad 
    # or we can start from the top and double the spine
    dict.add('B', "S3:1-7S.R4:6-34H.R7:?-6")
    # it might not come in very far... I wonder why no 'H'
    dict.add('B', "S3:1-7S.R4:6-5.R7:?-6")
    
    # C is a simple curve, bottom to top
    dict.add('C', "R4:8-2")

    # D is like B, but only one loop
    dict.add('D', "R4:6-67")
    dict.add('D', "R4:6-67F")
    dict.add('D', "S3:12-67S.R4:6-67")

    # E is drawn from the top, like two loops to allow a single stroke
    dict.add('E', "L1:12-78.L67:?-8")

    # F is can be drawn like E but straight at the bottom
    dict.add('F', "L1:12-78.L467:?-6")
    # or like an 'f' without the cross, but double the spine
    dict.add('F', "L4:2-6.S3:7-1S")
    # or like an 'f' but drawn from bottom up
    dict.add('F', "R4:6-25")

    # G is like c, but with an extra line to the left, or down
    dict.add('G', "L4:2-45L")
    dict.add('G', "L4:2-85.S8:01-78")

    # H has two option
    # 1/ straight down, loop across and down, straight up
    dict.add('H', "S3:1-7.R7:6-8.S5:7-1")
    dict.add('H', "L3:0-58.R8:0-8.S5:7-1")
    # 2/ straight down, 's' up to top right, straight down
    dict.add('H', "S3:1-7.R67:6-2.LS25:?-12.S5:1-7")

    # I is a doubled i, straight down, straight up
    dict.add('I', "S4:1-7.S4:7-1")

    # J needs no cap, but doubles the tail to differentiate from j
    dict.add('J', "R4:12-36.S7:036-258")
    # the foot could look like a loop
    dict.add('J', "R4:2-8C6")
    # or can have a cap
    dict.add('J', "S1:03-58.L5:2-6.RS6:258-03")
    # or can be drawn from bottom up
    dict.add('J', "L4:36-12")

    # K is bigger and probably strighter than k
    # It needs to come up to the top line
    dict.add('K', "L34:0-12S6.L45:12-78")
    # It can also be drawn like a backwards X
    dict.add('K', "R4:2-8S60")

    # L is like l, but with base doubled
    dict.add('L', "L4:0-8.S7:258-036")
    # or drawn bottom to top
    dict.add('L', "R4:8-0")

    # M is simply up/down/up/down
    # it doesn't matter how far down it comes in the middle
    dict.add('M', "R3:6-?.R5:?-8")
    # FIXME those top points can become sharp!!!

    # N is up down up
    dict.add('N', "R34:6-58.SL5:67-12")
    # But if the first point is the sharpest...
    dict.add('N', "S3:67-12.L4:03-25")

    # O is drawn clockwise, o is counter clockwise
    dict.add('O', "R4:012-012F", bot='0')
    dict.add('O', "R4:0-01", bot='0') # FIXME why no F ??

    # P is like B without the bottom loop
    # P is drawn from bottom of spine, up, then one high loop
    dict.add('P', "R4:6-034H")
    # If we can start from the top and double the spine,
    # we get a p, so don't.

    # Q is a circle from bottom to bottm, plus a tail
    # with CW or CCW is ok
    dict.add('Q', "RL4:678-678F.?78:?-?")

    # R is like B but with a straight leg
    # R is drawn from bottom of spine, up, then one loop and one leg
    dict.add('R', "R34:6-34H.?78:?-8")
    # or we can start from the top and double the spine
    dict.add('R', "S3:1-7S.R34:6-34H.?78:?-8")

    # S is drawn bottom to top
    dict.add('S', "L74:36-?.SR14:?-0125")

    # T can have a real cap, like J
    dict.add('T', "S1:036-258.L45:2-67")
    dict.add('T', "S1:258-036.R3:0-6")
    # or like a lowercase t with a double stem
    dict.add('T', "R4:0-8.S5:7-1")
    # of like a lowercase t drawn backwards
    dict.add('T', "L4:78-0")
    # maybe it is two separate lines??
    dict.add('T', "L5:678-1S2.S0:258-03")

    # U is curved, drawn left to right or right to left
    # u has a tail
    dict.add('U', "L4:0-2C")
    dict.add('U', "R4:2-0C")

    # V is like U, but straight and must be right to left,
    # or have a doubled final stroke
    dict.add('V', "R4:2-0S?")
    dict.add('V', "L4:0-2S?.S5:2-6S")

    # W is straight lines, or right to left
    dict.add('W', "L3:0-25S?.L5:036-2S?")
    dict.add('W', "R5:2-03.R3:?-0")

    # X: we join the two strokes on the right, and
    # either go bottom to top, or double the last stroke
    dict.add('X', "R4:6-0S28")
    dict.add('X', "L4:0-6S82.S4:6-2S")

    # Y: double the tail, either at foot or full stem
    dict.add('Y', "L1:03-25.R4:2-6.LS67:36-52")
    dict.add('Y', "L1:03-25.S45:2-6.S45:6-2")

    # Z: double the foot, or draw backwards
    dict.add('Z', "R34:0-6S2.S7:63-25.S7:25-36")
    # don't worry if it is a bit curved, as we cannot be confused with '2'
    dict.add('Z', "R74:8-2.SL1:58-30")


    # a is a circle with a tail
    dict.add('a', "L4:12-2.LS5:?-?")

    # b is a stick, then a ball
    dict.add('b', "S3:12-67.R7:6-63")
    # Depending on curvature, the ball might join with the stick FIXME
    dict.add('b', "L34:01-4578.R78:012-36")
    dict.add('b', "R4:0-6L")
    # FIXME a more cursive b might be good
    
    # c is a simple curve, top to bottom
    dict.add('c', "L4:2-8", top = 'C')

    # d is a loop, then up and down
    dict.add('d', "L34:58-12L.SL5:012-678")

    # e is a tight loop, with a tail
    dict.add('e', "L4:0134-785H")

    # f is drawn top to bottom with no cross-piece
    dict.add('f', "L4:2-67", top = 'F')

    # g is a loop then a tail
    dict.add('g', "L14:2-25.R4:2-367")

    # h is like a b but doesn't close at bottom
    dict.add('h', "S3:1-7.R7:6-8")
    # Depending on curvature, the ball might join with the stick FIXME
    dict.add('h', "L34:0-4578.SR87:012-8")
    # or the ball might cross over the stick
    dict.add('h', "R4:0-8L")
    

    # i is a simple down stroke, which can also be I or 1
    dict.add('i', "S4:1-7", top = 'I', bot = '1')

    # j has no dot, not cap, just a simple curve
    # if narrow, it might start at 1
    dict.add('j', "R4:12-36")

    # k is smaller than K, or loopy - like an R with a high back
    dict.add('k', "L34:0-4578.L78:12-78")
    dict.add('k', "S3:1-7S.R67:6-34H.?78:?-8")

    # l is drawn line an L to differentiate from i
    dict.add('l', "L4:01-8")
    # or it can sit on a base.
    #  l-r base
    dict.add('l', "R3:2-6.S7:3-5")
    # or r-l base
    dict.add('l', "L5:2-8.S7:5-3")

    # m is down/loop/loop
    dict.add('m', "S36:1-7.R36:36-58.R58:36-58")
    # or the loop might join to the stick
    dict.add('m', "L3:0-2.SR4:0-8.R5:36-58")

    # n has just the one loop
    dict.add('n', "S3:1-7.R4:36-58")
    

    # o is drawn counter clockwise, O is counter clockwise
    dict.add('o', "L4:012-012F", top='O', bot='0')

    # p is like P but with a downstroke at the start.
    dict.add('p', "S36:012-678.R45678:67-0134H")

    # q is like g, but with a tick to the right
    dict.add('q', "L01:12-25.L45:012-758")
    dict.add('q', "L01:12-25.S5:012-678.S8:6-2")
    
    # r is down, then up to the right
    dict.add('r', "S3:012-678.R4:6-2")

    # s is drawn top to bottom
    dict.add('s', "L14:2-8C.R74:?-036")

    # t is has a single arm on the left
    dict.add('t', "R4:0-8")
    # or t can be down/uplefthalf/right
    # FIXME need options for corners
    dict.add('t', "R4:1-5S73")
    dict.add('t', "R4:1-5LS83")
    dict.add('t', "R4:1-5S70")
    dict.add('t', "R4:1-5LS81")

    # u is like U but with a tail
    dict.add('u', "L4:0-2C.SL5:01-78")

    # v like V but straight lines
    dict.add('v', "L4:0-2S7")

    # w is loopy
    dict.add('w', "L3:0-25C.L5:03-2C")

    # x is top-to-bottom and joined on right
    dict.add('x', "L4:0-6") # S82 or C ??
    # or is backwards-c-then-c
    # FIXME dict.add('x', "R3:0-6C.?3:6-2.R5:3-6H.L8:0-8C"

    # y is simple v then tail
    dict.add('y', "L1:03-25.SR45:12-67")
    # the body can be low if the tail is long
    dict.add('y', "L4:03-2.R4:2-6")

    # z must be straight
    dict.add('z', "R34:0-6S2.LS7:036-258")

    # 0 is from bottom to bottom, either direction
    dict.add('0', "R4:78-67F")
    dict.add('0', "L4:7-78F") # start at 6 is too much like D

    # 1 is stroke from bottom to top
    dict.add('1', "S4:7-1")

    # 2 is like a 'z' but curved
    dict.add('2', "R34:03-6C.S7:036-258")

    # 3 is like an E, but backwards
    dict.add('3', "R1:013-67.R78:?-6")

    # 4 is drawn from bottom up
    dict.add('4', "L4:7-58H")
    #dict.add('4', "L4:7-58")

    # 5 is like S, but squarer at the top
    dict.add('5', "L1:2-678?0.R7:12-63")
    dict.add('5', "L1:2-678?6.R7:12-63")
    # or double to top hat to avoid difficulties
    dict.add('5', "S1:036-258.L14:2-678.R47:12-63")

    # 6 is a loop at the bottom
    dict.add('6', "L4:12-63L")

    # 7 need to be different from t ...
    dict.add('7', "R4:0-67")
    dict.add('7', "S1:3-5.S4:2-67S")

    # 8 varies depending on where at the top we start
    dict.add('8', "L14:12-58.R4:58-12")
    dict.add('8', "L14:012-8.R47:58-2.SL12:8-3")
    dict.add('8', "R4:0-25F.SL1:58-036")
    dict.add('8', "L4:25-0F.SR1:036-258")
    dict.add('8', "R14:01-36.L4:36-01")
    dict.add('8', "R14:01-36.L4:36-01L")

    # 9 is a CCW loop, then a stick, like g or q but less tail
    dict.add('9', "L1:12-25.S5:12-678")

    # space is a stroke to the right
    dict.add(' ', "S4:3-5")
    dict.add('<BS>', "S4:5-3")


    return




    # Capital J has a left/right tail
    dict.add('J', "R(4)1,6.S(7)3,5")
    # Or draw from the bottom up
    dict.add('J', "L(4)6,2")

    #dict.add('K', "L(4)0,2.R(4)6,6.L(4)2,8")
    dict.add('K', "L(4)0,2.L(4)2,8")

    # Capital L, like J, doubles the foot
    dict.add('L', "L(4)0,8.S(7)4,3")
    # or is drawn from bottom up
    dict.add('L', "R(4)8,0")
    dict.add('L', "S(7)5,3.S(3)7,1")

    dict.add('M', "R(3)6,5.R(5)3,8")
    dict.add('M', "R(3)6,5.L(1)0,2.R(5)3,8")

    dict.add('N', "R(3)6,8.L(5)0,2")

    # Capital O is CW, but can be CCW in special dict
    dict.add('O', "R(4)1,1", bot='0')

    dict.add('P', "R(4)6,3")
    dict.add('Q', "R(4)7,7.S(8)0,8")

    dict.add('R', "R(4)6,4.S(8)0,8")

    # S is drawn bottom to top.
    dict.add('S', "L(7)6,1.R(1)7,2")

    # Double the stem for capital T
    dict.add('T', "R(4)0,8.S(5)7,1")

    # U is L to R, V is R to L for now
    dict.add('U', "L(4)0,2")
    dict.add('V', "R(4)2,0")

    dict.add('W', "R(5)2,3.L(7)8,6.R(3)5,0")
    dict.add('W', "R(5)2,3.R(3)5,0")

    dict.add('X', "R(4)6,0")

    dict.add('Y',"L(1)0,2.R(5)4,6.S(5)6,2")
    dict.add('Y',"L(1)0,2.S(5)2,7.S(5)7,2")

    dict.add('Z', "R(4)8,2.L(4)6,0")

    # Lower case
    dict.add('a', "L(4)2,2.L(5)1,7")
    dict.add('a', "L(4)2,2.L(5)0,8")
    dict.add('a', "L(4)2,2.S(5)0,8")
    dict.add('b', "S(3)1,7.R(7)6,3")
    dict.add('c', "L(4)2,8", top='C')
    dict.add('d', "L(4)5,2.S(5)1,7")
    dict.add('d', "L(4)5,2.L(5)0,8")
    dict.add('e', "S(4)3,5.L(4)5,8")
    dict.add('e', "L(4)3,8")
    dict.add('f', "L(4)2,6", top='F')
    dict.add('f', "S(1)5,3.S(3)1,7", top='F')
    dict.add('g', "L(1)2,2.R(4)1,6")
    dict.add('h', "S(3)1,7.R(7)6,8")
    dict.add('h', "L(3)0,5.R(7)6,8")
    dict.add('i', "S(4)1,7", top='I', bot='1')
    dict.add('j', "R(4)1,6", top='J')
    dict.add('k', "L(3)0,5.L(7)2,8")
    dict.add('k', "L(4)0,5.L(7)2,8")
    dict.add('k', "L(4)1,8.L(7)2,8")
    dict.add('l', "L(4)0,8", top='L')
    dict.add('l', "S(3)1,7.S(7)3,5", top='L')
    dict.add('m', "S(3)1,7.R(3)6,8.R(5)6,8")
    dict.add('n', "S(3)1,7.R(4)6,8")
    dict.add('o', "L(4)1,1", top='O', bot='0')
    dict.add('p', "S(3)1,7.R(4)6,3")
    dict.add('q', "L(1)2,2.L(5)1,5")
    dict.add('q', "L(1)2,2.S(5)1,7.R(8)6,2")
    dict.add('q', "L(1)2,2.S(5)1,7.S(5)1,7")
    # FIXME this double 1,7 is due to a gentle where the
    # second looks like a line because it is narrow.??
    dict.add('r', "S(3)1,7.R(4)6,2")
    dict.add('s', "L(1)2,7.R(7)1,6", top='S', bot='5')
    dict.add('t', "R(4)0,8", top='T', bot='7')
    dict.add('t', "S(1)3,5.S(5)1,7", top='T', bot='7')
    dict.add('u', "L(4)0,2.S(5)1,7")
    dict.add('v', "L(4)0,2.L(2)0,2")
    dict.add('w', "L(3)0,2.L(5)0,2", top='W')
    dict.add('w', "L(3)0,5.R(7)6,8.L(5)3,2", top='W')
    dict.add('w', "L(3)0,5.L(5)3,2", top='W')
    dict.add('x', "L(4)0,6", top='X')
    dict.add('y', "L(1)0,2.R(5)4,6", top='Y') # if curved
    dict.add('y', "L(1)0,2.S(5)2,7", top='Y')
    dict.add('z', "R(4)0,6.L(4)2,8", top='Z', bot='2')

    # Digits
    dict.add('0', "L(4)7,7")
    dict.add('0', "R(4)7,7")
    dict.add('1', "S(4)7,1")
    dict.add('2', "R(4)0,6.S(7)3,5")
    dict.add('2', "R(4)3,6.L(4)2,8")
    dict.add('3', "R(1)0,6.R(7)1,6")
    dict.add('4', "L(4)7,5")
    dict.add('5', "L(1)2,6.R(7)0,3")
    dict.add('5', "L(1)2,6.L(4)0,8.R(7)0,3")
    dict.add('6', "L(4)2,3")
    dict.add('7', "S(1)3,5.R(4)1,6")
    dict.add('7', "R(4)0,6")
    dict.add('7', "R(4)0,7")
    dict.add('8', "L(4)2,8.R(4)4,2.L(3)6,1")
    dict.add('8', "L(1)2,8.R(7)2,0.L(1)6,1")
    dict.add('8', "L(0)2,6.R(7)0,1.L(2)6,0")
    dict.add('8', "R(4)2,6.L(4)4,2.R(5)8,1")
    dict.add('9', "L(1)2,2.S(5)1,7")

    dict.add(' ', "S(4)3,5")
    dict.add('<BS>', "S(4)5,3")
    dict.add('-', "S(4)3,5.S(4)5,3")
    dict.add('_', "S(4)3,5.S(4)5,3.S(4)3,5")
    dict.add("<left>", "S(4)5,3.S(3)3,5")
    dict.add("<right>","S(4)3,5.S(5)5,3")
    
#def ordered(a,b,c):
#    return (a <= b and b <= c) or (a >= b and b >= c)
def inorder(a,b,c):
    if ( (a == b and b == c) or
         (a == b and b+1 == c) or
         (a+1 == b and b == c)):
        return 1;
    if  a+1 == b and b+1 == c :
        return 2;
    return 0;

def ordered(a,b,c):
    return max (inorder(a,b,c), inorder(c,b,a))
def points_ordered(a,b,c):
    y = ordered(a/3, b/3, c/3)
    x = ordered(a%3, b%3, c%3)
    return x > 0 and y > 0 and x+y <= 3
def is_corner(p):
    #return p==0 or p==2 or p == 6 or p == 8
    return False

def filter_lines(list):
    print "filter ", list
    if len(list) < 2:
        return list
    first = list.pop(0)
    second = list.pop(0)
    new = [first]
    while len(list) :
        next = list.pop(0)
        if is_corner(second) or not points_ordered(first, second, next):
            new.append(second)
            first = second
        second = next
    new.append(second)
    return new

def path2str(path):
    s = ""
    for n in path:
        s = s + ("%d" % (n+1))
    return s

def different(*args):
    cur = 0
    for i in args:
        if cur != 0 and i != 0 and cur != i:
            return True
        if cur == 0:
            cur = i
    return False

def maxcurve(*args):
    for i in args:
        if i != 0:
            return i
    return 0

def choose_corner(curve,A,B):
    if abs(B.x-A.x) > abs(B.y-A.y):
        # use X component
        if B.x > A.x:
            # from the left
            if curve == 0:
                return 'L'
            elif curve == 1:
                # clockwise
                return 'TL'
            else:
                return 'BL'
        else:
            # from the right
            if curve == 0:
                return 'R'
            elif curve == 1:
                return 'BR'
            else:
                return 'TR'
    else:
        # use Y component
        if B.y > A.y:
            # from the top
            if curve == 0:
                return 'T'
            elif curve == 1:
                # clockwise
                return 'TR'
            else:
                return 'TL'
        else:
            # from the bottom
            if curve == 0:
                return 'B'
            elif curve == 1:
                return 'BL'
            else:
                return 'BR'

class DictSegment:
    # Each segment has six elements:
    #   direction: Right Straight Left (R=cw, L=ccw)
    #   location: 0-8.
    #   start: 0-8
    #   finish: 0-8
    #   Loops: Hi Lo or Full.  There are a list of these
    #   Curvature: Straight of Curved. Can be followed by locations of corners.
    # A parsed segment has gives discrete values for each element.
    # A pattern segment can give multiple values - or ? - for
    #   direction, location, start, finish and can give ? or empty
    #   for Curvature.  Loops must be given exactly.
    # A parsed segment matches a pattern if there is a match in
    # each element.
    # If the pattern has a Straight curvature, we (currently) require an exact match
    # We place a ':' after the location and a '-' after the 'start' to enable
    # unambiguous parsing.
    # We parse into strings. For parsed segments, most strings are single chars
    def extract(self, chars, name = ""):
        s = self.str
        r = ""
        while len(s) > 0 and s[0] in chars:
            r += s[0]
            s = s[1:]
        if name and len(r) == 0:
            raise ValueError, "Missing "+name
        self.str = s
        return r
    def __init__(self, str):
        # DL:S-FLC
        self.str = str
        self.dir = self.extract("RSL?", "Direction")
        self.loc = self.extract("012345678?", "Location")
        sep = self.extract(":", "Separator")
        self.strt = self.extract("012345678?", "Start")
        sep = self.extract("-", "Separator")
        self.fin = self.extract("012345678?", "Finish")
        self.loop = self.extract("HLF")
        if len(self.str) > 0 and self.str[0] in "SC?":
            self.curve = self.str[0]
            self.str = self.str[1:]
        else:
            self.curve = "?"
        self.points = self.extract("012345678?")
        if len(self.str) > 0:
            raise ValueError, "Extra character on end of segment description"
        
            
    def match(self, other):
        # self is a pattern, other is a parsed segment
        if self.dir != '?' and other.dir not in self.dir:
            return False
        if self.loc != '?' and other.loc not in self.loc:
            return False
        if self.strt != '?' and other.strt not in self.strt:
            return False
        if self.fin != '?' and other.fin not in self.fin:
            return False
        if self.loop != other.loop:
            return False
        if self.curve != '?' and other.curve != self.curve:
            return False
        if self.curve == 'C' and other.points:
            # if we require a curve, there must be no points - FIXME better syntax
            pass # return False
        if self.curve == 'S' or self.points != "":
            if len(other.points) != len(self.points):
                return False
            if self.points != "?" and other.points != self.points:
                return False
        return True

class DictPattern:
    # A Dict Pattern is a list of segments.
    # A parsed pattern matches a dict pattern if
    # the are the same nubmer of segments and they
    # all match.
    # A DictPattern is printed as segments joined by periods.
    #
    def __init__(self, str):
        self.segs = map(DictSegment, str.split("."))
    def match(self,other):
        if len(self.segs) != len(other.segs):
            return False
        for i in range(0,len(self.segs)):
            if not self.segs[i].match(other.segs[i]):
                return False
        return True
    
            
class Dictionary:
    # The dictionary holds all the pattern for symbols and
    # performs lookup
    # Each pattern in the directionary can be associated
    # with  3 symbols.  One when drawing in middle of screen,
    # one for top of screen, one for bottom.
    # Often these will all be the same.
    # This allows e.g. s and S to have the same pattern in different
    # location on the touchscreen.
    # A match requires a unique entry that matches.
    # 
    def __init__(self):
        self.dict = []
    def add(self, sym, pat, top = None, bot = None):
        if top == None: top = sym
        if bot == None: bot = sym
        try:
            self.dict.append((DictPattern(pat), sym, top, bot))
        except ValueError, v:
            raise ValueError, v.message + " for symbol " + sym

    def _match(self, p):
        max = -1
        val = None
        for (ptn, sym, top, bot) in self.dict:
            cnt = ptn.match(p)
            if cnt > max:
                max = cnt
                val = (sym, top, bot)
            elif cnt == max:
                val = None
        return val
    
    def match(self, str, pos = "mid"):
        p = DictPattern(str)
        m = self._match(p)
        if m == None:
            return m
        (mid, top, bot) = self._match(p)
        if pos == "top": return top
        if pos == "bot": return bot
        return mid

    def _closest(self, p):
        vals = []
        for (ptn, sym, top, bot) in self.dict:
            if ptn.match(p):
                vals.append((sym, top, bot))
        print "closest", vals
        
    def closest(self,str):
        # Find all matches if something matches mutliple times
        p = DictPattern(str)
        self._closest(p)


class Point:
    # This represents a point in the path and all the points leading
    # up to it.  It allows us to find the direction and curvature from
    # one point to another
    # We store x,y, and sum/cnt of points so far
    def __init__(self,x,y) :
        self.xsum = x
        self.ysum = y
        self.x = x
        self.y = y
        self.cnt = 1
        self.known_straight = False

    def tupple(self):
        return (self.x, self.y)

    def copy(self):
        n = Point(0,0)
        n.xsum = self.xsum
        n.ysum = self.ysum
        n.x = self.x
        n.y = self.y
        n.cnt = self.cnt
        n.known_straight = self.known_straight
        return n

    def add(self,x,y):
        if self.x == x and self.y == y:
            return
        self.x = x
        self.y = y
        self.xsum += x
        self.ysum += y
        self.cnt += 1

    def xlen(self,p):
        return abs(self.x - p.x)
    def ylen(self,p):
        return abs(self.y - p.y)
    def sqlen(self,p):
        x = self.x - p.x
        y = self.y - p.y
        return x*x + y*y
    
    def xdir(self,p):
        if self.x > p.x:
            return 1
        if self.x < p.x:
            return -1
        return 0
    def ydir(self,p):
        if self.y > p.y:
            return 1
        if self.y < p.y:
            return -1
        return 0
    def curve(self,p):
        if self.cnt == p.cnt:
            return 0
        #if self.known_straight:
        #    return 0
        x1 = p.x ; y1 = p.y
        (x2,y2) = self.meanpoint(p)
        x3 = self.x; y3 = self.y

        curve = (y3-y1)*(x2-x1) - (y2-y1)*(x3-x1)
        curve = curve * 100 / ((y3-y1)*(y3-y1)
                               + (x3-x1)*(x3-x1))
        if curve > 6:
            return 1
        if curve < -6:
            return -1
        return 0

    def curve2(self,A,C):
        if self.cnt == A.cnt or self.cnt == C.cnt:
            return 0
        if not self.is_sharp(A,C, cutoff = -900):
            # nearly straight
            return 0
        x1 = A.x ; y1 = A.y
        (x2,y2) = (self.x,self.y)
        x3 = C.x; y3 = C.y

        curve = (y3-y1)*(x2-x1) - (y2-y1)*(x3-x1)
        curve = curve * 100 / ((y3-y1)*(y3-y1)
                               + (x3-x1)*(x3-x1))
        print "curve2", curve
        if curve > 1:
            return 1
        if curve < -1:
            return -1
        return 0

    def Vcurve(self,p):
        if self.cnt == p.cnt:
            return 0
        x1 = p.x ; y1 = p.y
        (x2,y2) = self.meanpoint(p)
        x3 = self.x; y3 = self.y

        curve = (y3-y1)*(x2-x1) - (y2-y1)*(x3-x1)
        curve = curve * 100 / ((y3-y1)*(y3-y1)
                               + (x3-x1)*(x3-x1))
        return curve

    def meanpoint(self,p):
        if self.cnt - p.cnt - 1 == 0:
            # only the two endpoints
            x = (self.x + p.x) / 2
            y = (self.y + p.y) / 2
        else:
            # don't include either end point in mean
            x = (self.xsum - p.xsum - self.x) / (self.cnt - p.cnt - 1)
            y = (self.ysum - p.ysum - self.y) / (self.cnt - p.cnt - 1)
        return (x,y)

    def cosine(self,A,C):
        # find the cosine of the angle at self between A and C
        (ax,ay) = A
        (cx,cy) = C
        a = ax-self.x; b=ay-self.y
        c = cx-self.x; d=cy-self.y
        x = a*c + b*d
        y = a*d - b*c
        h = math.sqrt(x*x+y*y)
        if h > 0:
            cs = x*1000/h
        else:
            cs = 0
        return cs
    def sine(self,A,C):
        # find the sine of the angle at self between A and C
        (ax,ay) = A
        (cx,cy) = C
        a = ax-self.x; b=ay-self.y
        c = cx-self.x; d=cy-self.y
        x = a*c + b*d
        y = a*d - b*c
        h = math.sqrt(x*x+y*y)
        if h > 0:
            sn = y*1000/h
        else:
            sn = 0
        return sn

    def lcurve(self,A,C):
        if self.sine(A.tupple(), C.tupple())  > 0:
            return -1
        else:
            return 1

    def is_sharp(self,A,C, cutoff = 900):
        #return False
        # Measure the cosine at self between A and C
        # as A and C could be curve, we take the mean point on
        # self.A and self.C as the points to find cosine between
        (ax,ay) = self.meanpoint(A)
        (cx,cy) = self.meanpoint(C)
        a = ax-self.x; b=ay-self.y
        c = cx-self.x; d=cy-self.y
        x = a*c + b*d
        y = a*d - b*c
        h = math.sqrt(x*x+y*y)
        if h > 0:
            cs = x*1000/h
        else:
            cs = 0
        return (cs > cutoff)

    def is_straight(self, A, C):
        # A-self-C is straight if the cosine < -0.9 or
        # cosine < 0 and sine at either end is < 0.2
        cos = self.cosine(A.tupple(),C.tupple())
        if cos < -900:
            print "cos = ", cos
            return True
        if cos > 0:
            print "cos = ", cos
            return False
        sin = A.sine(self.tupple(),C.tupple())
        if abs(sin) < 200:
            print "cos = ", cos, "sin=", sin
            return True
        sin = C.sine(self.tupple(),A.tupple())
        print "cos = ", cos, "sin2=", sin
        if abs(sin) < 200:
            return True
        return False

class LoopList:
    def __init__(self):
        self.str = ""
        self.Fcnt = 0
        self.save = None
    def add(self, l):
        if l == '-':
            return
        if self.save:
            self.str += self.save
            self.save = None
        if l == 'F':
            self.Fcnt += 1
        else:
            if self.str or self.Fcnt:
                self.getstr()
                self.str += l
            else:
                self.save = l
    def getstr(self):
        while self.Fcnt >= 3:
            self.str += 'F'
            self.Fcnt -= 2
        self.Fcnt = 0
        return self.str
        


class BBox:
    # a BBox records min/max x/y of some Points and
    # can subsequently report row, column, pos of each point
    # can also locate one bbox in another
    
    def __init__(self, p):
        self.minx = p.x
        self.maxx = p.x
        self.miny = p.y
        self.maxy = p.y

    def add(self, p):
        if p.x > self.maxx:
            self.maxx = p.x
        if p.x < self.minx:
            self.minx = p.x

        if p.y > self.maxy:
            self.maxy = p.y
        if p.y < self.miny:
            self.miny = p.y
    def finish(self):
        # if aspect ratio is bad, we adjust max/min accordingly
        # before setting [xy][12].  We don't change self.min/max
        # as they are used to place stroke in bigger bbox.
        (minx,miny,maxx,maxy) = (self.minx,self.miny,self.maxx,self.maxy)
        if (maxx - minx) * 3 < (maxy - miny) * 2:
            # too narrow
            mid = int((maxx + minx)/2)
            halfwidth = int ((maxy - miny)/4)
            minx = mid - halfwidth
            maxx = mid + halfwidth
        if (maxy - miny) * 3 < (maxx - minx) * 2:
            # too wide
            mid = int((maxy + miny)/2)
            halfheight = int ((maxx - minx)/4)
            miny = mid - halfheight
            maxy = mid + halfheight

        self.x1 = int((2*minx + maxx)/3)
        self.x2 = int((minx + 2*maxx)/3)
        self.y1 = int((2*miny + maxy)/3)
        self.y2 = int((miny + 2*maxy)/3)

    def row(self, p):
        # 0, 1, 2 - top to bottom
        if p.y <= self.y1:
            return 0
        if p.y < self.y2:
            return 1
        return 2
    def col(self, p):
        if p.x <= self.x1:
            return 0
        if p.x < self.x2:
            return 1
        return 2
    def box(self, p):
        # 0 to 9
        return self.row(p) * 3 + self.col(p)

    def relpos(self,b):
        # b is a box within self.  find location 0-8
        if b.maxx < self.x2 and b.minx < self.x1:
            x = 0
        elif b.minx > self.x1 and b.maxx > self.x2:
            x = 2
        else:
            x = 1
        if b.maxy < self.y2 and b.miny < self.y1:
            y = 0
        elif b.miny > self.y1 and b.maxy > self.y2:
            y = 2
        else:
            y = 1
        return y*3 + x
    

def curve(p1,p2,p3):
    c = (p3.y-p1.y)*(p2.x-p1.x) - (p2.y-p1.y)*(p3.x-p1.x)
    c = c * 100 / ((p3.y-p1.y)*(p3.y-p1.y) + (p3.x-p1.x)*(p3.x-p1.x))
    #print "(%d,%d-%d,%d %d,%d)=%d"%(p1.x,p1.y,p3.x,p3.y,p2.x,p2.y,c),
    if c > 0:
        return 1
    if c < 0:
        return -1
    return 0
def Vcurve(p1,p2,p3):
    c = (p3.y-p1.y)*(p2.x-p1.x) - (p2.y-p1.y)*(p3.x-p1.x)
    c = c * 100 / ((p3.y-p1.y)*(p3.y-p1.y) + (p3.x-p1.x)*(p3.x-p1.x))
    #print "(%d,%d-%d,%d %d,%d)=%d"%(p1.x,p1.y,p3.x,p3.y,p2.x,p2.y,c),
    return c
def intersects(l0,l, p0,p):
    # these intersect if the curve from l0 to l via p0 and p are different
    # and the curves from p0 to p via l0 and l are different
    if curve(l0,p0,l) != curve(l0, p, l) and \
       curve(p0,l0,p) != curve(p0, l, p):
        return True
    return False
def intersect_dir(s, p):
    # see if the line segment from s[-1] to p intersects
    # any linesegment in s that is going in a different x or y direction.
    # Find the last one and report which direction ('h' or 'v') they
    # differ in.  or None if no intersects, or same direction.
    p0 = s[-1]
    l0 = s[0]
    direct = None
    for i in range(1,len(s)-1):
        l = s[i]
        print "i:",
        if intersects(l0,l, p0,p):
            both = False
            if l.xdir(l0) != p.xdir(p0):
                direct = 'h'
                start = i-1
                both = True
            if l.ydir(l0) != p.ydir(p0):
                direct = 'v'
                start = i-1
                both = True
            if False and both:
                direct = None
        print direct
        l0 = l
    if direct == None:
        return direct
    return (direct, start)

def last_change_dir(s, dr):
    xd = 0; yd= 0
    p0 = s[0]
    cnt = 0
    rv = cnt
    for p in s[1:]:
        xd2 = p.xdir(p0); yd2 = p.ydir(p0)
        if dr == 'h':
            if xd2 != xd and xd2 != 0:
                rv = cnt
                xd = xd2
        else:
            if yd2 != yd and yd2 != 0:
                rv = cnt
                yd = yd2
        p0 = p
        cnt += 1
    return rv

class PPath:
    # a PPath refines a list of x,y points into a list of Points
    # The Points mark out segments which end at significant Points
    # such as inflections and reversals.

    def __init__(self,width,height, x,y, draw):
        # x4 and y4 show how long a segment must be to be significant
        self.x4 = width/10
        self.y4 = height/10
        self.drawer = draw

        self.start = Point(x,y)
        self.mid = Point(x,y)
        self.curr = Point(x,y)
        self.list = [ self.start ]
        self.dropped_last = False

    def draw(self,p1,p2):
        self.drawer(p1.x,p1.y,p2.x,p2.y)
        if p2.cnt > p1.cnt:
            x = (p2.xsum - p1.xsum) / (p2.cnt - p1.cnt)
            y = (p2.ysum - p1.ysum) / (p2.cnt - p1.cnt)
            self.drawer(int(x),int(y),None,None)
        if len(self.list) > 1:
            # find mean of points around this corner
            p0 = self.list[-2]
            ox = (p0.x + p2.x)/2
            oy = (p0.y + p2.y)/2
            # Project to other size of corner
            d = max(abs(p1.x-ox), abs(p1.y-oy))
            if d == 0:
                return
            px = p1.x + (p1.x - ox) * 15 / d
            py = p1.y + (p1.y - oy) * 15 / d

            # now measure curve at p1
            cv = (p2.y-p0.y)*(p1.x-p0.x) - (p1.y-p0.y)*(p2.x-p0.x)
            cv = cv * 100 / ((p2.y-p0.y)*(p2.y-p0.y) + (p2.x-p0.x)*(p2.x-p0.x))

            # or better, measure tan - or cosine of angle
            a = p0.x-p1.x; b=p0.y-p1.y
            c = p2.x-p1.x; d=p2.y-p1.y
            if a*c + b*d != 0:
                tn = 1000*(a*d-b*c)/(a*c+b*d)
                #self.drawer(px,py,None, "%d"%tn)
                x = a*c + b*d
                y = a*d - b*c
                h = math.sqrt(x*x+y*y)
                cs = x*1000/h
                self.drawer(px,py,None, "%d"%cs)

    def straight_to(self, p):
        # is the line from p to self.start longish and straight
        if p.xlen(self.start) < self.x4*2 and p.ylen(self.start) < self.y4*2:
            # too short to care
            return False
        #print "st (%d,%d): %d" % (p.x,p.y,p.curve(self.start))
        return p.curve(self.start) == 0
    
    def add(self, x, y):
        self.curr.add(x,y)

        print 'COSINE', self.mid.cosine(self.start.tupple(),self.curr.tupple())
        if ( (abs(self.mid.xdir(self.start) - self.curr.xdir(self.mid)) != 0) or
             (abs(self.mid.ydir(self.start) - self.curr.ydir(self.mid)) != 0) or
             (self.straight_to(self.mid) and not self.straight_to(self.curr)) or
             (self.mid.cosine(self.start.tupple(),self.curr.tupple()) > -200) or
             (abs(self.curr.Vcurve(self.start))+2 < abs(self.mid.Vcurve(self.start)))
             ):
            pass
        else:
            self.mid = self.curr.copy()

        if self.curr.xlen(self.mid) > self.x4 or self.curr.ylen(self.mid) > self.y4:
            dropped_this = False
            if len(self.list) > 1:
                # start is on the list.  We are about to add 'mid'
                # If start-1..start..mid form a straight line, drop start
                if self.start.cosine(self.list[-2].tupple(), self.mid.tupple()) < -900:
                    self.list = self.list[:-1]
                    self.mid.known_straight = True
                    dropped_this = self.dropped_last
                elif False:
                    if self.start.is_straight(self.list[-2], self.mid) and \
                       not self.dropped_last:
                        self.list = self.list[:-1]
                        self.mid.known_straight = True
                        dropped_this = True
            print "dd", self.dropped_last, dropped_this
            self.dropped_last = dropped_this
            self.draw(self.list[-1],self.mid)
            self.start = self.mid.copy()
            self.list.append(self.start)
            self.mid = self.curr.copy()

    def close(self):
        if len(self.list) > 1:
            # start is on the list.  We are about to add 'curr'
            # If start-1..start..curr form a straight line, drop start
            if self.start.cosine(self.list[-2].tupple(), self.mid.tupple()) < -900:
                self.list = self.list[:-1]
                self.curr.known_straight = True
            elif False:
                if self.start.is_straight(self.list[-2], self.mid) and \
                       not self.dropped_last:
                    self.list = self.list[:-1]
                    self.curr.known_straight = True
        self.draw(self.list[-1], self.curr)
        self.list.append(self.curr)
        print "CLOSE", self.list[-1].x, self.list[-1].y
        #self.draw(self.mid.x,self.mid.y,self.curr.x,self.curr.y)
        #self.list.append(self.curr)
    def text(self):
        h='.'
        v='.'
        ans = ""
        p1 = None
        for p2 in self.list:
            if p1 != None:
                if p2.xlen(p1) > p2.ylen(p1):
                    # horizontal first
                    if p2.xdir(p1) > 0:
                        n = 'r'
                    else:
                        n = 'l'
                    if h != n:
                        h = n
                        ans += n
                    if p2.ylen(p1) > self.y4:
                        # vert too
                        if p2.ydir(p1) > 0:
                            n = 'd'
                        else:
                            n = 'u'
                        if v != n:
                            v = n
                            ans += n
                else:
                    # virt first
                    if p2.ydir(p1) > 0:
                        n = 'd'
                    else:
                        n = 'u'
                    if v != n:
                        v = n
                        ans += n
                    if p2.xlen(p1) > self.x4:
                        # horiz too
                        if p2.xdir(p1) > 0:
                            n = 'r'
                        else:
                            n = 'l'
                        if h != n:
                            h = n
                            ans += n
            p1 = p2
        return ans

    def get_sectlist(self):
        # Break self.list into list of segments, so last point of one is
        # first point of next.
        # We start a new segment when:
        #  - point is curved in the other direction to last point
        #  - point is very sharp - cosine > 0.9 (using meanpoint cosine)
        #  - a third direction change is found much closer to second than first
        #    The new segment begins at the last direction change in other dimension
        #
        # So we keep track of direction changes as indexes into seg
        #
        if len(self.list) <= 2:
            # just one line, it must be straight
            return [(0,self.list)]
        sl = []
        A = self.list[0]
        B = self.list[1]
        seg = [A,B]
        curcurve = 0
        # A is counted as a direction change in both directions
        dchng = [ [0], [0] ]
        for C in self.list[2:]:
            A = seg[-2]
            B = seg[-1]
            print "ABC is %d,%d %d,%d %d,%d" % (A.x,A.y, B.x,B.y, C.x, C.y)
            segbreak = False
            if (# direction change
                ( curcurve and curcurve != B.lcurve(A,C) ) or
                # sharp point
                ( B.cosine(B.meanpoint(A), C.meanpoint(B)) > 900)
                ):
                # B is a segment break
                if curcurve and curcurve != B.lcurve(A,C):
                    print "dir change", curcurve, B.lcurve(A,C)
                else:
                    print "Sharp",  B.cosine(B.meanpoint(A), C.meanpoint(B))
                                                          
                sl.append((curcurve, seg))
                seg = [B, C]
                dchng = [ [0], [0] ]
                curcurve = 0
            else:
                # it is worth seeing if B was a direction change
                # compare x direction against last direction change, as
                #  A to B could be vertical
                breakdir = -1
                print "dc", dchng[0][-1]
                xx = seg[dchng[0][-1]]
                print "xx", xx.x, xx.y
                if abs(C.xdir(B) - B.xdir(seg[dchng[0][-1]])) == 2:
                    # C is in a new direction, so B is a direction change.
                    dchng[0].append(len(seg)-1)
                    print "dirchange X", len(dchng[0])
                    if len(dchng[0]) >= 3:
                        # we have a third change, let's check position
                        ax = seg[dchng[0][-3]].x
                        bx = seg[dchng[0][-2]].x
                        cx = seg[dchng[0][-1]].x
                        print "X: ", abs(cx-bx), abs(bx-ax)
                        if abs(cx-bx) * 4 < abs(bx-ax):
                            # it is very close
                            breakdir = 1
                if abs(C.ydir(B) - B.ydir(seg[dchng[1][-1]])) == 2:
                    # C is in a new direction, so B is a direction change.
                    dchng[1].append(len(seg)-1)
                    print "dirchange Y", len(dchng[1])
                    if len(dchng[1]) >= 3:
                        # we have a third change, let's check position
                        ay = seg[dchng[1][-3]].y
                        by = seg[dchng[1][-2]].y
                        cy = seg[dchng[1][-1]].y
                        print "Y: ", abs(cy-by), abs(by-ay)
                        if abs(cy-by) * 4 < abs(by-ay):
                            # it is very close
                            breakdir = 0
                if breakdir != -1:
                    # break at last direction change
                    # clip current segment to include the break point
                    print "breakdir", breakdir, dchng[breakdir]
                    prev = seg[0:dchng[breakdir][-1]+1]
                    sl.append((curcurve, prev))
                    # and start with the remainder
                    seg = seg[dchng[breakdir][-1]:]
                    dchng = [ [0], [0] ]
                    # and B was a direction change in the other direction
                    dchng[1 - breakdir].append(len(seg)-1)
                seg.append(C)
                curcurve = B.lcurve(A,C)
                print "NOW curcurve", curcurve
        sl.append((curcurve, seg))
        print "sectlist:",
        for (c,j) in sl:
            print "%d,%d " % (c,len(j)),
        print


        # Remove any segment which is v.small. Must be at least 1/6
        nsl = []
        for (c,j) in sl:
            bb = BBox(j[0])
            for i in j:
                bb.add(i)
            print (bb.maxx - bb.minx) ,(self.bbox.maxx - self.bbox.minx), \
                  (bb.maxy - bb.miny), (self.bbox.maxy - self.bbox.miny)
            if ((bb.maxx - bb.minx) * 2 < (self.bbox.x2 - self.bbox.x1) and
                (bb.maxy - bb.miny) * 2 < (self.bbox.y2 - self.bbox.y1)):
                # too small
                pass
            else:
                nsl.append((c,j))
                
        print "new sectlist:",
        for (c,j) in nsl:
            print "%d,%d " % (c,len(j)),
        print
        return nsl
    
    def old_get_sectlist(self):
        if len(self.list) < 2:
            return [[0,self.list]]
        if len(self.list) == 2:
            return [[self.list[1].curve(self.list[0]),self.list]]
        l = []
        A = self.list[0]
        B = self.list[1]
        s = [A,B]
        curcurve = B.curve(A)
        for C in self.list[2:]:

            #cabc = C.curve(A)
            #cabc = curve(A,B,C)
            cabc = B.curve2(A,C)
            cab = B.curve(A)
            cbc = C.curve(B)
            if B.is_sharp(A,C) and not different(cabc, cab, cbc, curcurve):
                # B is too pointy, must break here
                l.append([curcurve, s])
                s = [B, C]
                print "pointy"
                curcurve = cbc
            elif not different(cabc, cab, cbc, curcurve):
                # curvature all happy
                # However if this line intersect a previous line doing in
                # different direction, we need a break.
                dr = intersect_dir(s, C)
                s.append(C)
                if dr == None:
                    print "happy", curcurve, "(",cabc,cab,cbc,")"
                else:
                    (direct, start) = dr
                    print "start =", start, "dir =", direct
                    # if the loop is too fat, we don't cause a break.
                    loop = BBox(s[start+1])
                    for i in range(start+1, len(s)-1):
                        loop.add(s[i])
                    if direct == 'h':
                        ratio = (loop.maxy-loop.miny)*100 / (self.bbox.maxy-self.bbox.miny)
                    else:
                        ratio = (loop.maxx-loop.minx)*100 / (self.bbox.maxx-self.bbox.minx)
                    if ratio < 25:
                        lng = last_change_dir(s, direct)
                        s2 = s[lng:]
                        s = s[0:lng+1]
                        l.append([curcurve, s])
                        s = s2
                        print "happy-break", curcurve, lng, direct, ratio
                    else:
                        print "happy-nobreak", curcurve, direct, ratio
                if curcurve == 0:
                    curcurve = maxcurve(cab, cbc, cabc)
            elif not different(cabc, cab, cbc) and not B.is_sharp(A,C, cutoff=0):
                # gentle inflection along AB
                # was: AB goes in old and new section
                # then: AB only in old section, but curcurve
                #      preseved.
                # now: back to was.
                # If the previous segment was 'gentle' and is
                # only 2 lines (3 points) long, it is totally
                # overlapped and can be discarded.
                if len(l) == 0:
                    l.append([curcurve,s])
                else:
                    print "gg", len(s), A.x,A.y,l[-1][1][-1].x,l[-1][1][-1].y
                    if len(s) == 3 and l[-1][1][-1] == A:
                        print "discard a double gentle"
                        pass
                    else:
                        l.append([curcurve,s])
                s = [A, B, C]
                curcurve =maxcurve(cab, cbc, cabc)
                print "gentle", cabc,cab,cbc, curcurve, "------------------------------------------------------------"
            else:
                # Change of direction at B
                l.append([curcurve,s])
                s = [B, C]
                print "change",cabc,cab,cbc,curcurve
                curcurve = cbc

            A = B
            B = C
        l.append([curcurve,s])

        return l

    def remove_shorts(self):
        # in self.list, if a point is close to the previous point,
        # remove it.
        if len(self.list) <= 2:
            return
        at_start = True
        n = [self.list[0]]
        leng = self.x4*self.y4*2*2
        for p in self.list[1:]:
            l = p.sqlen(n[-1])
            print "rm", leng, l
            #if l * (6*6) > leng:
            #    leng = l
            if l > leng or not at_start:
                n.append(p)
                #at_start = False
            if l > leng:
                lastlong = len(n)
        self.list = n[:lastlong]

    def text2(self):
        # OK, we have a list of points with curvature between.
        # want to divide this into sections.
        # for each 3 consectutive points ABC curve of ABC and AB and BC
        # If all the same, they are all in a section.
        # If not B starts a new section and the old ends on B or C...
        print "points = ", len(self.list)
        if len(self.list) >= 3:
            A = self.list[0]
            B = self.list[1]
            C = self.list[2]
            cos = B.cosine((A.x,A.y),(C.x,C.y))
            print "Cosine =", cos
            sin = B.sine((A.x,A.y),(C.x,C.y))
            print "Sine =", sin
            if cos  < 0:
                s1 = A.sine((B.x,B.y),(C.x,C.y))
                s2 = C.sine((A.x,A.y),(B.x,B.y))
                print "Other Sines are:",s1, s2
            #return "%d / %d" % (cos,sin)
        print "Curve is", self.list[-1].Vcurve(self.list[0])
        #return "Curve is %d" % self.list[-1].Vcurve(self.list[0])
        b4 = len(self.list)
        print "PRE_REMOVE", self.list[-1].x, self.list[-1].y
        self.remove_shorts()
        print "POST_REMOVE", self.list[-1].x, self.list[-1].y
        print "remove shorts: %d -> %d" % (b4, len(self.list))
        BB = BBox(self.list[0])
        for p in self.list:
            BB.add(p)
        BB.finish()
        self.bbox = BB
        sectlist = self.get_sectlist()
        t = ""
        nt = ""
        for c, s in sectlist:
            if c > 0:
                dr = "R"  # clockwise is to the Right
            elif c < 0:
                dr = "L"  # counterclockwise to the Left
            else:
                dr = "S"  # straight
            bb = BBox(s[0])
            for p in s:
                bb.add(p)
            bb.finish()
            # If  all points are in some row or column, then
            # line is S
            rwdiff = False; cldiff = False
            rw = bb.row(s[0]); cl=bb.col(s[0])
            print "test rowcol", c, dr
            for p in s:
                print bb.row(p), bb.col(p)
                if bb.row(p) != rw: rwdiff = True
                if bb.col(p) != cl: cldiff = True
            if not rwdiff or not cldiff: dr = "S"
            print "curves:",
            l0 = None
            l = s[0]
            straight = True
            corners = ""
            for p in s[1:]:
                print "%d[%d]" % (p.Vcurve(l), p.curve(l)),
                if p.curve(l) != 0:
                    straight = False
                if l0 != None:
                    if l.cosine(l0.tupple(), p.tupple()) < -500:
                        # too open a corner to be counted as 'all straight'
                        straight = False
                    print "(%d)" % l.cosine(l0.tupple(), p.tupple()),
                    if p.curve(l) == 0 and l.curve(l0) == 0 and l.cosine(l0.tupple(), p.tupple()) > -100:
                        #this is a corner, count it.
                        corners += "%d" % bb.box(l)
                l0 = l
                l = p
            print "."
            # find direction changes
            changes = -1
            xd = 0; yd= 0
            l = s[0]
            for p in s[1:]:
                xd2 = p.xdir(l); yd2 = p.ydir(l)
                if xd2 != xd and xd2 != 0:
                    changes += 1
                    xd = xd2
                if yd2 != yd and yd2 != 0:
                    changes += 1
                    yd = yd2
                l = p
            print "changes = ", changes

            # No find the direction changes and count the loops.
            # We look particularly at changes in ydir.
            # if 'after' direction is '-1' (change from down to up) and
            # ypos < BB.y2, then this is a 'H' high change
            # if 'after' directons is '1' and ypos > BB.y1, then this is
            # a 'L' low change.
            # else if 'after' isn't 0, this is a 'F' full change
            # We record every H or L, and if we have a string on 'n'
            # F's, we record (n-1)/2 of them.
            # However we start at the last y change before the first (non-initial)
            # x change, and finish after the last (non-final) x change.
            lasty = '-'
            seenx = False
            xd = 0; yd = 0; ypos = s[0].y
            l = s[0]
            lp = LoopList()
            for p in s[1:]:
                xd2 = p.xdir(l); yd2 = p.ydir(l)
                # If both x and y change at the same time we want to handle
                # them in the 'sensible' order. This depends on CW/CCW and dir
                # We do X first if (is CW) == (x was increasing)
                xfirst = (dr == 'R') == (xd == 1)
                print "LL",xfirst, xd,yd,xd2,yd2, ypos, bb.y1, bb.y2, lasty
                if xfirst and xd2 != xd and xd2 != 0:
                    if xd != 0:
                        if not seenx:
                            # this is the first non-initial x-change
                            lp.add(lasty)
                            lasty = '-'
                        seenx = True
                    xd = xd2
                if yd2 != yd and yd2 != 0:
                    if yd2 == -1 and ypos < bb.y2:
                        # This is H iff dr==L and going l-r or
                        #  dr == R and going r-l
                        if (dr == 'L' and xd == 1) or (dr == 'R' and xd == -1):
                            lasty = 'H'
                    elif yd2 == 1 and ypos > bb.y1:
                        if (dr == 'L' and xd == -1) or (dr == 'R' and xd == 1):
                            lasty = 'L'
                    else:
                        lasty = 'F'
                    if lasty != '-':
                        if seenx:
                            print 'x',lasty
                            lp.add(lasty)
                            lasty = '-'
                            seenx = False
                    yd = yd2
                if not xfirst and xd2 != xd and xd2 != 0:
                    if xd != 0:
                        if not seenx:
                            # this is the first non-initial x-change
                            lp.add(lasty)
                            lasty = '-'
                        seenx = True
                    xd = xd2

                l = p
                ypos = p.y
            print "ll",xd,yd, ypos, bb.y1, bb.y2
            # require the last line to be fairly flat to end a loop?? JUSTIFY - GENERALISE need for a 9 that curled like a G
            if yd != 0 and s[-1].xlen(s[-2])*2 > s[-1].ylen(s[-2]):
                lasty = '-'
                if yd == 1 and ypos < bb.y2:
                    if (dr =='L' and xd == 1) or (dr == 'R' and xd == -1):
                        lasty = 'H'
                elif yd == -1 and ypos > bb.y1:
                    if (dr == 'L' and xd == -1) or (dr == 'L' and xd == 1):
                        lasty = 'L'
                else:
                    lasty = 'F'
                if lasty != '-':
                    if seenx:
                        print 'y', lasty
                        lp.add(lasty)
                        lasty = '-'

            loops = lp.getstr()
            print "Loops is", loops
            
            t1 = dr
            t1 += "(%d)" % BB.relpos(bb)
            t1 += "%d,%d" % (bb.box(s[0]), bb.box(s[-1]))
            if straight:
                t1 += 'S'
            else:
                t1 += 'C'
            if changes >= 4:
                t1 += 'L'
            t += t1 + '.'

            t2 = "%c%d:%d-%d" % (dr,BB.relpos(bb), bb.box(s[0]), bb.box(s[-1]))
            if t2 == "R7:6-7":
                # debug weird 'b'
                print "HMMM", bb.minx,bb.maxx,bb.miny,bb.maxy,s[-1].x,s[-1].y
            t2 += loops
            corners2 = ""
            if straight:
                t2 += 'S'
                A = s[0]; B=s[1]
                for C in s[2:]:
                    if abs(Vcurve(A,B,C)) > 10:
                        corners2 += "%d" % bb.box(B)
                    A=B; B=C
            else:
                t2 += 'C'
            t2 += corners
            nt += t2 + '.'
            print "got", t1, t2, corners
            self.drawer(s[0].x,s[0].y+20,None,t1)
        print t, "and", nt
        return nt[:-1]

class SymbolChooser ( gtk.HBox ) :
    def __init__(self) :
        gtk.HBox.__init__(self)
        self.l = gtk.Label("0")
        self.l.show()
        self.syms = [ 'A','B','C','D','E','F','G','H','I','J','K','L','M',
                      'N','O','P','Q','R','S','T','U','V','W','X','Y','Z',
                      '0','1','2','3','4','5','6','7','8','9']
        a = gtk.Adjustment(0,0,35,1,10,0)
        a.connect("value_changed",self.change)
        self.s = gtk.SpinButton(a)
        self.s.set_digits(0)
        self.s.set_size_request(0,40)
        #self.s.set_draw_value(False)
        self.add(self.s)
        self.add(self.l)
        self.s.show()

    def change(self, a):
        self.l.set_text(self.syms[int(a.value)])

class Path:
    def __init__(self, width, height, p1, p2):
        self.x1 = width * p1 / 100
        self.x2 = width * p2 / 100
        self.y1 = height * p1 / 100
        self.y2 = height * p2 / 100
        self.list = []

    def add(self, x, y):
        if x > self.x2:
            x = 2
        elif x > self.x1:
            x = 1
        else:
            x = 0
        if y > self.y2:
            y = 2
        elif y > self.y1:
            y = 1
        else:
            y = 0

        n = y*3+x
        if len(self.list) == 0 or self.list[-1] != n:
            self.list.append(n)

class Path2:
    def __init__(self, width, height, p1, p2, s):
        self.x1 = int(width * p1 / 100)
        self.x2 = int(width * p2 / 100)
        self.y1 = int(height * p1 / 100)
        self.y2 = int(height * p2 / 100)
        self.pcnt = 0
        self.have_start = False
        self.h = '.'
        self.v = '.'
        self.sh = '.'
        self.sv = '.'
        self.show = s
        self.path = ""
        self.straight = 0

        self.crossh1 = 0
        self.crossh2 = 0
        self.crossv1 = 0
        self.crossv2 = 0
        self.crossv = 1
        self.crossh = 1

    def add(self,x,y):
        if  not self.have_start:
            self.x = x
            self.y = y
            self.prevx = x
            self.prevy = y
            self.have_start = True
            return
        if ((self.h != 'l' or x < self.x) and
            (self.h != 'r' or x > self.x) and
            (self.v != 'd' or y > self.y) and
            (self.v != 'u' or y < self.y)) :
            # still travelling in same direction
            if self.pcnt == 0:
                self.startx = x
                self.starty = y
                self.sumx = 0
                self.sumy = 0
            self.endx = x
            self.endy = y
            self.sumx += x
            self.sumy += y
            self.pcnt += 1
            self.nextpcnt = 0

        else :
            # swapped direction. This might be in new path
            if self.nextpcnt == 0:
                self.nextstartx = x
                self.nextstarty = y
                self.nextsumx = 0
                self.nextsumy = 0
            self.nextsumx += x
            self.nextsumy += y
            self.nextpcnt += 1
            # redraw line to show that curvature is ignored.
            self.show(self.x, self.y, x, y)

        change = '.'
        if (self.x < self.x1 and x >= self.x1 or
            self.x < self.x2 and x >= self.x2) :
            # horizontal move to the right
            if self.h == 'l':
                change = 'r'
            else:
                self.h = 'r'
        if (self.x > self.x1 and x <= self.x1 or
            self.x > self.x2 and x <= self.x2) :
            # horizontal move to the left
            if self.h == 'r':
                change = 'l'
            else:
                self.h = 'l'

        if (self.y < self.y1 and y >= self.y1 or
            self.y < self.y2 and y >= self.y2) :
            # vertical move down
            if self.v == 'u':
                change = 'd'
            else:
                self.v = 'd'
        if (self.y > self.y1 and y <= self.y1 or
            self.y > self.y2 and y <= self.y2) :
            # vertical move up
            if self.v == 'd':
                change = 'u'
            else:
                self.v = 'u'

        if (self.x < self.x1 and x >= self.x1 or
            self.x >= self.x1 and x < self.x1) :
            # cross v1
            self.crossv1 += self.crossv
            self.crossv = 2
        if (self.x < self.x2 and x >= self.x2 or
            self.x >= self.x2 and x < self.x2) :
            # cross v2
            self.crossv2 += self.crossv
            self.crossv = 2
        if (self.y < self.y1 and y >= self.y1 or
            self.y >= self.y1 and y < self.y1) :
            # cross h1
            self.crossh1 += self.crossh
            self.crossh = 2
        if (self.y < self.y2 and y >= self.y2 or
            self.y >= self.y2 and y < self.y2) :
            # cross h2
            self.crossh2 += self.crossh
            self.crossh = 2

        self.prevx = self.x; self.x = x
        self.prevy = self.y; self.y = y

        if change == '.':
            return

        print 'change',x,y,change,'(',self.h,self.v,')'


        if self.sh == self.h:
            # Only have vertical info, no curvature
            d = self.v
        elif self.sv == self.v:
            # Only have horizontal info
            d = self.h
        else:
            # Both dimensions changed recently - use curvature
            print "sums", self.sumx, self.sumy, self.pcnt
            if self.pcnt == 0:
                curve = 0
                slope = 0
            else:
                midx = int(self.sumx/self.pcnt)
                midy = int(self.sumy/self.pcnt)
                self.show(midx,midy,None,None)
                self.show(self.startx,self.starty,self.endx,self.endy)

                curve = (self.endy-self.starty)*(midx-self.startx) - (midy-self.starty)*(self.endx-self.startx)
                curve = curve * 100 / ((self.endy-self.starty)*(self.endy-self.starty)
                                       + (self.endx-self.startx)*(self.endx-self.startx))
                if abs(self.starty - self.endy) >= abs(self.startx - self.endx):
                    slope = 2 # steep
                else:
                    slope = 0 # shallow
            
            d = self.h + self.v
            print 'd=',d,'curve=', curve, "slope=", slope
            if curve >= 4:
                # clockwise
                if d == 'ru': d = 'ur'
                if d == 'ld': d = 'dl'
            elif curve <= -4:
                # counterclockwise
                if d == 'rd': d = 'dr'
                if d == 'lu': d = 'ul'
            elif slope > 1:
                # steep slope, v first
                d = self.v + self.h
            else:
                #shallow slow, no change
                pass
            if curve < 4 and curve > -4:
                self.straight += 1
        print "d = ",d
        self.path = self.path + d
        self.startx = self.nextstartx
        self.starty = self.nextstarty
        self.sumx = self.nextsumx
        self.sumy = self.nextsumy
        self.pcnt = self.nextpcnt
        self.nextpcnt = 0

        if self.h != '.':
            self.sh = self.h
            #self.h = '.'
        if self.v != '.':
            self.sv = self.v
            #self.v = '.'

        if change == 'l' or change == 'r':
            self.h = change
        else:
            self.v = change

    def close(self):
        if self.pcnt == 0:
            curve = 0
        else:
            midx = int(self.sumx/self.pcnt)
            midy = int(self.sumy/self.pcnt)
            self.show(midx,midy,None,None)
            self.show(self.startx,self.starty,self.endx,self.endy)
                
            curve = (self.endy-self.starty)*(midx-self.startx) - (midy-self.starty)*(self.endx-self.startx)
            curve = curve * 100 / ((self.endy-self.starty)*(self.endy-self.starty)
                                   + (self.endx-self.startx)*(self.endx-self.startx))
            if abs(self.starty - self.endy) >= abs(self.startx - self.endx):
                slope = 2 # steep
            else:
                slope = 0 # shallow
            if curve < 4 and curve > -4:
                self.straight += 1
            
        # Path is finished - report final status
        if self.sh == self.h:
            # Only have vertical info, no curvature
            d = self.v
        elif self.sv == self.v:
            # Only have horizontal info
            d = self.h
        else:
            # Have full direction and curvature
            d = self.h + self.v
            print 'final d=',d,'curve=', curve, 'slope =', slope
            if curve >= 4 or (curve > -4 and slope > 1):
                # clockwise or steep slope
                if d == 'ru': d = 'ur'
                if d == 'ld': d = 'dl'
            elif curve <= -4:
                # counterclockwise or shallow slope
                if d == 'rd': d = 'dr'
                if d == 'lu': d = 'ul'
        print "final d = ",d
        self.path = self.path + d

        earlytail = ""
        latetail = ""
        if (self.crossh1 + self.crossh2 > 3 and
            (min(self.crossh1,self.crossh2) == 1)):
            # early tail at top or bottom
            earlytail = "t"
        if (self.crossh1 + self.crossh2 > 3 and
            (min(self.crossh1,self.crossh2) == 2)):
            # late tail at top or bottom
            latetail = "t"
        if (self.crossv1 + self.crossv2 > 3 and
            (min(self.crossv1,self.crossv2) == 1)):
            # early tail at left or right
            earlytail = "t"
        if (self.crossv1 + self.crossv2 > 3 and
            (min(self.crossv1,self.crossv2) == 2)):
            # late tail at left or right
            latetail = "t"

        self.path = earlytail + self.path + latetail
        if self.straight:
            self.path += "%d" % self.straight
        return self.path

class GestureLearn:

    def click(self, widget, event):
        self.erase()
        self.learning =  self.learningbutton.get_active()
        print self.learning
        self.down = True
        self.points = []
        x = int(event.x)
        y = int(event.y)
        self.minx = x
        self.maxx = x
        self.miny = y
        self.maxy = y
        self.addpoint(x,y)
        self.bbox1 = BBox(Point(x,y))
        return True

    def release(self, widget, event):
        self.down = False

        if False:
            self.minx=60; self.maxx= self.minx+360
            self.miny=80; self.maxy = self.miny + 480
        else:

            print "position: ", self.classify_bbox()
            print "aspect: ", self.classify_aspect()
            if self.classify_aspect() == 5 :
                mid = (self.maxx+self.minx) / 2
                w = (self.maxy-self.miny) / 2 / 2
                self.minx = int(mid - w)
                self.maxx = int(mid + w)
            if self.classify_aspect() == 1 :
                mid = (self.maxy+self.miny) / 2
                w = (self.maxx-self.minx) / 2 / 2
                self.miny = int(mid - w)
                self.maxy = int(mid + w)
            
        w = self.maxx - self.minx
        h = self.maxy - self.miny
        x = self.minx
        y = self.miny

        self.bbox1.finish()
        self.canvas.window.draw_rectangle(self.box,
                                          False, x,y,w,h)
        #self.drawhash(self.box, x,y,w,h, 33, 66)
        #self.drawhash(self.box1, x,y,w,h, 25, 75)
        #self.drawhash(self.box2, x,y,w,h, 40, 60)
        #self.canvas.window.draw_line(self.box, x,self.bbox1.y1,x+w,self.bbox1.y1)
        #self.canvas.window.draw_line(self.box, x,self.bbox1.y2,x+w,self.bbox1.y2)
        #self.canvas.window.draw_line(self.box, self.bbox1.x1,y,self.bbox1.x1,y+h)
        #self.canvas.window.draw_line(self.box, self.bbox1.x2,y,self.bbox1.x2,y+h)


        #pth =  self.classify_path(33,66)
        #print "path is ", path2str(pth)
        #self.draw_path(pth)
        #self.find_inflect()
        ###result =  ("%d:" % self.classify_bbox())  + self.classify(33,66)
        result = self.classify_ppath()
        self.resultw.set_text(result)
        pos = 'mid'
        if self.classify_bbox() in (4,7,10):
            pos = 'top'
        if self.classify_bbox() in (6,9,12):
            pos = 'bot'
        sym = self.dict.match(result, pos)
        print "result is ", sym
        if sym == "<BS>":
            self.text = "." + self.text[0:-1]
            self.textw.set_text(self.text)
        elif sym:
            self.text = self.text[1:] + sym
            self.textw.set_text(self.text)
            self.storepath(sym, True)
        else:
            self.dict.closest(result)
            self.storepath('??', False)
        return

        snum = len(self.kb)
        copynum = 1
        if snum > 0 and len(self.kb[snum-1]) < 3:
            snum -= 1
            copynum = len(self.kb[snum])+1

        symnum = self.find_match(result)
        #snum is what we expect, symnum is what we found
        if not self.learning or ( symnum >= 0 and (symnum != snum or  self.dups <= 2)):
            self.text = self.text[1:] + self.chars[symnum]
            self.textw.set_text(self.text)
            self.dups += 1
            self.storepath(symnum, True)
        else:
            self.dups = 0
            self.storepath(snum, False)
            if symnum < 0:
                self.storeresult(snum, result)
            if copynum == 1:
                self.kb.append([ result ])
            else:
                self.kb[snum].append(result)
        self.present()

    def parse_points(self, l):
        # l is a line like
        # s: x,y x,y x,y
        # return s, and add all points to 'points'
        self.points = []
        words = l.strip().split(' ')
        # remove leading "'" and trailing "':"
        s = words[0][1:-2]
        start = True
        for w in words[1:]:
            c = w.split(',')
            x = int(c[0])
            y = int(c[1])
            if start:
                self.minx = x; self.maxx = x;
                self.miny = y; self.maxy = y
                self.bbox1 = BBox(Point(x,y))
            self.addpoint(x,y)
            self.bbox1.add(Point(x,y))
            start = False
        self.bbox1.finish()
        return s

    def replay(self):
        # read paths from ppfname, draw them, classify them and add them
        try:
            f = open(self.ppfname, "r")
        except:
            f = None
        if not f:
            return
        open(self.rfname, "w").close()
        l = f.readline()
        linenum = 1
        while len(l) > 0:
            sym = self.parse_points(l)
            self.display.sync()
            self.display.flush()
            self.erase()
            self.redraw(None, None)
            res = self.classify_ppath()
            self.resultw.set_text(res)
            #match = self.find_match(res)
            match = self.dict.match(res,'mid')
            self.display.sync()
            if match == None:
                self.dict.closest(res)
            print "line %d sym '%s' result=%s match=%s ?" % (linenum, sym,res, match),
            if  match == sym or (sym == '??' and match != None):
                l = f.readline()
                linenum += 1
                continue
            ln = sys.stdin.readline()
            ln = 'n'
            #os.exit(0)
            #print "" ; ln = 'y'
            if ln[0] == 'n':
                l = f.readline()
                linenum += 1
                continue
            if ln[0] == 'q':
                break
            if match >= 0 and match != sym:
                print "Sym '%s' clashes with '%s' on result %s\n" % (sym, match, res)
            elif sym < len(self.kb):
                self.kb[sym].append(res)
            elif sym == len(self.kb):
                self.kb.append([ res ])
            if match < 0:
                self.storeresult(sym, res)
            self.storepath(sym, True)
            l = f.readline()
            linenum += 1

    def redraw(self, w, e):
        if self.points == None:
            return
        w = self.maxx - self.minx
        h = self.maxy - self.miny
        x = self.minx
        y = self.miny

        self.canvas.window.draw_rectangle(self.box,
                                          False, x,y,w,h)
        #self.drawhash(self.box, x,y,w,h, 33, 66)
        self.canvas.window.draw_line(self.box, x,self.bbox1.y1,x+w,self.bbox1.y1)
        self.canvas.window.draw_line(self.box, x,self.bbox1.y2,x+w,self.bbox1.y2)
        self.canvas.window.draw_line(self.box, self.bbox1.x1,y,self.bbox1.x1,y+h)
        self.canvas.window.draw_line(self.box, self.bbox1.x2,y,self.bbox1.x2,y+h)

        prev = self.points[0]
        for p in self.points[1:]:
            self.canvas.window.draw_line(self.path,prev[0], prev[1], p[0], p[1])
            print "DrawLine", prev[0]-x, prev[1]-y, p[0]-x, p[1]-y
            #self.canvas.window.draw_rectangle(self.path, True, p[0], p[1], 2,2)
            prev = p

        self.classify_ppath()

    def find_match(self, pattern):
        pattern = pattern[:7]
        for i in range(0, len(self.kb)):
            for p in self.kb[i]:
                if pattern == p:
                    print "match ", p, "at", i
                    return i
        return -1
    
    def motion(self, widget, event):
        if self.down:
            if event.is_hint:
                x, y, state = event.window.get_pointer()
            else:
                x = event.x
                y = event.y
                state = event.state

            widget.window.draw_line(self.path, self.x, self.y, x, y)
            #widget.window.draw_rectangle(self.path, True, self.x, self.y, 2,2)
            self.addpoint(x,y)
            self.bbox1.add(Point(x,y))

        return True

    def drawhash(self, gc, x,y,w,h, p1, p2):
        x1 = x+int(w*p1/100)
        x2 = x+int(w*p2/100)
        y1 = y+int(h*p1/100)
        y2 = y+int(h*p2/100)
        self.canvas.window.draw_line(gc, x,y1,x+w,y1)
        self.canvas.window.draw_line(gc, x,y2,x+w,y2)
        self.canvas.window.draw_line(gc, x1,y,x1,y+h)
        self.canvas.window.draw_line(gc, x2,y,x2,y+h)

    def addpoint(self, x, y):
        self.points.append([x,y])
        self.x = x
        self.y = y
        if x < self.minx:
            self.minx = x
        if x > self.maxx:
            self.maxx = x
        if y < self.miny:
            self.miny = y
        if y > self.maxy:
            self.maxy = y

    def classify_bbox(self):
        if self.maxx < self.width*60/100 and self.minx < self.width * 40/100:
            xpos = 1
        elif self.maxx > self.width*40/100 and self.minx > self.width * 60/100:
            xpos = 3
        else:
            xpos = 2

        if self.maxy < self.width*60/100 and self.miny < self.width * 40/100:
            ypos = 1
        elif self.maxy > self.width*40/100 and self.miny > self.width * 60/100:
            ypos = 3
        else:
            ypos = 2
        #return [xpos, ypos]
        return xpos*3+ypos

    def classify_aspect(self):
        w = self.maxx - self.minx
        h = self.maxy - self.miny

        if w > h * 5:
            return 1
        elif w > h * 2:
            return 2
        elif h > w * 5:
            return 5
        elif h > w * 2:
            return 4
        else:
            return 3

    def classify_path(self, p1, p2):
        p = Path(self.maxx-self.minx, self.maxy-self.miny, p1, p2)
        for x,y in self.points :
            p.add(x-self.minx,y-self.miny)
        #return filter_lines(p.list)
        return p.list

    def draw_path(self, path):
        w = (self.maxx - self.minx) / 3
        h = (self.maxy - self.miny) / 3
        xl = [ w/2, w*3/2, w*5/2, w/2, w*3/2, w*5/2, w/2, w*3/2, w*5/2]
        yl = [ h/2, h/2, h/2, h*3/2, h*3/2, h*3/2, h*5/2, h*5/2, h*5/2]
        st = path.pop(0)
        x = xl[st] + self.minx
        y = yl[st] + self.miny
        o = -10
        for p in path :
            x2 = int(xl[p]) + self.minx
            y2 = int(yl[p]) + self.miny
            self.canvas.window.draw_line(self.final, x+o,y+o,
                                         x2+o+3, y2+o+3)
            o = o + 3
            x = x2
            y = y2
            

    def find_inflect(self):
        # find points that seem to be highly curved and draw a box around them
        step = len(self.points)/4
        step = 4
        print "step is ", step
        if step < 4:
            return
        maxdist = 0
        prevdist = 0
        maxp = [0,0]
        for end in range(step, len(self.points)-1):
            p1 = self.points[end - step]
            p2 = self.points[end - step / 2]
            p3 = self.points[end]
            dist = (p3[1]-p1[1])*(p2[0]-p1[0]) - (p2[1]-p1[1])*(p3[0]-p1[0])
            dist = dist*100 / ((p3[1]-p1[1])*(p3[1]-p1[1]) + (p3[0]-p1[0])*(p3[0]-p1[0]))
            sdist = dist
            dist = abs(dist)
            if dist > maxdist:
                maxdist = dist
                maxp = p2
            p2 = maxp
            if dist < maxdist / 2:
                if maxdist > 30:
                    self.canvas.window.draw_rectangle(self.box2,False,p2[0]-10,p2[1]-10,21,21)
                print "max was ", maxdist
                maxdist = dist
            prevdist = dist
            # now plot this dist along the base of the window. 0-200 fits in bottom
            # third.
            gx = (end - step / 2) * self.width / len(self.points)
            gy = sdist * (self.height/3) / 200
            gy = (self.height/2) - gy
            if gy <= 3:
                gy = 3
            if gy > self.height - 3:
                gy = self.height - 3
            self.canvas.window.draw_rectangle(self.path, False, gx-3,gy-3,7,7)
            
        p2 = self.points[0]
        self.canvas.window.draw_rectangle(self.box1,False,p2[0]-10,p2[1]-10,21,21)
        p2 = self.points[-1]
        self.canvas.window.draw_rectangle(self.box1,False,p2[0]-10,p2[1]-10,21,21)
        self.canvas.window.draw_line(self.path,0,self.height/2,self.width-1,self.height/2)
        return
        print "max dist is ", maxdist
        for end in range(step, len(self.points)-1):
            p1 = self.points[end - step]
            p2 = self.points[end - step / 2]
            p3 = self.points[end]
            dist = (p3[1]-p1[1])*(p2[0]-p1[0]) - (p2[1]-p1[1])*(p3[0]-p1[0])
            dist = dist*100 / ((p3[1]-p1[1])*(p3[1]-p1[1]) + (p3[0]-p1[0])*(p3[0]-p1[0]))
            dist = abs(dist)
            if dist > maxdist/2 :
                self.canvas.window.draw_rectangle(self.box1,False,p2[0]-10,p2[1]-10,21,21)

    def nocurve(self, x1,y1,x2,y2):
        if x2 == None and y2 == None:
            self.canvas.window.draw_rectangle(self.box1,False, x1+self.minx-5, y1+self.miny-5,10,10)
        elif x2 == None:
            l = self.canvas.create_pango_layout("")
            # 10000 on notebook 2000 on freerunner
            l.set_markup('<span size="10000">'+y2+'</span>')
            self.canvas.window.draw_layout(self.box1, x1+self.minx-10,y1+self.miny-10,
                                           l)
        else:
            self.canvas.window.draw_line(self.box1, x1+self.minx, y1+self.miny,
                                         x2+self.minx, y2+self.miny)
    def classify(self, p1, p2):
        p = Path2(self.maxx-self.minx, self.maxy-self.miny, p1, p2, self.nocurve)
        for x,y in self.points:
            p.add(x - self.minx, y-self.miny)
        return p.close()

    def classify_ppath(self):
        p = PPath(self.maxx-self.minx, self.maxy-self.miny,
                  self.points[0][0]-self.minx, self.points[0][1]-self.miny,
                  self.nocurve)
        for i in range(1, len(self.points)-1):
            p.add(self.points[i][0] - self.minx, self.points[i][1] - self.miny)
        p.close()
        return p.text2()
    
    def erase(self):
        self.canvas.window.draw_rectangle(self.bg,
                                          True, 0, 0, 480, 640)
        x=60; w=360
        y=80; h=480
        self.canvas.window.draw_rectangle(self.box2,
                                          False, x,y,w,h)
        self.drawhash(self.box2, x,y,w,h, 33, 66)

    def storepath(self, sym, isgood):
        # write out the pixel path self.points to file self.log
        if isgood:
            f = open(self.ppfnewname, "a")
        else:
            f = open(self.ppfnewname+'.bad', "a")
        f.write("'%s':" % sym)
        for x,y in self.points:
            f.write(" %d,%d" % (x,y))
        f.write("\n")
        f.close()

    def storeresult(self, symnum, result):
        f = open(self.rfname, "a")
        f.write("%d: %s\n"% (symnum, result))
        f.close()

    def close_application(self, widget):
        gtk.main_quit()


    def present(self):
        symnum = len(self.kb)
        copynum = 1
        if symnum > 0 and len(self.kb[symnum-1]) < 3:
            symnum -= 1
            copynum = len(self.kb[symnum])+1
        if symnum >= len(self.chars):
            self.sym.set_text("-done-")
            self.learning = False
            self.learningbutton.set_active(False)
        else:
            self.sym.set_text("%s %d" % (self.chars[symnum], copynum))

    def __init__(self):
        window = gtk.Window(gtk.WINDOW_TOPLEVEL)
        window.set_resizable(False)
        window.connect("destroy", self.close_application)
        window.set_title("Experiment Gesture Learning")
        window.set_size_request(480,640)
        self.display =window.get_screen().get_display()

        canvas = gtk.DrawingArea()
        canvas.set_size_request(480,640)
        self.width = 480
        self.height = 640

        canvas.connect("button_press_event", self.click)
        canvas.connect("button_release_event", self.release)
        canvas.connect("motion_notify_event", self.motion)
        canvas.connect("expose-event", self.redraw)

        canvas.set_events(gtk.gdk.EXPOSURE_MASK
                          | gtk.gdk.LEAVE_NOTIFY_MASK
                          | gtk.gdk.BUTTON_PRESS_MASK
                          | gtk.gdk.BUTTON_RELEASE_MASK
                          | gtk.gdk.POINTER_MOTION_MASK
                          | gtk.gdk.POINTER_MOTION_HINT_MASK)

        self.canvas = canvas
        self.down = False
        self.lastx = 0
        self.lasty = 0

        fix = gtk.Fixed()
        window.add(fix)
        fix.put(canvas,0,0)

        eb = gtk.EventBox()
        eb.show()
        fix.put(eb,0,0)
        vb = gtk.VBox()
        vb.show()
        eb.add(vb)
        #thing.set_size_request(20,20)

        b1 = gtk.RadioButton(None, "Learn")
        vb.add(b1); b1.show()
        b2 = gtk.RadioButton(b1, "Match")
        vb.add(b2); b2.show()
        self.learningbutton = b1
        b2.set_active(True)

        sym = gtk.Label("  ")
        vb.add(sym)
        sym.show()
        self.sym = sym
        self.text = ".........."
        self.textw = gtk.Label(self.text)
        self.textw.show()
        vb.add(self.textw)

        self.resultw = gtk.Label("")
        self.resultw.show()
        vb.add(self.resultw)
        
        
        canvas.show()
        fix.show()
        window.show()
        colormap = canvas.get_colormap()
        red = gtk.gdk.color_parse("red");
        blue = gtk.gdk.color_parse("blue"); 
        orange = gtk.gdk.color_parse("orange");
        purple = gtk.gdk.color_parse("purple");
        self.bg = canvas.get_style().bg_gc[gtk.STATE_NORMAL]

        self.path = canvas.window.new_gc()
        self.path.line_width = 2
        self.path.set_foreground(colormap.alloc_color(red))

        self.box = canvas.window.new_gc()

        self.box1 = canvas.window.new_gc()
        self.box1.set_foreground(colormap.alloc_color(blue))
        self.box2 = canvas.window.new_gc()
        self.box2.set_foreground(colormap.alloc_color(orange))

        self.final = canvas.window.new_gc()
        self.final.set_foreground(colormap.alloc_color(purple))

        self.points = None

        self.chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"
        self.kb = [];
        self.dups = 0;
        self.present()

        nowstr = time.strftime("%Y-%m-%d-%H-%M-%S")

        self.ppfname = "pixelpath"
        self.ppfnewname = "pixelpath." + nowstr
        if len(sys.argv) >= 2:
            self.ppfname = sys.argv[1]
            self.ppfnewname = sys.argv[1] + ".new"
            open(self.ppfnewname, "w").close()
        self.rfname = "result"

        self.dict = Dictionary()
        LoadDict(self.dict)
        self.replay()
        self.present()


def main():
    gtk.main()
    return 0       

if __name__ == "__main__":
    GestureLearn()
    main()
