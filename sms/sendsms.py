#!/usr/bin/env python

# Create/edit/send/display/search SMS messages.
# Two main displays:  Create and display
# Create:
#   Allow entry of recipient and text of SMS message and allow basic editting
#    When entering recipient, text box can show address matches for selection
#     Bottom buttons are "Select"..
#    When entering text, if there is no text, buttom buttons are:
#       "Browse", "Close"
#       If these is some text, bottom buttons are:
#        "Send", Save"
#
# Display:
#   We usually display a list of messages which can be selected from
#    There is a 'search' box to restrict message to those with a string
#   Options for selected message are:
#     Delete Reply View  Open(for draft)/Forward(for non-draft)
#   In View mode, the whole text is displayed and the 'View' button becomes "Index"
#     or "Show List"  or "ReadIt"
#   General options are:
#     New Config  List
#    New goes to Edit
#
#   Delete becomes Undelete and can undelete a whole stack.
#    Delete can become undelete without deleting be press-and-hold
#
#
# Messages are sent using a separate program. e.g. sms-gsm
# Different recipients can use different programs based on flag in address book.
# Somehow senders can be configured.
#   e.g. sms-exetel needs username, password, sender strings.
#   press-and-hold on the send button allows a sender to be selected.
#     
#
# Send an SMS message using some backend.

# 
#
# TODO:
#   'del' to return to 'list' view
#   top buttons:  del, view/list, new/open/reply
#           so can only reply when viewing whole message
#   Bottom:
#      all:   sent recv
#      send:  all  draft
#      recv:  all new
#      draft:  all  sent
#      new:   all recv
#   DONE handle newline chars in summary
#   DONE cope properly when the month changes.
#   switch-to-'new' on 'expose'
#   'draft' button becomes 'cancel' when all is empty
#   DONE better display of name/number of destination
#   jump to list mode when change 'list'
#   'open' becomes 'reply' when current message was received.
#   new message becomes non-new when replied to
#   '<list>' button doesn't select, but just makes choice.
#      'new' becomes 'select' when <list> has been pressed.
#   DONE Start in 'read', preferrably 'new' 
#   DONE always report status from send
#   DONE draft/new/recv/sent/all  - 5 groups
#   DONE allow scrolling through list
#   DONE + prefix to work
#   DONE compose as 'GSM' or 'EXE' send
#   DONE somehow do addressbook lookup for compose
#   DONE addressbook lookup for display
#   On 'send' move to 'sent' (not draft) and display list
#   When open 'draft', delete from drafts... or later..
#   When 'reply' to new message, make it not 'new'
#
#   get 'cut' to work from phone number entry.
#   how to configure sender...
#   need to select 'number only' mode for entry
#   need drop-down of common numbers
#   DONE text wrapping
#   punctuation
#   faster text input!!!
#   DONE status message of transmission
#   DONE maybe go to 'past messages' on send - need to go somewhere
#   cut from other sources??
#   DONE scroll if message is too long!
#
#   DONE reread sms file when changing view
#   Don't add drafts that have not been changed... or
#   When opening a draft, delete it... or replace when re-add
#   DONE when sending a message, store - as draft if send failed
#   DONE show the 'send' status somewhere
#   DONE add a 'new' button from 'list' to 'send'
#   Need 'reply' button.. Make 'open' show 'reply' when 'to' me.
#   Scroll when near top or bottom
#   hide status line when not needed.
#   searching mesg list
#   'folder' view - by month or day
#   highlight 'new' and 'draft' messages in different colour
#   support 'sent' and 'received' distinction
#   when return from viewing a 'new' message, clear the 'new' status
#   enable starting in 'listing/New' mode

import gtk, pango
import sys, time, os, re
import struct
from subprocess import Popen, PIPE
from storesms import SMSstore, SMSmesg
import dnotify

###########################################################
# Writing recognistion code
import math


def LoadDict(dict):
    # Upper case.
    # Where they are like lowercase, we either double
    # the last stroke (L, J, I) or draw backwards (S, Z, X)
    # U V are a special case

    dict.add('A', "R(4)6,8")
    dict.add('B', "R(4)6,4.R(7)1,6")
    dict.add('B', "R(4)6,4.L(4)2,8.R(7)1,6")
    dict.add('B', "S(6)7,1.R(4)6,4.R(7)0,6")
    dict.add('C', "R(4)8,2")
    dict.add('D', "R(4)6,6")
    dict.add('E', "L(1)2,8.L(7)2,8")
    # double the stem for F
    dict.add('F', "L(4)2,6.S(3)7,1")
    dict.add('F', "S(1)5,3.S(3)1,7.S(3)7,1")

    dict.add('G', "L(4)2,5.S(8)1,7")
    dict.add('G', "L(4)2,5.R(8)6,8")
    # FIXME I need better straight-curve alignment
    dict.add('H', "S(3)1,7.R(7)6,8.S(5)7,1")
    dict.add('H', "L(3)0,5.R(7)6,8.S(5)7,1")
    # capital I is down/up
    dict.add('I', "S(4)1,7.S(4)7,1")

    # Capital J has a left/right tail
    dict.add('J', "R(4)1,6.S(7)3,5")

    dict.add('K', "L(4)0,2.R(4)6,6.L(4)2,8")

    # Capital L, like J, doubles the foot
    dict.add('L', "L(4)0,8.S(7)4,3")

    dict.add('M', "R(3)6,5.R(5)3,8")
    dict.add('M', "R(3)6,5.L(1)0,2.R(5)3,8")

    dict.add('N', "R(3)6,8.L(5)0,2")

    # Capital O is CW, but can be CCW in special dict
    dict.add('O', "R(4)1,1", bot='0')

    dict.add('P', "R(4)6,3")
    dict.add('Q', "R(4)7,7.S(8)0,8")

    dict.add('R', "R(4)6,4.S(8)0,8")

    # S is drawn bottom to top.
    dict.add('S', "L(7)6,1.R(1)7,2")

    # Double the stem for capital T
    dict.add('T', "R(4)0,8.S(5)7,1")

    # U is L to R, V is R to L for now
    dict.add('U', "L(4)0,2")
    dict.add('V', "R(4)2,0")

    dict.add('W', "R(5)2,3.L(7)8,6.R(3)5,0")
    dict.add('W', "R(5)2,3.R(3)5,0")

    dict.add('X', "R(4)6,0")

    dict.add('Y',"L(1)0,2.R(5)4,6.S(5)6,2")
    dict.add('Y',"L(1)0,2.S(5)2,7.S(5)7,2")

    dict.add('Z', "R(4)8,2.L(4)6,0")

    # Lower case
    dict.add('a', "L(4)2,2.L(5)1,7")
    dict.add('a', "L(4)2,2.L(5)0,8")
    dict.add('a', "L(4)2,2.S(5)0,8")
    dict.add('b', "S(3)1,7.R(7)6,3")
    dict.add('c', "L(4)2,8", top='C')
    dict.add('d', "L(4)5,2.S(5)1,7")
    dict.add('d', "L(4)5,2.L(5)0,8")
    dict.add('e', "S(4)3,5.L(4)5,8")
    dict.add('e', "L(4)3,8")
    dict.add('f', "L(4)2,6", top='F')
    dict.add('f', "S(1)5,3.S(3)1,7", top='F')
    dict.add('g', "L(1)2,2.R(4)1,6")
    dict.add('h', "S(3)1,7.R(7)6,8")
    dict.add('h', "L(3)0,5.R(7)6,8")
    dict.add('i', "S(4)1,7", top='I', bot='1')
    dict.add('j', "R(4)1,6", top='J')
    dict.add('k', "L(3)0,5.L(7)2,8")
    dict.add('k', "L(4)0,5.R(7)6,6.L(7)1,8")
    dict.add('l', "L(4)0,8", top='L')
    dict.add('l', "S(3)1,7.S(7)3,5", top='L')
    dict.add('m', "S(3)1,7.R(3)6,8.R(5)6,8")
    dict.add('m', "L(3)0,2.R(3)6,8.R(5)6,8")
    dict.add('n', "S(3)1,7.R(4)6,8")
    dict.add('o', "L(4)1,1", top='O', bot='0')
    dict.add('p', "S(3)1,7.R(4)6,3")
    dict.add('q', "L(1)2,2.L(5)1,5")
    dict.add('q', "L(1)2,2.S(5)1,7.R(8)6,2")
    dict.add('q', "L(1)2,2.S(5)1,7.S(5)1,7")
    # FIXME this double 1,7 is due to a gentle where the
    # second looks like a line because it is narrow.??
    dict.add('r', "S(3)1,7.R(4)6,2")
    dict.add('s', "L(1)2,7.R(7)1,6", top='S', bot='5')
    dict.add('t', "R(4)0,8", top='T', bot='7')
    dict.add('t', "S(1)3,5.S(5)1,7", top='T', bot='7')
    dict.add('u', "L(4)0,2.S(5)1,7")
    dict.add('v', "L(4)0,2.L(2)0,2")
    dict.add('w', "L(3)0,2.L(5)0,2", top='W')
    dict.add('w', "L(3)0,5.R(7)6,8.L(5)3,2", top='W')
    dict.add('w', "L(3)0,5.L(5)3,2", top='W')
    dict.add('x', "L(4)0,6", top='X')
    dict.add('y', "L(1)0,2.R(5)4,6", top='Y') # if curved
    dict.add('y', "L(1)0,2.S(5)2,7", top='Y')
    dict.add('z', "R(4)0,6.L(4)2,8", top='Z', bot='2')

    # Digits
    dict.add('0', "L(4)7,7")
    dict.add('0', "R(4)7,7")
    dict.add('1', "S(4)7,1")
    dict.add('2', "R(4)0,6.S(7)3,5")
    dict.add('2', "R(4)3,6.L(4)2,8")
    dict.add('3', "R(1)0,6.R(7)1,6")
    dict.add('4', "L(4)7,5")
    dict.add('5', "L(1)2,6.R(7)0,3")
    dict.add('5', "L(1)2,6.L(4)0,8.R(7)0,3")
    dict.add('6', "L(4)2,3")
    dict.add('7', "S(1)3,5.R(4)1,6")
    dict.add('7', "R(4)0,6")
    dict.add('7', "R(4)0,7")
    dict.add('8', "L(4)2,8.R(4)4,2.L(3)6,1")
    dict.add('8', "L(1)2,8.R(7)2,0.L(1)6,1")
    dict.add('8', "L(0)2,6.R(7)0,1.L(2)6,0")
    dict.add('8', "R(4)2,6.L(4)4,2.R(5)8,1")
    dict.add('9', "L(1)2,2.S(5)1,7")

    dict.add(' ', "S(4)3,5")
    dict.add('<BS>', "S(4)5,3")
    dict.add('-', "S(4)3,5.S(4)5,3")
    dict.add('_', "S(4)3,5.S(4)5,3.S(4)3,5")
    dict.add("<left>", "S(4)5,3.S(3)3,5")
    dict.add("<right>","S(4)3,5.S(5)5,3")
    dict.add("<left>", "S(4)7,1.S(1)1,7") # "<up>"
    dict.add("<right>","S(4)1,7.S(7)7,1") # "<down>"
    dict.add("<newline>", "S(4)2,6")


class DictSegment:
    # Each segment has for elements:
    #   direction: Right Straight Left (R=cw, L=ccw)
    #   location: 0-8.
    #   start: 0-8
    #   finish: 0-8
    # Segments match if there difference at each element
    # is 0, 1, or 3 (RSL coded as 012)
    # A difference of 1 required both to be same / 3
    # On a match, return number of 0s
    # On non-match, return -1
    def __init__(self, str):
        # D(L)S,R
        # 0123456
        self.e = [0,0,0,0]
        if len(str) != 7:
            raise ValueError
        if str[1] != '(' or str[3] != ')' or str[5] != ',':
            raise ValueError
        if str[0] == 'R':
            self.e[0] = 0
        elif str[0] == 'L':
            self.e[0] = 2
        elif str[0] == 'S':
            self.e[0] = 1
        else:
            raise ValueError

        self.e[1] = int(str[2])
        self.e[2] = int(str[4])
        self.e[3] = int(str[6])

    def match(self, other):
        cnt = 0
        for i in range(0,4):
            diff = abs(self.e[i] - other.e[i])
            if diff == 0:
                cnt += 1
            elif diff == 3:
                pass
            elif diff == 1 and (self.e[i]/3 == other.e[i]/3):
                pass
            else:
                return -1
        return cnt

class DictPattern:
    # A Dict Pattern is a list of segments.
    # A parsed pattern matches a dict pattern if
    # the are the same nubmer of segments and they
    # all match.  The value of the match is the sum
    # of the individual matches.
    # A DictPattern is printers as segments joined by periods.
    #
    def __init__(self, str):
        self.segs = map(DictSegment, str.split("."))
    def match(self,other):
        if len(self.segs) != len(other.segs):
            return -1
        cnt = 0
        for i in range(0,len(self.segs)):
            m = self.segs[i].match(other.segs[i])
            if m < 0:
                return m
            cnt += m
        return cnt


class Dictionary:
    # The dictionary hold all the pattern for symbols and
    # performs lookup
    # Each pattern in the directionary can be associated
    # with  3 symbols.  One when drawing in middle of screen,
    # one for top of screen, one for bottom.
    # Often these will all be the same.
    # This allows e.g. s and S to have the same pattern in different
    # location on the touchscreen.
    # A match requires a unique entry with a match that is better
    # than any other entry.
    #
    def __init__(self):
        self.dict = []
    def add(self, sym, pat, top = None, bot = None):
        if top == None: top = sym
        if bot == None: bot = sym
        self.dict.append((DictPattern(pat), sym, top, bot))

    def _match(self, p):
        max = -1
        val = None
        for (ptn, sym, top, bot) in self.dict:
            cnt = ptn.match(p)
            if cnt > max:
                max = cnt
                val = (sym, top, bot)
            elif cnt == max:
                val = None
        return val

    def match(self, str, pos = "mid"):
        p = DictPattern(str)
        m = self._match(p)
        if m == None:
            return m
        (mid, top, bot) = self._match(p)
        if pos == "top": return top
        if pos == "bot": return bot
        return mid


class Point:
    # This represents a point in the path and all the points leading
    # up to it.  It allows us to find the direction and curvature from
    # one point to another
    # We store x,y, and sum/cnt of points so far
    def __init__(self,x,y) :
        self.xsum = x
        self.ysum = y
        self.x = x
        self.y = y
        self.cnt = 1

    def copy(self):
        n = Point(0,0)
        n.xsum = self.xsum
        n.ysum = self.ysum
        n.x = self.x
        n.y = self.y
        n.cnt = self.cnt
        return n

    def add(self,x,y):
        if self.x == x and self.y == y:
            return
        self.x = x
        self.y = y
        self.xsum += x
        self.ysum += y
        self.cnt += 1

    def xlen(self,p):
        return abs(self.x - p.x)
    def ylen(self,p):
        return abs(self.y - p.y)
    def sqlen(self,p):
        x = self.x - p.x
        y = self.y - p.y
        return x*x + y*y

    def xdir(self,p):
        if self.x > p.x:
            return 1
        if self.x < p.x:
            return -1
        return 0
    def ydir(self,p):
        if self.y > p.y:
            return 1
        if self.y < p.y:
            return -1
        return 0
    def curve(self,p):
        if self.cnt == p.cnt:
            return 0
        x1 = p.x ; y1 = p.y
        (x2,y2) = self.meanpoint(p)
        x3 = self.x; y3 = self.y

        curve = (y3-y1)*(x2-x1) - (y2-y1)*(x3-x1)
        curve = curve * 100 / ((y3-y1)*(y3-y1)
                               + (x3-x1)*(x3-x1))
        if curve > 6:
            return 1
        if curve < -6:
            return -1
        return 0

    def Vcurve(self,p):
        if self.cnt == p.cnt:
            return 0
        x1 = p.x ; y1 = p.y
        (x2,y2) = self.meanpoint(p)
        x3 = self.x; y3 = self.y

        curve = (y3-y1)*(x2-x1) - (y2-y1)*(x3-x1)
        curve = curve * 100 / ((y3-y1)*(y3-y1)
                               + (x3-x1)*(x3-x1))
        return curve

    def meanpoint(self,p):
        x = (self.xsum - p.xsum) / (self.cnt - p.cnt)
        y = (self.ysum - p.ysum) / (self.cnt - p.cnt)
        return (x,y)

    def is_sharp(self,A,C):
        # Measure the cosine at self between A and C
        # as A and C could be curve, we take the mean point on
        # self.A and self.C as the points to find cosine between
        (ax,ay) = self.meanpoint(A)
        (cx,cy) = self.meanpoint(C)
        a = ax-self.x; b=ay-self.y
        c = cx-self.x; d=cy-self.y
        x = a*c + b*d
        y = a*d - b*c
        h = math.sqrt(x*x+y*y)
        if h > 0:
            cs = x*1000/h
        else:
            cs = 0
        return (cs > 900)

class BBox:
    # a BBox records min/max x/y of some Points and
    # can subsequently report row, column, pos of each point
    # can also locate one bbox in another

    def __init__(self, p):
        self.minx = p.x
        self.maxx = p.x
        self.miny = p.y
        self.maxy = p.y

    def width(self):
        return self.maxx - self.minx
    def height(self):
        return self.maxy - self.miny

    def add(self, p):
        if p.x > self.maxx:
            self.maxx = p.x
        if p.x < self.minx:
            self.minx = p.x

        if p.y > self.maxy:
            self.maxy = p.y
        if p.y < self.miny:
            self.miny = p.y
    def finish(self, div = 3):
        # if aspect ratio is bad, we adjust max/min accordingly
        # before setting [xy][12].  We don't change self.min/max
        # as they are used to place stroke in bigger bbox.
        # Normally divisions are at 1/3 and 2/3. They can be moved
        # by setting div e.g. 2 = 1/2 and 1/2
        (minx,miny,maxx,maxy) = (self.minx,self.miny,self.maxx,self.maxy)
        if (maxx - minx) * 3 < (maxy - miny) * 2:
            # too narrow
            mid = int((maxx + minx)/2)
            halfwidth = int ((maxy - miny)/3)
            minx = mid - halfwidth
            maxx = mid + halfwidth
        if (maxy - miny) * 3 < (maxx - minx) * 2:
            # too wide
            mid = int((maxy + miny)/2)
            halfheight = int ((maxx - minx)/3)
            miny = mid - halfheight
            maxy = mid + halfheight

        div1 = div - 1
        self.x1 = int((div1*minx + maxx)/div)
        self.x2 = int((minx + div1*maxx)/div)
        self.y1 = int((div1*miny + maxy)/div)
        self.y2 = int((miny + div1*maxy)/div)

    def row(self, p):
        # 0, 1, 2 - top to bottom
        if p.y <= self.y1:
            return 0
        if p.y < self.y2:
            return 1
        return 2
    def col(self, p):
        if p.x <= self.x1:
            return 0
        if p.x < self.x2:
            return 1
        return 2
    def box(self, p):
        # 0 to 9
        return self.row(p) * 3 + self.col(p)

    def relpos(self,b):
        # b is a box within self.  find location 0-8
        if b.maxx < self.x2 and b.minx < self.x1:
            x = 0
        elif b.minx > self.x1 and b.maxx > self.x2:
            x = 2
        else:
            x = 1
        if b.maxy < self.y2 and b.miny < self.y1:
            y = 0
        elif b.miny > self.y1 and b.maxy > self.y2:
            y = 2
        else:
            y = 1
        return y*3 + x


def different(*args):
    cur = 0
    for i in args:
        if cur != 0 and i != 0 and cur != i:
            return True
        if cur == 0:
            cur = i
    return False

def maxcurve(*args):
    for i in args:
        if i != 0:
            return i
    return 0

class PPath:
    # a PPath refines a list of x,y points into a list of Points
    # The Points mark out segments which end at significant Points
    # such as inflections and reversals.

    def __init__(self, x,y):

        self.start = Point(x,y)
        self.mid = Point(x,y)
        self.curr = Point(x,y)
        self.list = [ self.start ]

    def add(self, x, y):
        self.curr.add(x,y)

        if ( (abs(self.mid.xdir(self.start) - self.curr.xdir(self.mid)) == 2) or
             (abs(self.mid.ydir(self.start) - self.curr.ydir(self.mid)) == 2) or
             (abs(self.curr.Vcurve(self.start))+2 < abs(self.mid.Vcurve(self.start)))):
            pass
        else:
            self.mid = self.curr.copy()

        if self.curr.xlen(self.mid) > 4 or self.curr.ylen(self.mid) > 4:
            self.start = self.mid.copy()
            self.list.append(self.start)
            self.mid = self.curr.copy()

    def close(self):
        self.list.append(self.curr)

    def get_sectlist(self):
        if len(self.list) <= 2:
            return [[0,self.list]]
        l = []
        A = self.list[0]
        B = self.list[1]
        s = [A,B]
        curcurve = B.curve(A)
        for C in self.list[2:]:
            cabc = C.curve(A)
            cab = B.curve(A)
            cbc = C.curve(B)
            if B.is_sharp(A,C) and not different(cabc, cab, cbc, curcurve):
                # B is too pointy, must break here
                l.append([curcurve, s])
                s = [B, C]
                curcurve = cbc
            elif not different(cabc, cab, cbc, curcurve):
                # all happy
                s.append(C)
                if curcurve == 0:
                    curcurve = maxcurve(cab, cbc, cabc)
            elif not different(cabc, cab, cbc)  :
                # gentle inflection along AB
                # was: AB goes in old and new section
                # now: AB only in old section, but curcurve
                #      preseved.
                l.append([curcurve,s])
                s = [A, B, C]
                curcurve =maxcurve(cab, cbc, cabc)
            else:
                # Change of direction at B
                l.append([curcurve,s])
                s = [B, C]
                curcurve = cbc

            A = B
            B = C
        l.append([curcurve,s])

        return l

    def remove_shorts(self, bbox):
        # in self.list, if a point is close to the previous point,
        # remove it.
        if len(self.list) <= 2:
            return
        w = bbox.width()/10
        h = bbox.height()/10
        n = [self.list[0]]
        leng = w*h*2*2
        for p in self.list[1:]:
            l = p.sqlen(n[-1])
            if l > leng:
                n.append(p)
        self.list = n

    def text(self):
        # OK, we have a list of points with curvature between.
        # want to divide this into sections.
        # for each 3 consectutive points ABC curve of ABC and AB and BC
        # If all the same, they are all in a section.
        # If not B starts a new section and the old ends on B or C...
        BB = BBox(self.list[0])
        for p in self.list:
            BB.add(p)
        BB.finish()
        self.bbox = BB
        self.remove_shorts(BB)
        sectlist = self.get_sectlist()
        t = ""
        for c, s in sectlist:
            if c > 0:
                dr = "R"  # clockwise is to the Right
            elif c < 0:
                dr = "L"  # counterclockwise to the Left
            else:
                dr = "S"  # straight
            bb = BBox(s[0])
            for p in s:
                bb.add(p)
            bb.finish()
            # If  all points are in some row or column, then
            # line is S
            rwdiff = False; cldiff = False
            rw = bb.row(s[0]); cl=bb.col(s[0])
            for p in s:
                if bb.row(p) != rw: rwdiff = True
                if bb.col(p) != cl: cldiff = True
            if not rwdiff or not cldiff: dr = "S"

            t1 = dr
            t1 += "(%d)" % BB.relpos(bb)
            t1 += "%d,%d" % (bb.box(s[0]), bb.box(s[-1]))
            t += t1 + '.'
        return t[:-1]


class text_input:
    def __init__(self, page, callout):

        self.page = page
        self.callout = callout
        self.colour = None
        self.line = None
        self.dict = Dictionary()
        self.active = True
        LoadDict(self.dict)

        page.connect("button_press_event", self.press)
        page.connect("button_release_event", self.release)
        page.connect("motion_notify_event", self.motion)
        page.set_events(page.get_events()
                        | gtk.gdk.BUTTON_PRESS_MASK
                        | gtk.gdk.BUTTON_RELEASE_MASK
                        |  gtk.gdk.POINTER_MOTION_MASK
                        | gtk.gdk.POINTER_MOTION_HINT_MASK)

    def set_colour(self, col):
        self.colour = col
    
    def press(self, c, ev):
        if not self.active:
            return
        # Start a new line
        self.line = [ [int(ev.x), int(ev.y)] ]
        if not ev.send_event:
            self.page.stop_emission('button_press_event')
        return
    def release(self, c, ev):
        if self.line == None:
            return
        if len(self.line) == 1:
            self.callout('click', ev)
            self.line = None
            return

        sym = self.getsym()
        if sym:
            self.callout('sym', sym)
        self.callout('redraw', None)
        self.line = None
        return

    def motion(self, c, ev):
        if self.line:
            if ev.is_hint:
                x, y, state = ev.window.get_pointer()
            else:
                x = ev.x
                y = ev.y
            x = int(x)
            y = int(y)
            prev = self.line[-1]
            if abs(prev[0] - x) < 10 and abs(prev[1] - y) < 10:
                return
            if self.colour:
                c.window.draw_line(self.colour, prev[0],prev[1],x,y)
            self.line.append([x,y])
        return

    def getsym(self):
        alloc = self.page.get_allocation()
        pagebb = BBox(Point(0,0))
        pagebb.add(Point(alloc.width, alloc.height))
        pagebb.finish(div = 2)

        p = PPath(self.line[1][0], self.line[1][1])
        for pp in self.line[1:]:
            p.add(pp[0], pp[1])
        p.close()
        patn = p.text()
        pos = pagebb.relpos(p.bbox)
        tpos = "mid"
        if pos < 3:
            tpos = "top"
        if pos >= 6:
            tpos = "bot"
        sym = self.dict.match(patn, tpos)
        if sym == None:
            print "Failed to match pattern:", patn
        return sym





########################################################################



class FingerText(gtk.TextView):
    def __init__(self):
        gtk.TextView.__init__(self)
        self.set_wrap_mode(gtk.WRAP_WORD_CHAR)
        self.exphan = self.connect('expose-event', self.config)
        self.input = text_input(self, self.stylus)

    def config(self, *a):
        self.disconnect(self.exphan)
        c = gtk.gdk.color_parse('red')
        gc = self.window.new_gc()
        gc.set_foreground(self.get_colormap().alloc_color(c))
        #gc.set_line_attributes(2, gtk.gdk.LINE_SOLID, gtk.gdk.CAP_ROUND, gtk.gdk.JOIN_ROUND)
        gc.set_subwindow(gtk.gdk.INCLUDE_INFERIORS)
        self.input.set_colour(gc)

    def stylus(self, cmd, info):
        if cmd == "sym":
            tl = self.get_toplevel()
            w = tl.get_focus()
            if w == None:
                w = self
            ev = gtk.gdk.Event(gtk.gdk.KEY_PRESS)
            ev.window = w.window
            if info == '<BS>':
                ev.keyval = 65288
                ev.hardware_keycode = 22
            else:
                (ev.keyval,) = struct.unpack_from("b", info)
            w.emit('key_press_event', ev)
            #self.get_buffer().insert_at_cursor(info)
        if cmd == 'click':
            self.grab_focus()
            if not info.send_event:
                info.send_event = True
                ev2 = gtk.gdk.Event(gtk.gdk.BUTTON_PRESS)
                ev2.send_event = True
                ev2.window = info.window
                ev2.time = info.time
                ev2.x = info.x
                ev2.y = info.y
                ev2.button = info.button
                self.emit('button_press_event', ev2)
                self.emit('button_release_event', info)
        if cmd == 'redraw':
            self.queue_draw()

    def insert_at_cursor(self, text):
        self.get_buffer().insert_at_cursor(text)
        
class FingerEntry(gtk.Entry):
    def __init__(self):
        gtk.Entry.__init__(self)

    def insert_at_cursor(self, text):
        c = self.get_property('cursor-position')
        t = self.get_text()
        t = t[0:c]+text+t[c:]
        self.set_text(t)

class SMSlist(gtk.DrawingArea):
    def __init__(self, getlist):
        gtk.DrawingArea.__init__(self)
        self.pixbuf = None
        self.width = self.height = 0
        self.need_redraw = True
        self.colours = None
        self.collist = {}
        self.get_list = getlist

        self.connect("expose-event", self.redraw)
        self.connect("configure-event", self.reconfig)
        
        self.connect("button_release_event", self.release)
        self.connect("button_press_event", self.press)
        self.set_events(gtk.gdk.EXPOSURE_MASK
                        | gtk.gdk.BUTTON_PRESS_MASK
                        | gtk.gdk.BUTTON_RELEASE_MASK
                        | gtk.gdk.STRUCTURE_MASK)

        # choose a font
        fd = pango.FontDescription('sans 10')
        fd.set_absolute_size(25 * pango.SCALE)
        self.font = fd
        met = self.get_pango_context().get_metrics(fd)
        self.lineheight = (met.get_ascent() + met.get_descent()) / pango.SCALE
        fd = pango.FontDescription('sans 5')
        fd.set_absolute_size(15 * pango.SCALE)
        self.smallfont = fd
        self.selected = 0
        self.top = 0
        self.book = None

        self.smslist = []

        self.queue_draw()


    def set_book(self, book):
        self.book = book

    def lines(self):
        alloc = self.get_allocation()
        lines = alloc.height / self.lineheight
        return lines

    def reset_list(self):
        self.selected = 0
        self.smslist = None
        self.size_requested = 0
        self.refresh()

    def refresh(self):
        self.need_redraw = True
        self.queue_draw()

    def assign_colour(self, purpose, name):
        self.collist[purpose] = name

    def reconfig(self, w, ev):
        alloc = w.get_allocation()
        if not self.pixbuf:
            return
        if alloc.width != self.width or alloc.height != self.height:
            self.pixbuf = None
            self.need_redraw = True

    def add_col(self, sym, col):
        c = gtk.gdk.color_parse(col)
        gc = self.window.new_gc()
        gc.set_foreground(self.get_colormap().alloc_color(c))
        self.colours[sym] = gc

    def redraw(self, w, ev):
        if self.colours == None:
            self.colours = {}
            for p in self.collist:
                self.add_col(p, self.collist[p])
            self.bg = self.get_style().bg_gc[gtk.STATE_NORMAL]

        if self.need_redraw:
            self.draw_buf()

        self.window.draw_drawable(self.bg, self.pixbuf, 0, 0, 0, 0,
                                         self.width, self.height)

    def draw_buf(self):
        self.need_redraw = False
        if self.pixbuf == None:
            alloc = self.get_allocation()
            self.pixbuf = gtk.gdk.Pixmap(self.window, alloc.width, alloc.height)
            self.width = alloc.width
            self.height = alloc.height
        self.pixbuf.draw_rectangle(self.bg, True, 0, 0,
                                   self.width, self.height)

        if self.top > self.selected:
            self.top = 0
        max = self.lines()
        if self.smslist == None or \
               (self.top + max > len(self.smslist) and self.size_requested < self.top + max):
            self.size_requested = self.top + max
            self.smslist = self.get_list(self.top + max)
        for i in range(len(self.smslist)):
            if i < self.top:
                continue
            if i > self.top + max:
                break
            if i == self.selected:
                col = self.colours['bg-selected']
            else:
                col = self.colours['bg-%d'%(i%2)]

            self.pixbuf.draw_rectangle(col,
                                       True, 0, (i-self.top)*self.lineheight,
                                       self.width, self.lineheight)
            self.draw_sms(self.smslist[i], (i - self.top) * self.lineheight)


    def draw_sms(self, sms, yoff):
        
        self.modify_font(self.smallfont)
        tm = time.strftime("%Y-%m-%d %H:%M:%S", sms.time[0:6]+(0,0,0))
        then = time.mktime(sms.time[0:6]+(0,0,-1))
        now = time.time()
        if now > then:
            diff = now - then
            if diff < 99:
                delta = "%02d sec ago" % diff
            elif diff < 99*60:
                delta = "%02d min ago" % (diff/60)
            elif diff < 48*60*60:
                delta = "%02dh%02dm ago" % ((diff/60/60), (diff/60)%60)
            else:
                delta = tm[0:10]
            tm = delta + tm[10:]

        l = self.create_pango_layout(tm)
        self.pixbuf.draw_layout(self.colours['time'],
                                0, yoff, l)
        co = sms.correspondent
        if self.book:
            cor = book_name(self.book, co)
            if cor:
                co = cor[0]
        if sms.source == 'LOCAL':
            col = self.colours['recipient']
            co = 'To ' + co
        else:
            col = self.colours['sender']
            co = 'From '+co
        l = self.create_pango_layout(co)
        self.pixbuf.draw_layout(col,
                                0, yoff + self.lineheight/2, l)
        self.modify_font(self.font)
        t = sms.text.replace("\n", " ")
        t = t.replace("\n", " ")
        l = self.create_pango_layout(t)
        if sms.state in ['DRAFT', 'NEW']:
            col = self.colours['mesg-new']
        else:
            col = self.colours['mesg']
        self.pixbuf.draw_layout(col,
                                180, yoff, l)

    def press(self,w,ev):
        row = int(ev.y / self.lineheight)
        self.selected = self.top + row
        if self.selected >= len(self.smslist):
            self.selected = len(self.smslist) - 1
        if self.selected < 0:
            self.selected = 0

        l = self.lines()
        self.top += row - l / 2
        if self.top >= len(self.smslist) - l:
            self.top = len(self.smslist) - l + 1
        if self.top < 0:
            self.top = 0

        self.refresh()
        
    def release(self,w,ev):
        pass

def load_book(file):
    try:
        f = open(file)
    except:
        f = open('/home/neilb/home/mobile-numbers-jan-08')
    rv = []
    for l in f:
        x = l.split(';')
        rv.append([x[0],x[1]])
    rv.sort(lambda x,y: cmp(x[0],y[0]))
    return rv

def book_lookup(book, name, num):
    st=[]; mid=[]
    for l in book:
        if name.lower() == l[0][0:len(name)].lower():
            st.append(l)
        elif l[0].lower().find(name.lower()) >= 0:
            mid.append(l)
    st += mid
    if len(st) == 0:
        return [None, None]
    if num >= len(st):
        num = -1
    return st[num]

def book_parse(book, name):
    if not book:
        return None
    cnt = 0
    while len(name) and name[-1] == '.':
        cnt += 1
        name = name[0:-1]
    return book_lookup(book, name, cnt)
    


def book_name(book, num):
    if len(num) < 8:
        return None
    for ad in book:
        if len(ad[1]) >= 8 and num[-8:] == ad[1][-8:]:
            return ad
    return None

def book_speed(book, sym):
    i = book_lookup(book, sym, 0)
    if i[0] == None or i[0] != sym:
        return None
    j = book_lookup(book, i[1], 0)
    if j[0] == None:
        return (i[1], i[0])
    return (j[1], j[0])

def name_lookup(book, str):
    # We need to report
    #  - a number - to dial
    #  - optionally a name that is associated with that number
    #  - optionally a new name to save the number as
    # The name is normally alpha, but can be a single digit for
    # speed-dial
    # Dots following a name allow us to stop through multiple matches.
    # So input can be:
    # A single symbol.
    #         This is a speed dial.  It maps to name, then number
    # A string of >1 digits
    #         This is a literal number, we look up name if we can
    # A string of dots
    #         This is a look up against recent incoming calls
    #         We look up name in phone book
    # A string starting with alpha, possibly ending with dots
    #         This is a regular lookup in the phone book
    # A number followed by a string
    #         This provides the string as a new name for saving
    # A string of dots followed by a string
    #         This also provides the string as a newname
    # An alpha string, with dots, followed by '+'then a single symbol
    #         This saves the match as a speed dial
    #
    # We return a triple of (number,oldname,newname)
    if re.match('^[A-Za-z0-9]$', str):
        # Speed dial lookup
        s = book_speed(book, str)
        if s:
            return (s[0], s[1], None)
        return None
    m = re.match('^(\+?\d+)([A-Za-z][A-Za-z0-9 ]*)?$', str)
    if m:
        # Number and possible newname
        s = book_name(book, m.group(1))
        if s:
            return (m.group(1), s[0], m.group(2))
        else:
            return (m.group(1), None, m.group(2))
    m = re.match('^([A-Za-z][A-Za-z0-9 ]*)(\.*)(\+[A-Za-z0-9])?$', str)
    if m:
        # name and dots
        speed = None
        if m.group(3):
            speed = m.group(3)[1]
        i = book_lookup(book, m.group(1), len(m.group(2)))
        if i[0]:
            return (i[1], i[0], speed)
        return None

class SendSMS(gtk.Window):
    def __init__(self, store):
        gtk.Window.__init__(self)
        self.set_default_size(480,640)
        self.set_title("SendSMS")
        self.store = store
        self.connect('destroy', self.close_win)
        
        self.selecting = False
        self.viewing = False
        self.book = None
        self.create_ui()

        self.show()
        self.reload_book = True
        self.number = None
        self.cutbuffer = None

        d = dnotify.dir(store.dirname)
        self.watcher = d.watch('newmesg', lambda f : self.got_new())

        self.connect('property-notify-event', self.newprop)
        self.add_events(gtk.gdk.PROPERTY_CHANGE_MASK)
    def newprop(self, w, ev):
        if ev.atom == '_INPUT_TEXT':
            str = self.window.property_get('_INPUT_TEXT')
            self.numentry.set_text(str[2])

    def close_win(self, *a):
        # FIXME save draft
        gtk.main_quit()

    def create_ui(self):

        fd = pango.FontDescription("sans 10")
        fd.set_absolute_size(25*pango.SCALE)
        self.button_font = fd
        v = gtk.VBox() ;v.show() ; self.add(v)

        self.sender = self.send_ui()
        v.add(self.sender)
        self.sender.hide()

        self.listing = self.list_ui()
        v.add(self.listing)
        self.listing.show()

        self.book = load_book("/media/card/address-book")
        self.listview.set_book(self.book)

        self.rotate_list(self, target='All')

    def send_ui(self):
        v = gtk.VBox()
        main = v

        h = gtk.HBox()
        h.show()
        v.pack_start(h, expand=False)
        l = gtk.Label('To:')
        l.modify_font(self.button_font)
        l.show()
        h.pack_start(l, expand=False)
        
        self.numentry = FingerEntry()
        h.pack_start(self.numentry)
        self.numentry.modify_font(self.button_font)
        self.numentry.show()
        self.numentry.connect('changed', self.update_to);

        h = gtk.HBox()
        l = gtk.Label('')
        l.modify_font(self.button_font)
        l.show()
        h.pack_start(l)
        h.show()
        v.pack_start(h, expand=False)
        h = gtk.HBox()
        self.to_label = l
        l = gtk.Label('0 chars')
        l.modify_font(self.button_font)
        l.show()
        self.cnt_label = l
        h.pack_end(l)
        h.show()
        v.pack_start(h, expand=False)

        h = gtk.HBox()
        h.set_size_request(-1,80)
        h.set_homogeneous(True)
        h.show()
        v.pack_start(h, expand=False)

        self.add_button(h, 'select', self.select)
        self.add_button(h, 'clear', self.clear)
        self.add_button(h, 'paste', self.paste)

        self.message = FingerText()
        self.message.show()
        self.message.modify_font(self.button_font)
        sw = gtk.ScrolledWindow() ; sw.show()
        sw.set_policy(gtk.POLICY_NEVER, gtk.POLICY_AUTOMATIC)
        #v.add(self.message)
        v.add(sw)
        sw.add(self.message)
        self.message.get_buffer().connect('changed', self.buff_changed)

        h = gtk.HBox()
        h.set_size_request(-1,80)
        h.set_homogeneous(True)
        h.show()
        v.pack_end(h, expand=False)

        self.add_button(h, 'Send GSM', self.send, 'GSM')
        self.draft_button = self.add_button(h, 'Draft', self.draft)
        self.add_button(h, 'Send EXE', self.send, 'EXE')

        return main

    def list_ui(self):
        v = gtk.VBox() ; main = v

        h = gtk.HBox() ; h.show()
        h.set_size_request(-1,80)
        h.set_homogeneous(True)
        v.pack_start(h, expand = False)
        self.add_button(h, 'Del', self.delete)
        self.view_button = self.add_button(h, 'View', self.view)
        self.reply = self.add_button(h, 'New', self.open)

        h = gtk.HBox() ; h.show()
        h.set_size_request(-1,80)
        h.set_homogeneous(True)
        v.pack_end(h, expand=False)
        self.buttonA = self.add_button(h, 'Sent', self.rotate_list, 'A')
        self.buttonB = self.add_button(h, 'Recv', self.rotate_list, 'B')


        self.last_response = gtk.Label('')
        v.pack_end(self.last_response, expand = False)

        h = gtk.HBox() ; h.show()
        v.pack_start(h, expand=False)
        b = gtk.Button("clr") ; b.show()
        b.connect('clicked', self.clear_search)
        h.pack_end(b, expand=False)
        l = gtk.Label('search:') ; l.show()
        h.pack_start(l, expand=False)
        
        e = gtk.Entry() ; e.show()
        self.search_entry = e
        h.pack_start(e)

        self.listview = SMSlist(self.load_list)
        self.listview.show()
        self.listview.assign_colour('time', 'blue')
        self.listview.assign_colour('sender', 'red')
        self.listview.assign_colour('recipient', 'black')
        self.listview.assign_colour('mesg', 'black')
        self.listview.assign_colour('mesg-new', 'red')
        self.listview.assign_colour('bg-0', 'yellow')
        self.listview.assign_colour('bg-1', 'pink')
        self.listview.assign_colour('bg-selected', 'white')
        
        v.add(self.listview)

        self.singleview = gtk.TextView()
        self.singleview.modify_font(self.button_font)
        self.singleview.show()
        self.singleview.set_wrap_mode(gtk.WRAP_WORD_CHAR)
        sw = gtk.ScrolledWindow()
        sw.add(self.singleview)
        sw.set_policy(gtk.POLICY_NEVER, gtk.POLICY_AUTOMATIC)
        sw.hide()
        v.add(sw)
        self.singlescroll = sw
        

        main.show()
        return main

    def add_button(self, parent, label, func, *args):
        b = gtk.Button(label)
        b.child.modify_font(self.button_font)
        b.connect('clicked', func, *args)
        b.set_property('can-focus', False)
        parent.add(b)
        b.show()
        return b

    def update_to(self, w):
        n = w.get_text()
        if n == '':
            self.reload_book = True
            self.to_label.set_text('')
        else:
            if self.reload_book:
                self.reload_book = False
                self.book = load_book("/media/card/address-book")
                self.listview.set_book(self.book)
            e = name_lookup(self.book, n)
            if e and e[1]:
                self.to_label.set_text(e[1] + 
                                       ' '+
                                       e[0])
                self.number = e[0]
            else:
                self.to_label.set_text('??')
                self.number = n
        self.buff_changed(None)

    def buff_changed(self, w):
        if self.numentry.get_text() == '' and self.message.get_buffer().get_property('text') == '':
            self.draft_button.child.set_text('Cancel')
        else:
            self.draft_button.child.set_text('SaveDraft')
        l = len(self.message.get_buffer().get_property('text'))
        if l <= 160:
            m = 1
        else:
            m = (l+152)/153
        self.cnt_label.set_text('%d chars / %d msgs' % (l, m))

    def select(self, w, *a):
        if not self.selecting:
            self.message.input.active = False
            w.child.set_text('Cut')
            self.selecting = True
        else:
            self.message.input.active = True
            w.child.set_text('Select')
            self.selecting = False
            b = self.message.get_buffer()
            bound = b.get_selection_bounds()
            if bound:
                (s,e) = bound
                t = b.get_text(s,e)
                self.cutbuffer = t
                b.delete_selection(True, True)

    def clear(self, *a):
        w = self.get_toplevel().get_focus()
        if w == None:
            w = self.message
        if w == self.message:
            self.cutbuffer = self.message.get_buffer().get_property('text')
            b = self.message.get_buffer()
            b.set_text('')
        else:
            self.cutbuffer = w.get_text()
            w.set_text('')

            
    def paste(self, *a):
        w = self.get_toplevel().get_focus()
        if w == None:
            w = self.message
        if self.cutbuffer:
            w.insert_at_cursor(self.cutbuffer)
        pass
    def send(self, w, style):
        sender = '0403463349'
        recipient = self.number
        mesg = self.message.get_buffer().get_property('text')
        if not mesg or not recipient:
            return
        try:
            if style == 'EXE':
                p = Popen(['exesms', sender, recipient, mesg], stdout = PIPE)
            else:
                p = Popen(['gsm-sms', sender, recipient, mesg], stdout = PIPE)
        except:
            rv = 1
            line = 'Fork Failed'
        else:
            line = 'no response'
            rv = p.wait()
            for l in p.stdout:
                if l:
                    line = l
        
        s = SMSmesg(to = recipient, text = mesg)

        if rv or line[0:2] != 'OK':
            s.state = 'DRAFT'
            target = 'Draft'
        else:
            target = 'All'
        self.store.store(s)
        self.last_response.set_text('Mess Send: '+ line.strip())
        self.last_response.show()

        self.sender.hide()
        self.listing.show()
        self.rotate_list(target=target)

    def draft(self, *a):
        sender = '0403463349'
        recipient = self.numentry.get_text()
        if recipient:
            rl = [recipient]
        else:
            rl = []
        mesg = self.message.get_buffer().get_property('text')
        if mesg:
            s = SMSmesg(to = recipient, text = mesg, state = 'DRAFT')
            self.store.store(s)
        self.sender.hide()
        self.listing.show()
        self.rotate_list(target='Draft')
    def config(self, *a):
        pass
    def delete(self, *a):
        if len(self.listview.smslist ) < 1:
            return
        s = self.listview.smslist[self.listview.selected]
        self.store.delete(s)
        sel = self.listview.selected
        self.rotate_list(target=self.display_list)
        self.listview.selected = sel
        if self.viewing:
            self.view(self.view_button)

    def view(self, w, *a):
        if self.viewing:
            w.child.set_text('View')
            self.viewing = False
            self.singlescroll.hide()
            self.listview.show()
            if self.listview.smslist and  len(self.listview.smslist ) >= 1:
                s = self.listview.smslist[self.listview.selected]
                if s.state == 'NEW':
                    self.store.setstate(s, None)
                    if self.display_list == 'New':
                        self.rotate_list(target='New')
            self.reply.child.set_text('New')
        else:
            if not self.listview.smslist or len(self.listview.smslist ) < 1:
                return
            s = self.listview.smslist[self.listview.selected]
            w.child.set_text('List')
            self.viewing = True
            self.last_response.hide()
            self.listview.hide()
            if self.book:
                n = book_name(self.book, s.correspondent)
                if n and n[0]:
                    n = n[0] + ' ['+s.correspondent+']'
                else:
                    n = s.correspondent
            else:
                n = s.correspondent
            if s.source == 'LOCAL':
                t = 'To: ' + n + '\n'
            else:
                t = 'From: %s (%s)\n' % (n, s.source)
            tm = time.strftime('%d%b%Y %H:%M:%S', s.time[0:6]+(0,0,0))
            t += 'Time: ' + tm + '\n'
            t += '\n'
            t += s.text
            self.singleview.get_buffer().set_text(t)
            self.singlescroll.show()

            if s.source == 'LOCAL':
                self.reply.child.set_text('Open')
            else:
                self.reply.child.set_text('Reply')
            
    def open(self, *a):
        if self.viewing:
            if len(self.listview.smslist) < 1:
                return
            s = self.listview.smslist[self.listview.selected]
            if s.state == 'NEW':
                self.store.setstate(s, None)
        
            self.numentry.set_text(s.correspondent)
            self.message.get_buffer().set_text(s.text)
            self.draft_button.child.set_text('SaveDraft')
        else:
            self.numentry.set_text('')
            self.message.get_buffer().set_text('')
            self.draft_button.child.set_text('Cancel')
        self.listing.hide()
        self.sender.show()

    def load_list(self, lines):
        now = time.time()
        l = []
        target = self.display_list
        patn = self.search_entry.get_text()
        #print 'pattern is', patn
        if target == 'New':
            (now, l) = self.store.lookup(now, 'NEW')
        elif target == 'Draft':
            (now, l) = self.store.lookup(now, 'DRAFT')
        else:
            if lines == 0: lines = 20
            while now and len(l) < lines:
                (now, l2) = self.store.lookup(now)
                for e in l2:
                    if patn and patn not in e.correspondent:
                        continue
                    if target == 'All':
                        l.append(e)
                    elif target == 'Sent' and e.source == 'LOCAL':
                        l.append(e)
                    elif target == 'Recv' and e.source != 'LOCAL':
                        l.append(e)
        return l
        
    def rotate_list(self, w=None, ev=None, which = None, target=None):
        # lists are:
        #   All, Recv, New, Sent, Draft
        # When one is current, two others can be selected

        if target == None:
            if w == None:
                target = self.display_list
            else:
                target = w.child.get_text()

        if target == 'All':
            self.buttonA.child.set_text('Sent')
            self.buttonB.child.set_text('Recv')
        if target == 'Sent':
            self.buttonA.child.set_text('All')
            self.buttonB.child.set_text('Draft')
        if target == 'Draft':
            self.buttonA.child.set_text('All')
            self.buttonB.child.set_text('Sent')
        if target == 'Recv':
            self.buttonA.child.set_text('All')
            self.buttonB.child.set_text('New')
        if target == 'New':
            self.buttonA.child.set_text('All')
            self.buttonB.child.set_text('Recv')
                
        self.display_list = target
        self.listview.reset_list()

    def clear_search(self, *a):
        pass

    def got_new(self):
        self.rotate_list(self, target = 'New')

def main(args):
    for p in ['/media/card','/media/disk','/var/tmp']:
        if os.path.exists(p):
            pth = p
            break
    w = SendSMS(SMSstore(pth+'/SMS'))
    gtk.settings_get_default().set_long_property("gtk-cursor-blink", 0, "main")

    gtk.main()

if __name__ == '__main__':
    main(sys.argv)
