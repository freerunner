
#
# Contacts are stored in a file, one per line, with ':' separated
# fields.  If a field can have a list, it is comma separated.
# entries in a list can have a 'tag=' prefix.
# We can have references to other entries, so each entry has an ID
# Fields are:
#   id
#   Family Name
#   Given Name
#   groups     - list
#   references - list
#   PO Box
#   Address
#   Address extension
#   Suburb/town
#   postcode
#   County
#   phone numbers - list
#   modify date

