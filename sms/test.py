
import os, time, sys
from storesms import SMSstore, SMSmesg


try:
    os.mkdir("/tmp/sms")
except:
    pass
st = SMSstore("/tmp/sms")

if len(sys.argv) == 2 and sys.argv[1][0] == '-':
    (next, l) = st.lookup(time.time(), sys.argv[1][1:])
    print next
    for m in l:
        print m.format()

elif len(sys.argv) > 1:
    m = SMSmesg(time.time(),
                "0403463349",
                ["0415836820"],
                sys.argv[1]
                )
    if len(sys.argv) > 2:
        st.store(m, sys.argv[2])
    else:
        st.store(m)
else:
    (next, l) = st.lookup(time.time())
    print next
    for m in l:
        print m.format()

