#!/usr/bin/env python

#TODO
# aux button
# calculator
# sms
#   entry shows number of new messages
#   embed shows latest new message if there is one
#   buttons allow 'read' or 'open'
# phone calls
#   number of missed calls, or incoming number
#   embed shows call log, or tap board
#   buttons allow 'call' or 'answer' or 'open' ....
# embed calendar
#   selected by 'date'
#   tapping can select a date
# run windowed program
# monitor list of windows
# address list
#   this is a 'group' where 'tasks' are contacts


# Launcher, version 2.
# This module just define the UI and plugin interface
# All tasks etc go in loaded modules.
#
# The elements of the UI are:
#
#   ----------------------------------
#   |  Text Entry box (one line)     |
#   +--------------------------------+
#   |                |               |
#   |    group       |      task     |
#   |   selection    |     selection |
#   |    list        |      list     |
#   |                |               |
#   |                |               |
#   +----------------+               |
#   |   optional     |               |
#   |   task-        |               |
#   |     specific   |               |
#   |   widget       +---------------|
#   |                | secondary     |
#   |                | button row    |
#   +----------------+---------------+
#   |         Main Button Row        |
#   +--------------------------------+
#
# If the optional widget is present, then the Main Button Row
# disappears and the secondary button row is used instead.
#
# The selection lists auto-scroll when an item near top or bottom
# is selected.  Selecting an entry only updates other parts of
# the UI and does not perform any significant action.
# Actions are performed by buttons which appear in a button
# row.
# The optional widget can be anything provided by the group
# or task, for example:
#  - tap-board for entering text
#  - calendar for selecting a date
#  - display area for small messages (e.g. SMS)
#  - preview during file selection
#  - map of current location
#
# Entered text alway appears in the Text Entry box and
# is made available to the current group and task
# That object may use the text and may choose to delete it.
# e.g.
#   A calculator task might display an evaluation of the text
#   A call task might offer to call the displayed number, and
#     delete it when the call completes
#   An address-book group might restrict displayed tasks
#     to those which match the text
#   A 'todo' task might set the text to match the task content,
#     then update the content as the text changes.

import sys, gtk, os, pango

if __name__ == '__main__':
    sys.path.insert(1, '/home/neilb/home/freerunner/lib')
    sys.path.insert(1, '/root/lib')

from tapboard import TapBoard
import grouptypes, listselect, scrawl
from grouptypes import *


class LaunchWin(gtk.Window):
    def __init__(self):
        gtk.Window.__init__(self, gtk.WINDOW_TOPLEVEL)
        self.set_default_size(480, 640)
        self.connect('destroy', lambda w: gtk.main_quit())

        self.embeded_widget = None
        self.widgets = {}
        self.buttons = []
        self.create_ui()
        self.load_config()

    def create_ui(self):
        # Create the UI framework, first the components

        # The Entry
        e1 = gtk.Entry()
        e1.set_alignment(0.5) ; # Center text
        e1.connect('changed', self.entry_changed)
        e1.connect('backspace', self.entry_changed)
        e1.show()
        self.entry = e1

        # The group list
        l1 = listselect.ListSelect(center = False)
        l1.connect('selected', self.group_select)
        # set appearance here
        l1.set_colour('group','blue')
        self.grouplist = l1
        l1.set_zoom(40)
        l1.show()

        # The task list
        l2 = listselect.ListSelect(center = True)
        l2.connect('selected', self.task_select)
        l2.set_colour('cmd', 'black')
        l2.set_colour('win', 'blue')
        l2.set_zoom(35)
        # set appearance
        self.tasklist = l2
        l2.show()

        # The embedded widget: provide a VBox as a place holder
        v1 = gtk.VBox()
        self.embed_box = v1

        # The Main button box - buttons are added later
        h1 = gtk.HBox(True)
        self.main_buttons = h1
        # HACK
        h1.set_size_request(-1, 80)
        h1.show()

        # The Secondary button box
        h2 = gtk.HBox(True)
        h2.set_size_request(-1, 60)
        self.secondary_buttons = h2

        # Now make the two columns

        v2 = gtk.VBox(True)
        v2.pack_start(self.grouplist, expand = True)
        v2.pack_end(self.embed_box, expand = False)
        v2.show()

        v3 = gtk.VBox()
        v3.pack_start(self.tasklist, expand = True)
        v3.pack_end(self.secondary_buttons, expand = False)
        v3.show()

        # and bind them together
        h3 = gtk.HBox(True)
        h3.pack_start(v2, expand=True)
        h3.pack_end(v3, expand=True)
        h3.show()

        # And now one big vbox to hold it all
        v4 = gtk.VBox()
        v4.pack_start(e1, expand=False)
        v4.pack_end(self.main_buttons, expand=False)
        v4.pack_end(h3, expand=True)
        v4.show()
        self.add(v4)
        self.show()

        ## We want writing recognistion to work
        ## over the whole middle section.  Only that
        ## turns out to be too hard for my lowly gtk
        ## skills.  So we do recognition separately
        ## on each selection box only.
        s1 = scrawl.Scrawl(l1, self.getsym, lambda p: l1.tap(p[0],p[1]))
        s2 = scrawl.Scrawl(l2, self.getsym, lambda p: l2.tap(p[0],p[1]))
        s1.set_colour('red')
        s2.set_colour('blue')


        ctx = self.get_pango_context()
        fd = ctx.get_font_description()
        fd.set_absolute_size(30 * pango.SCALE)
        self.button_font = fd;
        self.entry.modify_font(fd)

    def load_config(self):
        fname = os.path.join(os.environ['HOME'], ".launch2rc")
        types = {}
        types['ignore'] = IgnoreType
        types['list' ] = ListType
        groups = []
        f = open(fname)
        gobj = None
        for line in f:
            l = line.strip()
            if not l:
                continue
            if l[0] == '[':
                l = l.strip('[]')
                f = l.split('/', 1)
                group = f[0]
                if len(f) > 1:
                    group_type = f[1]
                else:
                    group_type = "list"
                if group_type in types:
                    gobj = types[group_type](group)
                else:
                    gobj = types['ignore'](group)
                groups.append(gobj)
            elif gobj != None:
                gobj.parse(l)
        self.grouplist.list = groups
        self.grouplist.list_changed()


    def entry_changed(self, entry):
        print "fixme", entry.get_text()

    def group_select(self, list, item):
        print "select group", item
        g = list.list[item]
        self.tasklist.list = g.tasklist()
        self.tasklist.list_changed()
        if self.tasklist.list != None:
            self.task_select(self.tasklist,
                             self.tasklist.selected)

    def task_select(self, list, item):
        if item == None:
            self.set_buttons(None)
            self.set_embed(None)
        else:
            task = self.tasklist.list[item]
            task.callback = self.update
            self.set_buttons(task.buttons())
            self.set_embed(task.embedded())

    def update(self, task):
        if self.tasklist.selected != None and \
           self.tasklist.list[self.tasklist.selected] == task:
            self.set_buttons(task.buttons())
            self.set_embed(task.embedded())
    
    def set_buttons(self, list):
        if not list:
            # hide the button boxes
            self.secondary_buttons.hide()
            self.main_buttons.hide()
            self.buttons = []
            return
        if self.same_buttons(list):
            return

        self.buttons = list
        self.update_buttons(self.main_buttons)
        self.update_buttons(self.secondary_buttons)
        if self.embeded_widget:
            self.main_buttons.hide()
            self.secondary_buttons.show()
        else:
            self.secondary_buttons.hide()
            self.main_buttons.show()

    def same_buttons(self, list):
        if len(list) != len(self.buttons):
            return False
        for i in range(len(list)):
            if list[i] != self.buttons[i]:
                return False
        return True

    def update_buttons(self, box):
        # make sure there are enough buttons
        have = len(box.get_children())
        need = len(self.buttons) - have
        if need > 0:
            for i in range(need):
                b = gtk.Button("?")
                b.child.modify_font(self.button_font)
                b.set_property('can-focus', False)
                box.add(b)
                b.connect('clicked', self.button_pressed, have + i)
            have += need
        b = box.get_children()
        # hide extra buttons
        if need < 0:
            for i in range(-need):
                b[have-i-1].hide()
        # update each button
        print have, need, self.buttons
        for i in range(len(self.buttons)):
            print i, 'is', self.buttons[i]
            b[i].child.set_text(self.buttons[i])
            b[i].show()

    def button_pressed(self, widget, num):
        self.tasklist.list[self.tasklist.selected].press(num)
        self.task_select(self.tasklist,
                         self.tasklist.selected)

    def set_embed(self, widget):
        if type(widget) == str:
            widget = self.make_widget(widget)
            
        if widget == self.embeded_widget:
            return
        if self.embeded_widget:
            self.embeded_widget.hide()
            self.embed_box.remove(self.embeded_widget)
            self.embeded_widget = None
        if widget:
            self.embed_box.add(widget)
            self.embeded_widget = widget
            widget.show()
            self.main_buttons.hide()
            self.embed_box.show()
            if self.buttons:
                self.secondary_buttons.show()
        else:
            self.embed_box.hide()
            self.secondary_buttons.hide()
            if self.buttons:
                self.main_buttons.show()

    def make_widget(self, name):
        if name in self.widgets:
            return self.widgets[name]
        if name == "tapboard":
            w = TapBoard()
            def key(w, k):
                if k == '\b':
                    self.entry.emit('backspace')
                elif k == 'Return':
                    self.entry.emit('activate')
                elif len(k) == 1:
                    self.entry.emit('insert-at-cursor', k)
            w.connect('key', key)
            self.widgets[name] = w
            return w
        return None

    def getsym(self, sym):
        print "gotsym", sym
        if sym == '<BS>':
            self.entry.emit('backspace')
        elif sym == '<newline>':
            self.entry.emit('activate')
        elif len(sym) == 1:
            self.entry.emit('insert-at-cursor', sym)

if __name__ == '__main__':
    sys.path.insert(1, '/home/neilb/home/freerunner/lib')
    sys.path.insert(1, '/root/lib')
    l = LaunchWin()
    gtk.main()
