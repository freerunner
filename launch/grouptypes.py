#
# Generic code for group types
#
# A 'group' must provide:
#
# - 'parse' to take a line from the config file and interpret it.
# - [0] and [1] which are a  'name' and 'type' usable by ListSelect
#    the type will normally be 'group'.
# - 'tasklist' which provides a task list to display in the other
#    ListSelect.
# - 'embedded' which optionally provides a widget to display in
#    the 'embedded widget' position.  This widget may also be
#    displayed full-screen
#
# A 'group' can generate the signal:
# - 'new-task-list' to report that the task list has changed
# ???
#

import cmd, re

class TaskGroup:
    def __init__(self, name):
        self.name = name
        self.type = 'group'

    def __getitem__(self, key):
        if key == 0:
            return self.name
        if key == 1:
            return self.type

    def __len__(self):
        return 1

    def __iter__(self):
        return (self.__getitem__(x) for x in range(2))

    def embedded(self):
        return None

    def buttons(self):
        return []

class IgnoreType(TaskGroup):
    def __init__(self, name):
        TaskGroup.__init__(self, name)

    def parse(self, line):
        pass

    def tasklist(self):
        return []

class ListType(TaskGroup):
    """A ListType group parses a list of tasks out of
    the config file and provides them as a static list.
    Tasks can be:
      !command   - run command and capture text output
      (window)command - run command and expect it to appear as 'window'
      command.command - run an internal command from the given module
    In each case, arguments can follow and are treated as you would expect.
    """

    def __init__(self, name):
        TaskGroup.__init__(self,name)
        self.tasks = []

    def parse(self, line):
        line = line.strip()
        m = re.match('^([A-Za-z0-9_ ]+)=(.*)', line)
        if m:
            name = m.groups()[0].strip()
            line = m.groups()[1].strip()
        else:
            name = None
        if line[0] == '!':
            self.parse_txt(name, line)
        elif line[0] == '(':
            self.parse_win(name, line)
        else:
            self.parse_internal(name, line)

    def tasklist(self):
        return self.tasks

    def parse_txt(self, name, line):
        if name == None:
            name = line[1:]
        task = cmd.ShellTask(name, line)
        if task.name:
            self.tasks.append(task)

    def parse_win(self, name, line):
        f = line[1:].split(')', 1)
        if len(f) != 2:
            return
        if name == None:
            name = f[0]
        task = cmd.WinTask(name, f[0], f[1])
        if task:
            self.tasks.append(task)

    def parse_internal(self, name, line):
        # split in to words, honouring quotes
        words = map(lambda a: a[0].strip('"')+a[1].strip("'")+a[2],
                    re.findall('("[^"]*")?(\'[^\']*\')?([^ "\'][^ "\']*)? *',
                               line))[:-1]
        
        cmd = words[0].split('.', 1)
        if len(cmd) != 2:
            return

        exec "import " + cmd[0]
        fn = eval(words[0])
        if name == None:
            name = words[0]
        task = fn(name, *words[1:])
        if task:
            self.tasks.append(task)
