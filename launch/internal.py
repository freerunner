#
# internal commands for 'launch'
# some of these affect launch directly, some are just
# ad hoc simple things.

import time as _time
import gobject
import dnotify, os

class Task:
    def __init__(self):
        self.callback = None

    def __getitem__(self, ind):
        if ind == 0:
            self.update()
            return self.name
        if ind == 1:
            return self.type

    def __len__(self):
        return 2

    def __iter__(self):
        return (self.__getitem__(x) for x in range(2))

    def buttons(self):
        return []
    def embedded(self):
        return None
    
    def update(self):
        pass

class date(Task):
    def __init__(self, name):
        Task.__init__(self)
        self.name = 'date'
        self.type = 'cmd'
        self.update()

    def update(self):
        self.name = _time.strftime('%d-%b-%Y',
                                   _time.localtime(_time.time()))

    def on_change(self, obj, ind):
        now = _time.time()
        next_hour = int(now/60/60)+1
        gobject.timeout_add(int (((next_hour*3600) - now) * 1000),
                            lambda : obj.item_changed(ind, self))


class time(Task):
    def __init__(self, name):
        Task.__init__(self)
        self.name = 'time'
        self.type = 'cmd'
        self.update()

    def update(self):
        self.name = _time.strftime('%H:%M',
                                   _time.localtime(_time.time()))

    def on_change(self, obj, ind):
        now = _time.time()
        next_min = int(now/60)+1
        gobject.timeout_add(int (((next_min*60) - now) * 1000),
                            lambda : obj.item_changed(ind, self))


class file(Task):
    def __init__(self, name, path):
        Task.__init__(self)
        self.path = path
        self.name = 'file ' + path
        self.type = 'cmd'
        try:
            self.watch = dnotify.dir(os.path.dirname(path))
        except OSError:
            self.watch = None
        self.fwatch = None
        self.update()

    def update(self):
        try:
            f = open(self.path)
            l = f.readline().strip()
            f.close()
        except:
            l = "-"

        self.name = l
        if self.fwatch:
            self.fwatch.cancel()
            self.fwatch = None

    def on_change(self, obj, ind):
        if self.watch == None:
            try:
                self.watch = dnotify.dir(os.path.dirname(name))
            except OSError:
                self.watch = None
        if self.watch == None:
            return
        if self.fwatch == None:
            self.fwatch = self.watch.watch(os.path.basename(self.path),
                                           lambda f: obj.item_changed(ind, self))
