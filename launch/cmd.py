#
# Support for running commands from the Laucher
#
# Once a command is run, we watch for it to finish
# and update status when it does.
# We also maintain window list and for commands that appear
# in windows, we associate the window with the command.
# ShellTask() runs a command and captures output in a text buffer
#  that can be displayed in a FingerScroll
# WinTask() runs a command in a window

import os,fcntl, gobject
from internal import Task
from subprocess import Popen, PIPE
from FingerScroll import FingerScroll

class ShellTask(Task):
    # Normally there is one button : "Run"
    # When this is pressed we create a 'FingerScroll' text buffer
    # to hold the output.
    # The button then becomes 'ReRun'
    # When we get deselected, the buffer gets hidden and we get
    # new button 'Display'
    def __init__(self, name, line):
        Task.__init__(self)
        self.name = name
        self.type = "cmd"
        self.append = False
        self.cmd = line[1:]
        if line[1] == '+':
            self.append = True
            self.cmd = line[2:]
        self.buffer = None
        self.displayed = False
        self.job = None

    def buttons(self):
        if self.displayed:
            if self.job:
                return ['-','Kill']
            else:
                return ['ReRun', '-']
        if self.buffer != None:
            if self.job:
                return ['-', 'Display']
            else:
                return ['Run', 'Display']
        return ['Run']

    def embedded(self):
        if self.displayed:
            return self.buffer
        return None

    def press(self, num):
        if num == 1:
            if self.displayed and self.job:
                # must be a 'kill' request'
                os.kill(self.job.pid, 15)
                return
            # Display the buffer
            self.displayed = True
            self.callback(self)
            return

        if self.job:
            return

        if self.buffer == None:
            self.buffer = FingerScroll()
            self.buffer.show()
            self.buffer.connect('hide', self.unmap_buff)
        self.buff = self.buffer.get_buffer()
        if not self.append:
            self.buff.delete(self.buff.get_start_iter(), self.buff.get_end_iter())
        # run the program
        self.job = Popen(self.cmd, shell=True, close_fds = True,
                         stdout=PIPE, stderr=PIPE)

        def set_noblock(f):
            flg = fcntl.fcntl(f, fcntl.F_GETFL, 0)
            fcntl.fcntl(f, fcntl.F_SETFL, flg | os.O_NONBLOCK)
        set_noblock(self.job.stdout)
        set_noblock(self.job.stderr)
        self.wc = gobject.child_watch_add(self.job.pid, self.done)
        self.wout = gobject.io_add_watch(self.job.stdout, gobject.IO_IN, self.read)
        self.werr = gobject.io_add_watch(self.job.stderr, gobject.IO_IN, self.read)

        self.displayed = True
        
    def read(self, f, dir):
        l = f.read()
        self.buff.insert(self.buff.get_end_iter(), l)
        gobject.idle_add(self.adjust)
        if l == "":
            return False
        return True

    def adjust(self):
        adj = self.buffer.vadj
        adj.set_value(adj.upper - adj.page_size)

    def done(self, *a):
        self.read(self.job.stdout, None)
        self.read(self.job.stderr, None)
        gobject.source_remove(self.wout)
        gobject.source_remove(self.werr)
        self.job.stdout.close()
        self.job.stderr.close()
        self.job = None
        self.callback(self)

    def unmap_buff(self, widget):
        if self.job == None:
            self.displayed = False


class WinTask(Task):
    def __init__(self, name, window, line):
        Task.__init__(self)
        self.name = name
        self.type = "win"

    def buttons(self):
        return ['run']

    def press(self, num):
        print "Thankyou for pressing", num 

    def embedded(self):
        return 'tapboard'
