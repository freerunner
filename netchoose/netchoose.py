#!/usr/bin/env python

import pygtk
import gtk
import os

import gobject
import dbus
import sys
import time
import mdbus

class NetChooser:
    def __init__(self):
        window = gtk.Window(gtk.WINDOW_TOPLEVEL)
        window.connect("destroy", self.close_application)
        window.set_title("NetChooser")

        # vertical stack of stuff.
        vb = gtk.VBox()
        window.add(vb)
        vb.show()

        ## GPRS: row of bits
        hb = gtk.HBox()
        vb.add(hb)
        hb.show()

        ### GPRS toggle button
        tb = gtk.ToggleButton("GPRS")
        tb.connect("toggled", self.gprs_set)
        tb.show()
        ### AP name entry
        ap = gtk.Entry(16)
        ap.show()

        hb.add(tb)
        hb.add(ap)

        ## WLAN: row of stuff
        hb = gtk.HBox()
        vb.add(hb)
        hb.show()

        ### WLAN toggle button
        tb = gtk.ToggleButton("WLAN")
        tb.connect("toggled", self.wlan_set)
        tb.show()
        ### Acces point dropdown
        ap = gtk.combo_box_entry_new_text()
        ap.child.connect("changed", self.wlan_ap)
        ap.show()

        hb.add(tb)
        hb.add(ap)

        window.show()

    def close_application(self, widget):
        gtk.main_quit()
        

    def gprs_set(self, widget):
        if widget.get_active():
            # start GPRS
            c = Commands(dbus.SystemBus)
            c.callMethod("org.freesmartphone.frameworkd",
                         "/org/freesmartphone/GSM/Device",
                         "org.freesmartphone.GSM.PDP.ActivateContext",
                         [ "vfinternet.au", "x", "x"])
        else:
            # stop GPRS
            c = Commands(dbus.SystemBus)
            c.callMethod("org.freesmartphone.frameworkd",
                         "/org/freesmartphone/GSM/Device",
                         "org.freesmartphone.GSM.PDP.DeactivateContext")
        return
    def wlan_set(self, widget):
        return
    def wlan_ap(self, widget):
        return
        
def main():
    gtk.main()
    return 0
if __name__ == "__main__":
    NetChooser()
    main()
    
