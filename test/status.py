
import sys
import pygtk
import gtk
import os
import gobject

capfile = "/sys/class/power_supply/battery/capacity"
curlimfile = "/sys/class/i2c-adapter/i2c-0/0-0073/pcf50633-mbc/usb_curlim"
chgfile = "/sys/class/i2c-adapter/i2c-0/0-0073/pcf50633-mbc/chgmode"
currfile = "/sys/class/power_supply/battery/current_now"
filename = "/media/card/panel-plugin/pixmaps/battery_%03d.png"
filename_charging = "/media/card/panel-plugin/pixmaps/battery_%03d_charging_%d.png"

def file_text(name):
    try:
        f = open(name)
    except:
        return ""
    t = f.read()
    return t.strip()
def file_num(name):
    try:
        i = int(file_text(name))
    except:
        i = 0
    return i

def setfile(icon):
    cap = file_num(capfile)
    capr = int((cap+5)/10)*10
    curr = file_num(currfile)
    lim = file_num(curlimfile)
    if curr >= 0 or lim == 0:
        f = filename % capr
    else:
        f = filename_charging % (capr, lim)
    print f
    i.set_from_file(f)
    
def update():
    global i
    setfile(i)
    to = gobject.timeout_add(30*1000, update)

i = gtk.StatusIcon()
setfile(i)
i.set_visible(True)
to = gobject.timeout_add(30*1000, update)

gtk.main()
