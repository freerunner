#include <unistd.h>
#include <stdlib.h>
#include <sys/fcntl.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <memory.h>
#include <linux/if.h>
#include <linux/if_tun.h>
#include <stdio.h>

main(int argc, char *argv[])
{
	int fd;
	struct ifreq ifr;
	int err;
	char *dev;
	char cmdbuf[1000];
	char pbuf[1500];
	int n;

	fd = open("/dev/net/tun", O_RDWR);
	if (fd < 0) {
		perror("tun");
		exit(1);
	}
 
	memset(&ifr, 0, sizeof(ifr));

	ifr.ifr_flags = IFF_TUN;
	err = ioctl(fd, TUNSETIFF, &ifr);
	if (err < 0) {
		perror("TUNSETIFF");
		exit(1);
	}

	dev = ifr.ifr_name;

	printf("dev = %s\n", dev);

	sprintf(cmdbuf, "ifconfig %s 10.255.255.254 pointopoint 10.255.255.253", dev);
	system(cmdbuf);

	sprintf(cmdbuf, "route add -net 0.0.0.0/1 gw 10.255.255.254 dev %s", dev);
	system(cmdbuf);
	sprintf(cmdbuf, "route add -net 128.0.0.0/1 gw 10.255.255.254 dev %s", dev);
	system(cmdbuf);

	n = read(fd, pbuf, sizeof(pbuf));
	printf("read got %d\n", n);

	system(cmdbuf);

	system("ifconfig wlan0 192.168.1.70");
	system(" iptables -A POSTROUTING -t nat -j MASQUERADE -s 10.255.255.254");
	write(fd, pbuf, n);
	while (1) {
	n = read(fd, pbuf, sizeof(pbuf));
	printf("read got %d\n", n);
	}
}
