
#include <fakekey/fakekey.h>

#include <X11/Xlib.h>

main(int argc, char *argv[])
{
	Display *d;
	FakeKey *f;
	int a;
	char *c;

	d = XOpenDisplay(NULL);

	f = fakekey_init(d);

	for (a=1; a<argc; a++) {
		for (c=argv[a]; *c; c++) {
			if (*c == '#')
				fakekey_press_keysym(f, XK_BackSpace, 0);
			else
				fakekey_press(f, c, 1, 0);
			fakekey_release(f);
		}
		if (a+1 < argc)
			fakekey_press(f, " ", 1, 0);
		else
			fakekey_press_keysym(f, XK_Return, 0);
		fakekey_release(f);
	}
}
