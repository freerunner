#!/usr/bin/python
import gtk
import gio

def file_changed (monitor, file, unknown, event):
  if event == gio.FILE_MONITOR_EVENT_CHANGES_DONE_HINT:
    print "file finished changing"

file = gio.File('/tmp/monitor')
monitor = file.monitor_file ()
monitor.connect ("changed", file_changed)
gtk.main()
