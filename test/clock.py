

import gtk, pango

w = gtk.Window(gtk.WINDOW)
w.set_size_request(16,16)
w.realize()
w.window.property_change('_XEMBED_INFO', '_XEMBED_INFO', 32, gtk.gdk.PROP_MODE_REPLACE, [1,1])
fd = pango.FontDescription('sans 10')
fd.set_absolute_size(25*pango.SCALE)
w.modify_font(fd)
layout = w.create_pango_layout("88:88")
(ink, (ex,ey,ew,eh)) = layout.get_pixel_extents()

pm = gtk.gdk.Pixmap(w.window, ew,eh)
pm.draw_rectangle(w.get_style().bg_gc[gtk.STATE_NORMAL],
                  True, 0, 0, ew, eh)
pm.draw_layout(w.get_style().fg_gc[gtk.STATE_NORMAL],
               0,0,layout)
w.set_size_request(ew,eh)


w.show()

def redraw(a,b):
    print "event", b
    w.window.draw_rectangle(w.get_style().bg_gc[gtk.STATE_NORMAL],
                            True, 0, 0, ew, eh)
    w.window.draw_layout(w.get_style().fg_gc[gtk.STATE_NORMAL],
                         0,0,layout)


w.connect('expose-event', redraw)
w.connect('configure-event', redraw)
gtk.main()

