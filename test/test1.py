
# experiment with clip board
# We define a clip board "test"
# We set it to 'waiting' and whenever anyone else sets it,
# we collect the value and reset to 'waiting'

import gtk
import pygtk
import gobject
targets = [ (gtk.gdk.SELECTION_TYPE_STRING, 0, 0) ]

def getdata(clipb, sel, info, data):
    print "sending"
    sel.set_text("waiting")

def cleardatadelay(clipb, data):
    print 'cleardel'
    gobject.timeout_add(2000, lambda : cleardata(clipb, data))
    
def cleardata(clipb, data):
    a = clipb.wait_for_text()
    print "Got ", a
    clipb.set_with_data(targets, getdata, cleardatadelay, None)
    
cb = gtk.Clipboard(selection='PRIMARY')

def set():
    global cb
    print "set"
    cb.set_with_data(targets, getdata, cleardatadelay, None)

gobject.idle_add(set)

gtk.main()

