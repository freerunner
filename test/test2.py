
# get the value from clipboard "test", then set a new value

import gtk
import pygtk
import sys
import gobject

targets = [ (gtk.gdk.SELECTION_TYPE_STRING, 0, 0) ]

def getdata(clipb, sel, info, data):
    a = sys.argv[1]
    print "sending", a
    sel.set_text(a)

def cleardata(clipb, data):
    print "clear"
    gtk.main_quit()
    
cb = gtk.Clipboard(selection='PRIMARY')

def set():
    global cb
    print "set"
    cb.set_with_data(targets, getdata, cleardata, None)

gobject.idle_add(set)
gtk.main()

